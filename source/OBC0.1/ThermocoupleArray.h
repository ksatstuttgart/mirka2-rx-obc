#pragma once
class ThermocoupleArray
{
public:
	ThermocoupleArray();

	~ThermocoupleArray();
	// //Fragt die Absoluttemperatur am TC ab (=>Wert im Hitzeschild)
	float polltc(int index = 0);
	// Fragt die Absoluttemperatur des Cold-Junction- Referenzchips ab (Verbaut im Kapselinneren)
	float polltcj(int index = 0);
	// Setzt TC-Einstellungen wieder auf die Ausgangswerte zur�ck
	void reset();
	// Initialisiert ein Thermocouple-Array (TBD: Hier m�ssen noch TC-typische Einstellungsparameter eingef�gt werden)
	void initialize();
};

