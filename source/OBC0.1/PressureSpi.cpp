#include "Debug.h"
#include "PressureSpi.h"


PressureSpi::PressureSpi()
{
	//--------------
	DEBUG_PRINT("S:PSens (SPI) Cr");
}


PressureSpi::~PressureSpi()
{
	//--------------
	DEBUG_PRINT("S:PSens (SPI) Dest");
}


// Fragt den SPI-Drucksensor ab
float PressureSpi::pollP()
{
	DEBUG_PRINT("DA:PSens_P (SPI)");
	return 0;
}


// Fragt den Temperaturwert des SPI-Drucksensors ab
float PressureSpi::pollT()
{
	DEBUG_PRINT("DA:PSens_T (SPI)");
	return 0;
}


// Setzt den SPI-Drucksensor auf Augangswerte zur�ck
void PressureSpi::reset()
{
	//--------------
	DEBUG_PRINT("S:PSens (SPI) Reset");
}


// Initialisiert den SPI-Drucksensor mit allen Pin- und sonstigen Werten
void PressureSpi::initialize()
{
	//--------------
	DEBUG_PRINT("S:PSens (SPI) Init");
}
