// TODO: 
// Konstruktoren �berdenken
// Init und Reset unterschiedlich machen
#include "Debug.h"
#include "Buffer.h"

Buffer::Buffer()
{

	//--------------
	DEBUG_PRINT("S:Buffer Cr");
}


Buffer::~Buffer()
{

	//--------------
	DEBUG_PRINT("S:Buffer De");
}


// Initialisiert den Datenpuffer des Boards (Noch gleich mit Reset)
void Buffer::initialize()
{
	//Alle Membervariablen gleich null setzen
	 m_aratex = 0;
	 m_aratey = 0;
	 m_aratez = 0;
	 m_accx = 0;
	 m_accy = 0;
	 m_accz = 0;
	 m_t1 = 0;
	 m_t2 = 0;
	 m_t3 = 0;
	 m_t4 = 0;
	 m_t5 = 0;
	 m_t6 = 0;
	 m_cjt1 = 0;
	 m_cjt2 = 0;
	 m_cjt3 = 0;
	 m_cjt4 = 0;
	 m_cjt5 = 0;
	 m_cjt6 = 0;
	 m_pspi1 = 0;
	 m_pspi2 = 0;
	 m_panalog = 0;
	 m_pst1 = 0;
	 m_pst2 = 0;
	 m_gpsx = 0;
	 m_gpsy = 0;
	 m_gpsz = 0;
	 m_gpstime = 0;
	//--------------
	DEBUG_PRINT("S:Buffer Init");
}


// L�scht alle Daten im Puffer und setzt ihn damit auf die Ausgangseinstellungen zur�ck
void Buffer::reset()
{
	m_aratex = 0;
	m_aratey = 0;
	m_aratez = 0;
	m_accx = 0;
	m_accy = 0;
	m_accz = 0;
	m_t1 = 0;
	m_t2 = 0;
	m_t3 = 0;
	m_t4 = 0;
	m_t5 = 0;
	m_t6 = 0;
	m_cjt1 = 0;
	m_cjt2 = 0;
	m_cjt3 = 0;
	m_cjt4 = 0;
	m_cjt5 = 0;
	m_cjt6 = 0;
	m_pspi1 = 0;
	m_pspi2 = 0;
	m_panalog = 0;
	m_pst1 = 0;
	m_pst2 = 0;
	m_gpsx = 0;
	m_gpsy = 0;
	m_gpsz = 0;
	m_gpstime = 0;
	//--------------
	DEBUG_PRINT("S:Buffer Reset");
}

//Getter und Setter f�r den Buffer, Setter gekoppelt f�r einfacheren Aufruf
void Buffer::setArate(float x, float y, float z)
{
	m_aratex = x;
	m_aratey = y;
	m_aratez = z;
}
void Buffer::setAcc(float x, float y, float z)
{
	m_accx = x;
	m_accy = y;
	m_accz = z;
}
void Buffer::setTemp(float t1, float t2, float t3, float t4, float t5, float t6)
{
	m_t1 = t1;
	m_t2 = t2;
	m_t3 = t3;
	m_t4 = t4;
	m_t5 = t5;
	m_t6 = t6;
}
void Buffer::setCJTemp(float tcj1, float tcj2, float tcj3, float tcj4, float tcj5, float tcj6)
{
	m_cjt1 = tcj1;
	m_cjt2 = tcj2;
	m_cjt3 = tcj3;
	m_cjt4 = tcj4;
	m_cjt5 = tcj5;
	m_cjt6 = tcj6;
}
void Buffer::setP(float pspi1, float pspi2, float panalog)
{
	m_pspi1 = pspi1;
	m_pspi2 = pspi2;
	m_panalog = panalog;
}
void Buffer::setPT(float pst1, float pst2)
{
	m_pst1 = pst1;
	m_pst2 = pst2;
}
void Buffer::setGPS(float gpsx, float gpsy, float gpsz, float gpstime)
{
	m_gpsx = gpsx;
	m_gpsy = gpsy;
	m_gpsz = gpsz;
	m_gpstime = gpstime;
}

float Buffer::getArate(int index)
{
	switch (index)
	{
	case 0:
		return m_aratex;
	case 1:
		return m_aratex;
	case 2:
		return m_aratez;
	default:
		return 0; //Vorl�ufiger Defaultwert, sollte durch etwas besser erkennbares ersetzt werden (z.B. quiet NaN oder der Float-Maximalwert)
	}
}
float Buffer::getAcc(int index)
{
	switch (index)
	{
	case 0:
		return m_accx;
	case 1:
		return m_accy;
	case 2:
		return m_accz;
	default:
		return 0;
	}
}
float Buffer::getTemp(int index)
{
	switch (index)
	{
	case 0:
		return m_t1;
	case 1:
		return m_t2;
	case 2:
		return m_t3;
	case 3:
		return m_t4;
	case 4:
		return m_t5;
	case 5:
		return m_t6;
	default:
		return 0;
	}
}
float Buffer::getCJTemp(int index)
{
	switch (index)
	{
	case 0:
		return m_cjt1;
	case 1:
		return m_cjt2;
	case 2:
		return m_cjt3;
	case 3:
		return m_cjt4;
	case 4:
		return m_cjt5;
	case 5:
		return m_cjt6;
	default:
		return 0;
	}
}
float Buffer::getP(int index)
{
	switch (index)
	{
	case 0:
		return m_pspi1;
	case 1:
		return m_pspi2;
	case 2:
		return m_panalog;
	default:
		return 0;
	}
}
float Buffer::getPT(int index)
{
	switch (index)
	{
	case 0:
		return m_pst1;
	case 1:
		return m_pst2;
	default:
		return 0;
	}
}
float Buffer::getGPS(int index)
{
	switch (index)
	{
	case 0:
		return m_gpsx;
	case 1:
		return m_gpsy;
	case 2:
		return m_gpsz;
	case 3:
		return m_gpstime;
	default:
		return 0;
	}
}