//TODO: Richtige Init- und Abrufroutinen verwenden
#include "Debug.h"
#include "Imu.h"


Imu::Imu()
{
	//--------------
	DEBUG_PRINT("S:IMU Cr");
}


Imu::~Imu()
{
	//--------------
	DEBUG_PRINT("S:IMU De");
}


// Setzt die IMU auf Ausgangseinstellungen zur�ck
void Imu::reset()
{
	m_aratex = 0;
	m_aratey = 0;
	m_aratez = 0;

	m_accx = 0;
	m_accy = 0;
	m_accz = 0;
	//--------------
	DEBUG_PRINT("S:IMU Reset");
}


// Gibt die Beschleunigungswerte abh�ngig vom �bergebenen Index zur�ck:
float Imu::pollAcc(int index)
{
	DEBUG_PRINT("DA:IMU Acceleration");
	if (acc_isnew)
	{
		switch (index)
		{
		case 0:
			return m_accx;
		case 1:
			return m_accy;
		case 2:
			return m_accz;
		default:
			return 0;
		}
	}
	else
	{
		return 0;
	}
}


// Gibt die Drehrate (Angular Rate) abh�ngig vom �bergebenen Index zur�ck: 0 = x, 1 = y, 2 = z
float Imu::pollArate(int index)
{
	DEBUG_PRINT("DA:IMU Angular Rate");
	if (arate_isnew)
	{
		switch (index)
		{
		case 0:
			return m_aratex;
		case 1:
			return m_aratey;
		case 2:
			return m_aratez;
		default:
			return 0;
		}
	}
	else
	{
		return 0;
	}
}


// Initialisiert die IMU mit allen ben�tigten Pin- und sonstigen Werten
void Imu::initialize()
{
	m_aratex = 0;
	m_aratey = 0;
	m_aratez = 0;

	m_accx = 0;
	m_accy = 0;
	m_accz = 0;
	arate_isnew = false; //Flags setzen
	acc_isnew = false;
	//--------------
	DEBUG_PRINT("S:IMU Init");
}

//Ruft neue Daten ab und speichert sie in der Klasse
void Imu::tick()
{
	m_aratex = 1; //Hier kommen die wirklichen Abrufroutinen hin
	m_aratey = 1;
	m_aratez = 1;

	m_accx = 1;
	m_accy = 1;
	m_accz = 1;

	arate_isnew = true; //Flags setzen
	acc_isnew = true;
	//--------------
	DEBUG_PRINT("S:IMU Tick");
}