#pragma once
class PressureAnl
{
public:
	PressureAnl();
	~PressureAnl();
	// Fragt den Druckwert des Analog-Drucksensors ab
	float pollP();
	// Setzt alle Einstellungen des Analog-Drucksensors zur�ck
	void reset();
	// Initialisiert den Analog-Drucksensor mit allen ben�tigten Pin- und sonstigen Einstellungen
	void initialize();
};

