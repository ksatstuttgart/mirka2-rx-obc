#pragma once
class Imu
{
private:
	// Beschleunigung
	float m_accx;
	float m_accy;
	float m_accz;
	// Drehrate
	float m_aratex;
	float m_aratey;
	float m_aratez;
	// Flags f�r die Datenr�ckgabe
	bool arate_isnew;
	bool acc_isnew;
public:
	Imu();
	~Imu();
	// Setzt die IMU auf Ausgangseinstellungen zur�ck
	void reset();
	// Liest die Werte f�r Drehrate und Beschleunigung aus und speichert sie in der Klasse
	void tick();
	// Gibt die zuletzt gespeicherten Beschleunigungswerte in den drei Achsen zur�ck
	float pollAcc(int index = 0);
	// Gibt die zuletzt gespeicherten Drehraten (Angular Rates) auf allen drei Achsen zur�ck
	float pollArate(int index = 0);
	// Initialisiert die IMU mit allen ben�tigten Pin- und sonstigen Werten
	void initialize();
};

