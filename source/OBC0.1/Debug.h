//--------------------------------------------------------------------------------------------------------//
// Debugheader, definiert die Debugausgabe �ber den seriellen Port.
// Um die Codegr��e kleinzuhalten, hier ein Abk�rzungsverzeichnis:
// S = Success
// F = Failure
// Cr = Creation
// De = Destruction
// Init = Initialization
// DA = Data Acquisition, Sensordaten ausgelesen
// RTS = Ready to send
// PSens = Pressure Sensor
// 
//--------------------------------------------------------------------------------------------------------//

#pragma once

//Debugdefine: Auskommentieren, um Debugausgaben zu entfernen
//#define DEBUG

//-----------------------Debugausgabe: Bitte unver�ndert lassen-----------------------
#ifdef DEBUG
#include "Arduino.h"
#define DEBUG_PRINT(x)  Serial.println (x)
//volatile bool imuAcc;
//volatile bool imuArate;
//volatile bool imuTick;
//volatile bool PAnl;
//volatile bool PSpi1;
//volatile bool PTSpi1;
//volatile bool PSpi2;
//volatile bool PTSpi2;
//volatile bool Tc;
//volatile bool TcCj;
//
//#define LOG_IMUACC imuAcc = true;
//#define DELOG_IMUACC imuAcc = false;
//#define LOG_IMURATE imuArate = true;
//#define DELOG_IMURATE imuArate = false;
//#define LOG_IMUTICK imuTick = true;
//#define LOG_IMUTICK imuTick = true;
//#define LOG_PANL PAnl = true;
//#define LOG_PANL PAnl = true;
//#define LOG_IMUACC PSpi1 = true;
//#define LOG_IMUACC PSpi1 = true;
//#define LOG_IMUACC PSpi2 = true;
//#define LOG_IMUACC PSpi2 = true;
//#define LOG_IMUACC PTSpi1 = true;
//#define LOG_IMUACC PTSpi1 = true;

#else
#define DEBUG_PRINT(x)
#endif
//-----------------------Debugausgabe Ende-----------------------