//TODO:
//Einheiten erg�nzen
//Wie viele der Drucksensoren verf�gen �ber Temperaturf�hler?
//Evtl. m�ssen setter �berarbeitet werden
//Lifesign-Signale f�r Watchdog eventuell hierhin

#pragma once

class Buffer
{
private:
	//Drehrate IMU
	volatile float m_aratex;
	volatile float m_aratey;
	volatile float m_aratez;
	//Beschleunigung IMU
	volatile float m_accx;
	volatile float m_accy;
	volatile float m_accz;
	//Temperaturen der Thermocouples
	volatile float m_t1;
	volatile float m_t2;
	volatile float m_t3;
	volatile float m_t4;
	volatile float m_t5;
	volatile float m_t6;
	//Temperaturen der Cold-Junction-Sensoren
	volatile float m_cjt1;
	volatile float m_cjt2;
	volatile float m_cjt3;
	volatile float m_cjt4;
	volatile float m_cjt5;
	volatile float m_cjt6;
	//Dr�cke der Drucksensoren
	volatile float m_pspi1;
	volatile float m_pspi2;
	volatile float m_panalog;
	//Temperatur der Drucksensor-Chips
	volatile float m_pst1;
	volatile float m_pst2;
	//Koordinaten des GPS (evtl. noch �ndern, falls Koord. anderes Format haben)
	volatile float m_gpsx;
	volatile float m_gpsy;
	volatile float m_gpsz;
	volatile float m_gpstime;
	

public:
	Buffer();
	~Buffer();
	// Initialisiert den Datenpuffer des Boards
	void initialize();
	// L�scht alle Daten im Puffer und setzt ihn damit auf die Ausgangseinstellungen zur�ck
	void reset();
	//Getter und Setter f�r den Buffer, gekoppelt f�r einfacheren Aufruf
	void setArate(float x, float y, float z);
	void setAcc(float x, float y, float z);
	void setTemp(float t1, float t2, float t3, float t4, float t5, float t6);
	void setCJTemp(float tcj1, float tcj2, float tcj3, float tcj4, float tcj5, float tcj6);
	void setP(float pspi1, float pspi2, float panalog);
	void setPT(float pst1, float pst2);
	void setGPS(float gpsx, float gpsy, float gpsz, float gpstime);

	float getArate(int index = 0);
	float getAcc(int index = 0);
	float getTemp(int index = 0);
	float getCJTemp(int index = 0);
	float getP(int index = 0);
	float getPT(int index = 0);
	float getGPS(int index = 0);

};

