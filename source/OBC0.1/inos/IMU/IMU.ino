
#include <FreeSixIMU.h>
#include <FIMU_ADXL345.h>
#include <FIMU_ITG3200.h>
#include <Wire.h>
#include <Arduino.h>

#ifndef IMU_H
#define IMU_H
/*
 
 */

class Imu
{
public:
	Imu();
	~Imu();
	// Setzt die IMU auf Ausgangseinstellungen zur�ck
	void reset();
	// Gibt die Beschleunigungswerte in den drei Achsen an den �bergebenen Array zur�ck ([x,y,z])
    void pollacc(float acc[]);
	// Ruft die Drehrate (Angular Rate) auf allen drei Achse ab und schreibt sie in den �bergebenen Array ([x,y,z])
    void pollarate(float aRate[]);
    // Initialisiert die IMU mit allen ben�tigten Pin- und sonstigen Werten
    void initialize();
};
#endif

FreeSixIMU sixDOF = FreeSixIMU();

Imu::Imu()
{
}

Imu::~Imu()
{
}

// Setzt die IMU auf Ausgangseinstellungen zur�ck
void Imu::reset()
{
    initialize();
}

// Gibt die Beschleunigungswerte in den drei Achsen an den �bergebenen Array zur�ck ([x,y,z])
void Imu::pollacc(float acc[])
{
    sixDOF.getValues(acc);
}


// Ruft die Drehrate (Angular Rate) auf allen drei Achse ab und schreibt sie in den �bergebenen Array ([x,y,z])
void Imu::pollarate(float aRate[])
{
    sixDOF.getAngles(aRate);
}


// Initialisiert die IMU mit allen ben�tigten Pin- und sonstigen Werten
void Imu::initialize()
{
    sixDOF.init(); //begin the IMU
}

Imu imu;
void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  imu = Imu();
  imu.initialize();
  
  //Serial.println("initialization done.");
}

void loop(){
  //Serial.println("-----------------");
  //Serial.print("acc:");
  //float acc[3];
  //imu.pollacc(acc); 
  //for(int i = 0; i < 3; i++){
  //  Serial.print(String(int(acc[i]))+ "."+String(getDecimal(acc[i])) + ","); //combining both whole and decimal part in string with a full                                                                      //stop between them
  //}
  //Serial.println("");
  //Serial.print("rate:");
  float rate[3];
  imu.pollarate(rate);
  for(int i = 0; i < 3; i++){
    Serial.print(String(int(rate[i]))+ "."+String(getDecimal(rate[i]))); //combining both whole and decimal part in string with a full stop between them
    if(i < 2) Serial.print(",");
  }
  Serial.println("");
  
  delay(100);
}

//function to extract decimal part of float
long getDecimal(float val)
{
 int intPart = int(val);
 long decPart = 1000*(val-intPart); //I am multiplying by 1000 assuming that the foat values will have a maximum of 3 decimal places
                                   //Change to match the number of decimal places you need
 if(decPart>0)return(decPart);           //return the decimal part of float number if it is available 
 else if(decPart<0)return((-1)*decPart); //if negative, multiply by -1
 else if(decPart=0)return(00);           //return 0 if decimal part of float number is not available
}
