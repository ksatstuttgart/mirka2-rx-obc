#include "Debug.h"
#include "Intercom.h"


Intercom::Intercom()
{
	//--------------
	DEBUG_PRINT("S:Intercom Cr");
}


Intercom::~Intercom()
{
	//--------------
	DEBUG_PRINT("S:Intercom De");
}


// Initialisiert die Board- zu Board- Kommunikation per SPI
void Intercom::initialize(bool isMaster)
{
	//--------------
	DEBUG_PRINT("S:Intercom Init");
}


// Setzt alle Einstellungen der Board- zu - Board-Kommunikation auf die Ausgangseinstellungen zur�ck
void Intercom::reset()
{
	//--------------
	DEBUG_PRINT("S:Intercom Reset");
}

// �berpr�ft und formatiert alle neu in den Datenpuffern vorliegenden Daten und schreibt sie in den Sendepuffer. Gibt im Erfolgsfall ein true zur�ck.
bool Intercom::readyToSend()
{
	DEBUG_PRINT("RTS:Intercom");
	return true;
}


// Nur ausf�hrbar f�r den SPI-Master: Sammelt alle neu in den Sendepuffern vorliegende Daten der anderen Boards ein und schickt die im vorigen Durchlauf gesammelten Daten an das Kommunikationsboard weiter. Gibt im Erfolgsfall true zur�ck.
bool Intercom::ping()
{
	DEBUG_PRINT("S:Intercom Master Data Call");
	return true;
}
