#include "Debug.h"
#include "ThermocoupleArray.h"


ThermocoupleArray::ThermocoupleArray()
{
	DEBUG_PRINT("S:TCs Cr");
}


ThermocoupleArray::~ThermocoupleArray()
{
	DEBUG_PRINT("S:TCs De");
}


// //Fragt die Absoluttemperatur am TC ab (=>Wert im Hitzeschild)
float ThermocoupleArray::polltc(int index)
{
	DEBUG_PRINT("DA:TCs T");
	switch (index)
	{
		//Hier die breaks durch returns ersetzen
	case 0:
		//TBD: TC1 abrufen
		return 0;
	case 1:
		//TBD: TC2 abrufen
		return 0;
	case 2:
		//TBD: TC3 abrufen
		return 0;
	case 3:
		//TBD: TC4 abrufen
		return 0;
	case 4:
		//TBD: TC5 abrufen
		return 0;
	case 5:
		//TBD: TC6 abrufen
		return 0;
	default:
		return 0; // Hier noch besseren Defaultwert einf�gen 
	}
}


// Fragt die Absoluttemperatur des Cold-Junction- Referenzchips ab (Verbaut im Kapselinneren)
float ThermocoupleArray::polltcj(int index)
{
	DEBUG_PRINT("DA:TCs CjT");
	switch (index)
	{
		//Hier die breaks durch returns ersetzen
	case 0:
		//TBD: TCJ1 abrufen
		return 0;
	case 1:
		//TBD: TCJ2 abrufen
		return 0;
	case 2:
		//TBD: TCJ3 abrufen
		return 0;
	case 3:
		//TBD: TCJ4 abrufen
		return 0;
	case 4:
		//TBD: TCJ5 abrufen
		return 0;
	case 5:
		//TBD: TCJ6 abrufen
		return 0;
	default:
		return 0; // Hier noch besseren Defaultwert einf�gen 
	}
}

// Setzt TCs wieder auf die Ausgangswerte zur�ck
void ThermocoupleArray::reset()
{
	DEBUG_PRINT("S:TCs Reset");
}


// Initialisiert ein Thermocouple (TBD: Hier m�ssen noch TC-typische Einstellungsparameter eingef�gt werden)
void ThermocoupleArray::initialize()
{
	DEBUG_PRINT("S:TCs Init");
}
