#include "Debug.h"
#include "PressureAnl.h"


PressureAnl::PressureAnl()
{
	//--------------
	DEBUG_PRINT("S:PSens (Analog) Cr");
}


PressureAnl::~PressureAnl()
{
	//--------------
	DEBUG_PRINT("S:PSens (Analog) De");
}


// Fragt den Druckwert des Analog-Drucksensors ab
float PressureAnl::pollP()
{

	DEBUG_PRINT("DA:PSens (Analog)");
	return 0;
}


// Setzt alle Einstellungen des Analog-Drucksensors zur�ck
void PressureAnl::reset()
{
	//--------------
	DEBUG_PRINT("S:PSens (Analog) Reset");
}


// Initialisiert den Analog-Drucksensor mit allen ben�tigten Pin- und sonstigen Einstellungen
void PressureAnl::initialize()
{
	//--------------
	DEBUG_PRINT("S:PSens (Analog) Init");
}
