//TODO: Die einzelnen Tasks m�ssen ausgemessen werden, um ihre zeitliche Dauer zu erhalten

//--------------------------------------------------------------------------------------------------------//
// Arduinosketch: OBC Grundger�st f�r Prototyp OBC 0.1
//
// Klassenkonstrukte und anderes Objektorientiertes Zeug ist im Prototyp enthalten, wird aber je nach
// Speicherbedarf in sp�teren Revisionen wieder in serielle Programmierung umgewandelt.
//--------------------------------------------------------------------------------------------------------//

//Includes
#include "Debug.h"
#include "Buffer.h"
#include "Imu.h"
#include "Intercom.h"
#include "PressureAnl.h"
#include "PressureSpi.h"
#include "ThermocoupleArray.h"

//Globales definieren der Zust�nde
enum MODE
{
	INIT, //INIT: Initialisierung, hier werden die Sensoren und der Watchdog-Mechanismus initialisiert
	TESTING, //TESTING: Bodentestmodus, soll sich aktivieren, sobald alle drei Debug-Pins auf dem PCB gesetzt sind
	FLIGHT,	 //FLIGHT: Flugmodus, ist default wenn die Debug-Pins nicht gesetzt sind
	ERROR	//ERROR: Fehlermodus/Safemode, wenn etwas sehr schiefgeht oder ein Board die anderen gef�hrdet, kann es (tempor�r) abgeschaltet werden.
};

//Definieren des aktuellen Zustands
MODE current_mode;

//Sensoren und Dienste erstellen
Imu imu; //IMU
Buffer buffer; //Datenspeicher des Mikrocontrollers
Intercom intercom; //SPI-Kommunikation
PressureAnl pressureAnl; //Analog-Drucksensor
PressureSpi pressureSpi1; //SPI- Drucksensoren
PressureSpi pressureSpi2;
ThermocoupleArray thermocoupleArray; //Array von 6 Thermocouples

//Z�hler f�r die Task-ISR erstellen
volatile int isrCounter = 0;

void setup()
{
//Debugausgabe: LES aktivieren
pinMode(13, OUTPUT);
//Timer-Interrupt f�r das Taskmanagement setzen, Frequenz 5 Hz
	cli(); // F�r die Dauer der Interrupteinstellungen alle Interrupts ausschalten
	TCCR1A = 0;// Das TCCR1A-Register auf 0 setzen
	TCCR1B = 0;// s.o., f�r TCCR1B
	TCNT1 = 0;// Z�hler auf 0 setzen
	// Das compare-match-register f�r Inkrement alle f�nftelsekunde setzen
	OCR1A = 3124;// = (16*10^6) / (5*1024) - 1 (muss <65536 sein)
	// Clear Timer on Compare Match Modus setzen
	TCCR1B |= (1 << WGM12);
	// CS10 und CS12-Bit setzen, um einen Prescaler von 1024 zu erhalten (s.o.)
	TCCR1B |= (1 << CS12) | (1 << CS10);
	// Den Interrupt (Timer Compare) einschalten
	TIMSK1 |= (1 << OCIE1A);
	sei(); //Nach Ende der Einstellungen Interrupts wieder einschalten

//Baudrate f�r die (Debug-)Ausgabe festlegen
	Serial.begin(57600);
	DEBUG_PRINT("S:Serial connection");
//Anfangszustand setzen
	DEBUG_PRINT("S:Mode=INIT");
	current_mode = INIT;

}

void loop()
{
//-----------------------Beginn der Haupt-FSM-----------------------
	switch (current_mode)
	{
		case INIT:
		{
						//Sensoren und Interfaces initialisieren
						buffer.initialize();
						imu.initialize();
						intercom.initialize(true);
						pressureAnl.initialize();
						pressureSpi1.initialize();
						pressureSpi2.initialize();
						thermocoupleArray.initialize();
						//Wechsel zum FLIGHT-Modus
						current_mode = FLIGHT;
						DEBUG_PRINT("Entering Flight State");
						break;
		}

		case TESTING:
		{
						DEBUG_PRINT("Entering Ground Testing State");
						//-------------------------------------------
						break;
		}
		case FLIGHT:
		{
						//-------------------------------------------
						// Timing der Tasks: Regelm��iges aufrufen des Interrupttimers
						// Timer1 wird benutzt, da Timer0 f�r die millis()-fkt. genutzt wird und Timer2 f�r die tone()-Funktion (Evtl. f�r Buzzer genutzt).
						// Die Interrupt Service Routine enth�lt den eigentlichen Code.

					    //Abarbeiten der Debugausgaben (Sollte eher nicht in der ISR abgearbeitet werden): TBD


						break;
		}
		case ERROR:
		{
						DEBUG_PRINT("Entering Error/Safemode State");
						//-------------------------------------------
						break;
		}
			//Falls current_mode aus irgendeinem Grund nicht richtig gesetzt ist, wird er wieder auf INIT zur�ckgeschaltet
		default:
		{
				   current_mode = INIT;
				   break;
		}
	}
//-----------------------Ende der Haupt-FSM-----------------------
}
//Interrupt Service Routine f�r den Timer-Interrupt (Wird bei jedem Interrupt ausgef�hrt), enth�lt das Task Management
ISR(TIMER1_COMPA_vect){
	switch (current_mode)
	{
	case INIT:
		//Noch wird hier nichts getan, die Synchronisation ist TBD
		break;
	case TESTING:
		//Noch nicht implementiert
		break;
	case FLIGHT:
		// Hier werden alle Haupttasks ausgef�hrt, und zwar nach folgendem Schema (Unterteilt in Ticks von je 1/5 s):
		// 1. Sensorwerte auslesen und im Buffer speichern (Sensor/IMU), in vorigem Takt empfangene Werte verarbeiten(Iridium)
		// 2. Werte mit dem jeweils anderen mC austauschen (Sensor/IMU), "
		// 3. Werte auf SD speichern/an Iridium-mC schicken, "
		// 4. Healthdaten sammeln und verarbeiten
		// 5. Watchdog ausf�hren

		switch (isrCounter)
		{
		case 0:
			//Debugausgabe: LED anschalten
			digitalWrite(13, HIGH);
			//IMU-Daten aktualisieren
			imu.tick();
			//IMU-Daten abrufen und in Buffer speichern
			buffer.setAcc(imu.pollAcc(0), imu.pollAcc(1), imu.pollAcc(2));
			buffer.setArate(imu.pollArate(0), imu.pollArate(1), imu.pollArate(2));
			//Dr�cke abrufen und in Buffer speichern
			buffer.setP(pressureSpi1.pollP(), pressureSpi2.pollP(), pressureAnl.pollP());
			//Temperaturen abrufen und in Buffer speichern
			buffer.setTemp(thermocoupleArray.polltc(0), thermocoupleArray.polltc(1), thermocoupleArray.polltc(2), thermocoupleArray.polltc(3), thermocoupleArray.polltc(4), thermocoupleArray.polltc(5));
			buffer.setCJTemp(thermocoupleArray.polltcj(0), thermocoupleArray.polltcj(1), thermocoupleArray.polltcj(2), thermocoupleArray.polltcj(3), thermocoupleArray.polltcj(4), thermocoupleArray.polltcj(5));
			buffer.setPT(pressureSpi1.pollT(), pressureSpi2.pollT());
			//GPS-Koordinaten abrufen und in Buffer speichern (TBD)
			//Thermopile-Daten abrufen und in Buffer speichern (TBD)

			isrCounter++;
			break;
		case 1:
			isrCounter++;
			break;
		case 2:
			isrCounter++;
			break;
		case 3:
			isrCounter++;
			break;
		case 4:
			//Debugausgabe: LED ausschalten
			digitalWrite(13, LOW);
			isrCounter++;
			break;
		default:
			isrCounter = 0; //Falls ein Fehler auftritt und der Counter eine Zahl gr��er 4 erreicht, wird er zur�ckgesetzt.
			break;
		}

		break;
	case ERROR:
		//Noch nicht implementiert
		break;
	default:
		break;
	}
}
