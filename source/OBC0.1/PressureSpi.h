#pragma once
class PressureSpi
{
public:
	PressureSpi();
	~PressureSpi();
	// Fragt den SPI-Drucksensor ab
	float pollP();
	// Fragt den Temperaturwert des SPI-Drucksensors ab
	float pollT();
	// Setzt den SPI-Drucksensor auf Augangswerte zur�ck
	void reset();
	// Initialisiert den SPI-Drucksensor mit allen Pin- und sonstigen Werten
	void initialize();
};

