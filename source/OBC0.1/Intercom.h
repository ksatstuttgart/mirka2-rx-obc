#pragma once
class Intercom
{
public:
	Intercom();
	~Intercom();
	// Initialisiert die Board- zu Board- Kommunikation per SPI
	void initialize(bool isMaster);
	// Setzt alle Einstellungen der Board- zu - Board-Kommunikation auf die Ausgangseinstellungen zur�ck
	void reset();
	// �berpr�ft und formatiert alle neu in den Datenpuffern vorliegenden Daten und schreibt sie in den Sendepuffer. Gibt im Erfolgsfall ein true zur�ck. 
	bool readyToSend();
	// Nur ausf�hrbar f�r den SPI-Master: Sammelt alle neu in den Sendepuffern vorliegende Daten der anderen Boards ein und schickt die im vorigen Durchlauf gesammelten Daten an das Kommunikationsboard weiter. Gibt im Erfolgsfall true zur�ck.
	bool ping();
};

