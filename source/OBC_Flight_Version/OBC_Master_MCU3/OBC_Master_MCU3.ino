#include <EEPROM.h>
#include <SD.h>
#include <SPI.h>
#include "FSM.h"
#include "avr\wdt.h"
#include "Pins.h"
#include "Defines.h"
#include "Sys_Status.h"
#include "Buffer.h"
#include "SPI_Master.h"
#include "TCs.h"
#include "PSens.h"

OBC_STATES cur_state = ENTRY_STATE; //Anfangsstatus festlegen
ALTITUDE cur_alt = HIGHALT; //Anfangsh�he festlegen
RET_CODES retVal = ERROR; //Returncode-Variable erstellen, hier als Error falls die erste Pointerzuordnung gleich fehlschl�gt

systemStatus sysStatus = { false, 1 }; //System-Statusstruktur erstellen
sensorStatus sensStatus = { false }; //Sensor-Statusstruktur erstellen, enth�lt "Working"-Flags der Peripherie
systemTime sysT; //Speicher f�r die Systemzeit
timeBuf tBuf; //Buffer f�r den EEPROM-Speichervorgang

from_mcu2_type from_mcu2; //Buffer f�r die Kommunikation erstellen
to_mcu1_type to_mcu1;

File logFile;
File *lFile = &logFile;

//Timer-Variablen erstellen
volatile uint16_t isrCounter = 0;
volatile bool next = false;

//Sensoren erstellen
PSens digPs_smd = PSens(SS_PSENS_SMD, 4096);
PSens digPs_ext = PSens(SS_PSENS_EXT, 4096);

int(*state_fun)(void) = state[cur_state]; //Funktionspointer zum Ausf�hren der Zust�nde erstellen, mit Anfangszustand belegen
int retbuf = 0; //Buffer f�r R�ckgabewerte erstellen, Wert 0 ist nicht in der �bergangstabelle enthalten -> F�hrt zu �bergang in SYSERROR-Mode wenn kein neuer Wert vor der ersten Verwendung eingespielt wird

//Generelle Flags
bool doNothing = false; //Benutzt, um die MCUs inert zu schalten, wenn sie nichts mehr zu tun haben
volatile bool mcu1call = false; //Flag: MCU1 hat den externen Interrupt ausgel�st
volatile bool mcu2call = false; //...
bool mcu1flag = false; //Flags, die die Werte von mcu1call/mcu2call bei Abruf "einfrieren", damit sie innnerhalb eines Funktionsdurchlaufes konstant bleiben
bool mcu2flag = false; //"

//TODO: Evtl. in struct verschieben
short spifail_mcu1 = 0; //Z�hlen die Misserfolge in Serie bei der Kommunikation mit den MCUs
short spifail_mcu2 = 0;
short syncfail_mcu1 = 0; //Z�hlen die Sync-Misserfolge
short syncfail_mcu2 = 0;

//TODO: "Vorstrafenregister" der anderen beiden MCUs, hier wird gesammelt. Anhand der gesammelten Informationen kann entschieden werden. Auch in struct
bool mcu1_int_na = false; //Interruptleitung von MCU1 zu 3 hat beim letzten Versuch nicht funktioniert
bool mcu2_int_na = false; //...2 zu 3

bool mcu1_mia = false; //MCU1 meldet sich nicht �ber Interrupt, Ergebnisse der SPI-Kommunikation werden abgewartet
bool mcu2_mia = false; //MCU2...



void setup()
{
	//Alle Pins inert schalten
	pinMode(SS_PSENS_EXT, INPUT); //D1
	pinMode(INT_INPUT_MCU2, INPUT); //D2 
	pinMode(INT_INPUT_MCU1, INPUT); //D3
	pinMode(SS_PSENS_SMD, INPUT); //D4
	pinMode(SS_TC1, INPUT); //D5, au�er NEWSS-Debugflag ist gesetzt
	pinMode(SS_TC2, INPUT); //D6, "
	pinMode(SS_TC3, INPUT); //D7
	pinMode(SS_TC4, INPUT); //D8
	pinMode(SS_TC5, INPUT); //D9
	//pinMode(SS_TC6, INPUT); //D10
	//pinMode(S_MOSI, INPUT); //D11
	//pinMode(S_MISO, INPUT); //D12
	//pinMode(S_SCK, INPUT); //D13

	pinMode(INT_OUTPUT, INPUT); //A0
	pinMode(VOTE_MCU2, INPUT); //A1
	pinMode(VOTE_MCU1, INPUT); //A2
	pinMode(SS_SD, INPUT); //A3
	pinMode(SS_MCU1, INPUT); //A4, au�er NEWSS-Debugflag ist gesetzt
	pinMode(SS_MCU2, INPUT); //A5, "
	pinMode(DEBUGPIN, INPUT); //A6
	pinMode(PSENS_ANALOG, INPUT);//A7

	digitalWrite(SS_PSENS_EXT, LOW);
	digitalWrite(INT_INPUT_MCU2, LOW);
	digitalWrite(INT_INPUT_MCU1, LOW);
	digitalWrite(SS_PSENS_SMD, LOW);
	digitalWrite(SS_TC1, LOW);
	digitalWrite(SS_TC2, LOW);
	digitalWrite(SS_TC3, LOW);
	digitalWrite(SS_TC4, LOW);
	digitalWrite(SS_TC5, LOW); 
	//digitalWrite(SS_TC6, LOW);
	//digitalWrite(S_MOSI, LOW);
	//digitalWrite(S_MISO, LOW); 
	//digitalWrite(S_SCK, LOW);

	digitalWrite(INT_OUTPUT, LOW); 
	digitalWrite(VOTE_MCU2, LOW);
	digitalWrite(VOTE_MCU1, LOW);
	digitalWrite(SS_SD, LOW);
	digitalWrite(SS_MCU1, LOW);
	digitalWrite(SS_MCU2, LOW);
	digitalWrite(DEBUGPIN, LOW);
	digitalWrite(PSENS_ANALOG, LOW);

#ifdef DBG_SERIALOUTPUT
	//Serielle Kommunikation einschalten: Nur wenn Debugging-Falg gesetzt ist (Nicht zu verwechseln mit dem DEBUG-Pin)
	Serial.begin(38400);
	pinMode(0, INPUT); //TODO Keine Ahnung ob das n�tig ist oder wirkt: RX inert schalten, da nur TX benutzt wird
	digitalWrite(0, LOW); //"
#endif // DBG_SERIALOUTPUT
	
	//Watchdogtimer mit einer Laufzeit von 8 Sekunden stellen (Beliebig gew�hlte Zeit, muss evtl. ver�ndert werden)
	//wdt_enable(WDTO_8S); //TODO wdt 8s hat nicht funktioniert, muss �berarbeitet werden

	//Externe Interrupts anschlie�en
	attachInterrupt(digitalPinToInterrupt(INT_INPUT_MCU1), int_mcu1call, RISING); //Rising edge, die anderen MCUs m�ssen ihre Pins auf HIGH stellen
	attachInterrupt(digitalPinToInterrupt(INT_INPUT_MCU2), int_mcu2call, RISING); //"

	//Systemzeit auf 0 initialisieren
	sysT.timeOffset = 0;

	//Timer-Interrupt f�r getimte Tasks setzen, Frequenz 10 Hz (Alle 100 Millisekunden) ausgel�st durch Timer1
	cli(); // F�r die Dauer der Interrupteinstellungen alle Interrupts ausschalten
	TCCR1A = 0;// Das TCCR1A-Register auf 0 setzen
	TCCR1B = 0;// s.o., f�r TCCR1B
	TCNT1 = 0;// Z�hler auf 0 setzen
	// Das compare-match-register f�r Inkrement alle 2 Sekunden setzen
	OCR1A = 6249;// = (16*10^6) / (10*256) - 1 (muss <65536 sein)
	// Clear Timer on Compare Match Modus setzen
	TCCR1B |= (1 << WGM12);
	//CS12-Bit setzen, um einen Prescaler von 256 zu erhalten (s.o.)
	TCCR1B |= (1 << CS12);
	// Den Interrupt (Timer Compare) einschalten
	TIMSK1 |= (1 << OCIE1A);
	sei(); //Nach Ende der Einstellungen Interrupts wieder einschalten
}

void loop()
{
	if (!doNothing)
	{
		retbuf = state_fun(); //F�hre aktuellen Zustand aus und erhalte R�ckgabewert
		if (retbuf > MIN_RET && retbuf < MAX_RET) //Pr�fe G�ltigkeit des Returnwerts
		{
			retVal = (RET_CODES)retbuf; //Konvertiere zu Returnwert-Typ 
		}
		cur_state = lookup_transitions(cur_state, cur_alt, retVal); //Zustand wechseln
		state_fun = state[cur_state]; //Zustands-Funktionspointer aktualisieren
		if (EXIT_STATE == cur_state) //Ist letzter Zustand der FSM erreicht, schalte FSM-Ausf�hrung VOR BEGINN DES ENDZUSTANDS aus
		{
			doNothing = true;
		}
	}
	//wdt_reset(); //Watchdog-Reset TODO Siehe andere wdt-Zeile
}

//�bergangs-Funktion:
OBC_STATES lookup_transitions(OBC_STATES state, ALTITUDE alt, RET_CODES ret)
{
	int i; //Z�hler
	int transCount = 0;

	switch (alt) //Analysiere die aktuelle H�he und wechsele die Lookuptabelle dementsprechend, 3x �hnlicher Code wg. Faulheit
	{
	case HIGHALT:
		transCount = (sizeof(state_transitions_default) / sizeof(*state_transitions_default)); //Die Anzahl der Eintr�ge im Array = Anzahl m�glicher �berg�nge
		for (i = 0; i < transCount; i++) //Gehe alle m�glichen �berg�nge durch, bis ein passender gefunden wurde
		{
			if (state == state_transitions_default[i].source_state) //Aktueller Zustand als Ursprung(Source) gefunden
			{
				if (ret == state_transitions_default[i].ret_code) //�bergebener Returnwert als valider Wert gefunden
				{
					return state_transitions_default[i].dest_state; //Gebe passenden neuen Zustand(Destination) aus
				}
			}
		}
		return SYS_ERROR; //Einer oder alle der eingegebenen Parameter waren nicht in der �bergangstabelle enthalten!
		//Keine break-Anweisung wegen dar�berliegender Returns
	case MEDALT:
		transCount = (sizeof(state_transitions_medalt) / sizeof(*state_transitions_medalt)); //Die Anzahl der Eintr�ge im Array = Anzahl m�glicher �berg�nge
		for (i = 0; i < transCount; i++) //Gehe alle m�glichen �berg�nge durch, bis ein passender gefunden wurde
		{
			if (state == state_transitions_medalt[i].source_state) //Aktueller Zustand als Ursprung(Source) gefunden
			{
				if (ret == state_transitions_medalt[i].ret_code) //�bergebener Returnwert als valider Wert gefunden
				{
					return state_transitions_medalt[i].dest_state; //Gebe passenden neuen Zustand(Destination) aus
				}
			}
		}
		return SYS_ERROR; //Einer oder alle der eingegebenen Parameter waren nicht in der �bergangstabelle enthalten!

	case LOWALT:
		transCount = (sizeof(state_transitions_lowalt) / sizeof(*state_transitions_lowalt)); //Die Anzahl der Eintr�ge im Array = Anzahl m�glicher �berg�nge
		for (i = 0; i < transCount; i++) //Gehe alle m�glichen �berg�nge durch, bis ein passender gefunden wurde
		{
			if (state == state_transitions_lowalt[i].source_state) //Aktueller Zustand als Ursprung(Source) gefunden
			{
				if (ret == state_transitions_lowalt[i].ret_code) //�bergebener Returnwert als valider Wert gefunden
				{
					return state_transitions_lowalt[i].dest_state; //Gebe passenden neuen Zustand(Destination) aus
				}
			}
		}
		return SYS_ERROR; //Einer oder alle der eingegebenen Parameter waren nicht in der �bergangstabelle enthalten!
	default:
		return SYS_ERROR; //Unpassender H�henparameter �bergeben!
	}
}

//Zeitfunktion: Maskiert millis() und die timeOffset-Variable, die das Offset bei Reset beh�lt
unsigned long sysTime(void)
{
	return (millis() + sysT.timeOffset);
}

//-----------------State-Funktionen-----------------

int init_waitl_state(void)
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("initWaitLong State"));
	delay(START_WAITSHORT); //Um nicht zu lange warten zu m�ssen, wird beim Debuggen WAITSHORT benutzt
#else
	delay(START_WAITLONG); //Warte lange (Einbau)
#endif // DBG_SERIALOUTPUT
	//Nach der Wartezeit: Stelle den Erststartmarker im EEPROM auf ACK, damit n�chstes Mal nur noch kurz/nicht gewartet wird
	EEPROM[0].update(0x6);
	return OK;
};

int init_waits_state(void)
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("initWaitShort State"));
#endif // DBG_SERIALOUTPUT
	delay(START_WAITSHORT); //Warte kurz (Auswurf)
	return OK;
};

int init_recall_state(void)
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("Recall State"));
#endif // DBG_SERIALOUTPUT


	//Auslesen von Statusvariablen aus dem EEPROM
	if (EEPROM[0] == 0x6) //Ist das Erststartbit = ACK? Dann kein Erststart.
	{
		if (EEPROM[1] == 0x6) //Ist das Offsetbit = ACK? Dann kein Zweitstart.
		{
			for (int i = 0; i < 4; i++) //Systemzeitoffset aus EEPROM lesen
			{
				sysT.tArray[i] = EEPROM[i + 2];
			}
			sysStatus.isTimeOffset = true; //Resetflag einschalten
			return RES; //Reset erkannt
		}
		else
		{
			return SEC; //Kein Erststart, aber auch kein Timeroffset. Daher: Zweitstart
		}
	}
	else
	{
		return FIRST; //Erststart erkannt
	}
};
int init_debugcheck_state(void)
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("Debugcheck State"));
#endif // DBG_SERIALOUTPUT
	if (analogRead(DEBUGPIN) < 20) //Ist Debugpin auf LOW?
	{
		//TODO Wartezeit oder anderen Sicherheitsmechanismus einbauen?
		return DEBUG;
	}
	else
	{
		return DEF;
	}
};
int init_start_state(void)
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("InitStart State"));
#endif // DBG_SERIALOUTPUT

//Hier wird die Peripherie eingeschaltet und getestet
	
	//SPI-Buffer initialisieren
	for (int i = 0; i < 40; i++)
	{
		from_mcu2.getbyte[i] = 0;
	}

	for (int i = 0; i < 74; i++)
	{
		to_mcu1.getbytes[i] = 0;
	}

	//SD:
	//SS-Pin der Bibliothek (D10) muss Output sein, sonst funktioniert sie nicht
	digitalWrite(SS_TC6, HIGH);
	pinMode(SS_TC6, OUTPUT);
	/*
	if (SD.begin(SS_SD)) //Callt auch gleichzeitig SPI.begin
	{
#ifdef DBG_SERIALOUTPUT
		Serial.println(F("SD found"));
#endif // DBG_SERIALOUTPUT
		sensStatus.sdWorking = true;
		*lFile = SD.open("OBC.log", FILE_WRITE);
		if (lFile)
		{
			if (sysStatus.isTimeOffset)
			{
				lFile->println("Restart");
			}
			else
			{
				lFile->println("New Run");
			}
			lFile->close();
		}
	}
	else
	{
#ifdef DBG_SERIALOUTPUT
		Serial.println(F("No SD"));
#endif // DBG_SERIALOUTPUT
	}*/

	//Restliche SPI-Pins setzen:
	digitalWrite(SS_PSENS_EXT, HIGH);
	digitalWrite(SS_PSENS_SMD, HIGH);
	digitalWrite(SS_TC1, HIGH);
	digitalWrite(SS_TC2, HIGH);
	digitalWrite(SS_TC3, HIGH);
	digitalWrite(SS_TC4, HIGH);
	digitalWrite(SS_TC5, HIGH);
	digitalWrite(SS_TC6, HIGH);
	digitalWrite(SS_MCU1, HIGH);
	digitalWrite(SS_MCU2, HIGH);

	pinMode(SS_PSENS_EXT, OUTPUT);
	pinMode(SS_PSENS_SMD, OUTPUT);
	pinMode(SS_TC1, OUTPUT);
	pinMode(SS_TC2, OUTPUT);
	pinMode(SS_TC3, OUTPUT);
	pinMode(SS_TC4, OUTPUT);
	pinMode(SS_TC5, OUTPUT);
	pinMode(SS_TC6, OUTPUT);
	pinMode(SS_MCU1, OUTPUT);
	pinMode(SS_MCU2, OUTPUT);


	//SPI initialisieren //DEBUG, anstelle SD-Karte!
	SPI.begin();
	SPI.setClockDivider(SPI_CLOCK_DIV8);

	//Interrupt-Output
	digitalWrite(INT_OUTPUT, LOW);
	pinMode(INT_OUTPUT, OUTPUT);

	//Reset-Pins
	digitalWrite(VOTE_MCU1, LOW);
	digitalWrite(VOTE_MCU2, LOW);

	pinMode(VOTE_MCU1, OUTPUT);
	pinMode(VOTE_MCU2, OUTPUT);
	//TODO: Noch mehr?

	return OK;
};

int flight_checkalt_state(void)
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("checkAlt State"));

#endif // DBG_SERIALOUTPUT

	//Nachsehen, ob die Zeit f�r einen H�henwechsel gekommen ist (und die aktuelle Iridiumnachricht f�r diese H�he vollendet ist)
	if (sysTime() > FLIGHT_WAITTOMED)
	{
		if (sysTime() > FLIGHT_WAITTOLOW)
		{
			if (cur_alt == LOWALT)
			{
				return ALT_LOW;
			}
			else
			{
				if (sysStatus.sendNum == 12)
				{
					return ALT_LOW;
				}
				else
				{
					return ALT_MED;
				}
			}
		}
		else
		{
			if (cur_alt == MEDALT)
			{
				return ALT_MED;
			}
			else
			{
				if (sysStatus.sendNum == 12)
				{
					return ALT_MED;
				}
				else
				{
					return ALT_HIGH;
				}
			}
		}
	}
	else
	{
		return ALT_HIGH;
	}
};


int flight_sync_state(void)
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("flightSync State"));
#endif // DBG_SERIALOUTPUT

	if (mcu1call)
	{
#ifdef DBG_SERIALOUTPUT
		Serial.println(F("MCU1 Flag removed"));
#endif
		mcu1call = false;
	}
	if (mcu2call)
	{
#ifdef DBG_SERIALOUTPUT
		Serial.println(F("MCU2 Flag removed"));
#endif
		mcu2call = false;
	}

	//Zur�cksetzen der Interruptflags
	mcu1flag = mcu2flag = false;


	//WICHTIG: �NDERUNG DER FSM! MCU3 steuert das Abrufen der Sensorwerte mittels interrupts!
	if (!next)
	{
		return REPEAT; //Wiederhole, falls Watchdogtimer nicht angeschlagen hat
	}
	else
	{
		next = false; //Setze Timer zur�ck
	}
	//Senden von Interrupts an beide MCUs und kurzes Warten auf Antwort
	digitalWrite(INT_OUTPUT, HIGH);
	pinMode(INT_OUTPUT, OUTPUT);
	delay(INTERRUPT_PEAKTIME);
	digitalWrite(INT_OUTPUT, LOW);
	pinMode(INT_OUTPUT, INPUT);
	delay(SYNCTIME); 

	//�berpr�fen, ob Interrupts eingetroffen sind
	mcu1flag = mcu1call;
	mcu2flag = mcu2call;
	if (mcu1flag) //MCU1 hat sich zur�ckgemeldet
	{
		if (mcu2flag)//MCU2 hat sich auch zur�ckgemeldet
		{
			//Beide haben sich gemeldet, Erfolg! Weiter zu talk1
			//Flags zur�cksetzen
			mcu1flag = mcu1call = false;
			mcu2flag = mcu2call = false;
			mcu1_int_na = false;
			mcu2_int_na = false;
#ifdef DBG_SERIALOUTPUT
			Serial.println(F("Both"));
#endif
			return OK;
		}
		else //MCU1 hat sich gemeldet, aber MCU2 nicht 
		{
#ifdef DBG_SERIALOUTPUT
			Serial.println(F("MCU1"));
#endif
			if (mcu2_int_na)//Zweiter aufeinanderfolgender Versto� f�r MCU2: MCU2 wird eingetragen
			{
				mcu2_mia = true;
				mcu1flag = mcu1call = false;
				mcu1_int_na = false;
				return FAIL; //TODO muss �berarbeitet werden, damit bei zweitem aufeinanderfolgenden Interruptfehlen nur noch ein SPI-Versuch unternommen wird und bei Misserfolg ein Reset eingeleitet wird.
				// Bei erfolgreichem SPI-Kommunizieren wird die Interrupt-Leitung als fehlerhaft gemeldet(Funktioniert das? Sync-States?)
			}
			else //erster Versto� f�r MCU2
			{
				mcu2_int_na = true;
				mcu1flag = mcu1call = false;
				mcu1_int_na = false;
				return DEF;
			}
		}
	}
	else if (mcu2flag) //MCU2 hat sich gemeldet, MCU1 aber nicht
	{
#ifdef DBG_SERIALOUTPUT
		Serial.println(F("MCU2"));
#endif
		if (mcu1_int_na) //Zweiter aufeinanderfolgender Versto� f�r MCU1: MCU1 wird eingetragen
		{
			mcu1_mia = true; //MCU1 wird eingetragen
			mcu2flag = mcu2call = false;
			mcu2_int_na = false;
			return FAIL; //TODO siehe oben FAIL
		}
		else //Erster Versto� f�r MCU1
		{
			mcu1_int_na = true;
			mcu2flag = mcu2call = false;
			mcu2_int_na = false;
			return DEF;
		}
	}
	else //Keiner der beiden MCUs hat sich zur�ckgemeldet
	{
		bool errFlag = false;
		if (mcu1_int_na) //Zweiter aufeinanderfolgender Versto� f�r MCU1: MCU1 wird eingetragen
		{
			mcu1_mia = true; //MCU1 wird eingetragen
			errFlag = true;//Hier w�rde MCU1 f�r Reset vorgemerkt TODO
		}
		else //Erster Versto� f�r MCU1
		{
			mcu1_int_na = true;
		}
		if (mcu2_int_na) //Zweiter aufeinanderfolgender Versto� f�r MCU2: MCU2 wird eingetragen
		{
			mcu2_mia = true; //MCU2 wird eingetragen
			errFlag = true; //Hier w�rde MCU2 f�r Reset vorgemerkt TODO
		}
		else //Erster Versto� f�r MCU2
		{
			mcu2_int_na = true;
		}
		if (errFlag)
		{
			return FAIL; //TODO: Durch Resetanweisung ersetzen
		}
		else
		{
			return DEF;
		}
	}
};


int flight_talk1_state(void) //Erste SPI-Kommunikation
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("flightTalk1 State"));
#endif // DBG_SERIALOUTPUT
	//Daten von MCU2 holen
	digitalWrite(SS_MCU2, LOW);
	delay(20);
	SPI.transfer(1); //Einen Leertransfer
	for (int i = 0; i < 39; i++)
	{
		from_mcu2.getbyte[i] = SPI.transfer(1);
		delayMicroseconds(20);
	}
	digitalWrite(SS_MCU2, HIGH);

	//Daten in ausgehenden Buffer verschieben
	to_mcu1.to_mcu1_buffer.time2 = from_mcu2.from_mcu2_buffer.time2;
	to_mcu1.to_mcu1_buffer.state2 = from_mcu2.from_mcu2_buffer.state2;
	to_mcu1.to_mcu1_buffer.resets2 = from_mcu2.from_mcu2_buffer.resets2;
	to_mcu1.to_mcu1_buffer.vbat = from_mcu2.from_mcu2_buffer.vbat;

	to_mcu1.to_mcu1_buffer.rotX = from_mcu2.from_mcu2_buffer.rotX;
	to_mcu1.to_mcu1_buffer.rotY = from_mcu2.from_mcu2_buffer.rotY;
	to_mcu1.to_mcu1_buffer.rotZ = from_mcu2.from_mcu2_buffer.rotZ;

	to_mcu1.to_mcu1_buffer.quatw = from_mcu2.from_mcu2_buffer.quatw;
	to_mcu1.to_mcu1_buffer.quatx = from_mcu2.from_mcu2_buffer.quatx;
	to_mcu1.to_mcu1_buffer.quaty = from_mcu2.from_mcu2_buffer.quaty;
	to_mcu1.to_mcu1_buffer.quatz = from_mcu2.from_mcu2_buffer.quatz;

	to_mcu1.to_mcu1_buffer.calib = from_mcu2.from_mcu2_buffer.calib;

	to_mcu1.to_mcu1_buffer.tp = from_mcu2.from_mcu2_buffer.tp;
	to_mcu1.to_mcu1_buffer.gpslat = from_mcu2.from_mcu2_buffer.gpslat;
	to_mcu1.to_mcu1_buffer.gpslon = from_mcu2.from_mcu2_buffer.gpslon;
	to_mcu1.to_mcu1_buffer.gpsspeed = from_mcu2.from_mcu2_buffer.gpsspeed;
	to_mcu1.to_mcu1_buffer.gpshead = from_mcu2.from_mcu2_buffer.gpshead;
	to_mcu1.to_mcu1_buffer.gpshk = from_mcu2.from_mcu2_buffer.gpshk;

#ifdef DBG_SERIALOUTPUT
	//DEBUG
	Serial.println(to_mcu1.to_mcu1_buffer.time2);
	Serial.println(to_mcu1.to_mcu1_buffer.state2);
	Serial.println(to_mcu1.to_mcu1_buffer.resets2);
	Serial.println(to_mcu1.to_mcu1_buffer.vbat);
	Serial.println(to_mcu1.to_mcu1_buffer.rotX);
	Serial.println(to_mcu1.to_mcu1_buffer.rotY);
	Serial.println(to_mcu1.to_mcu1_buffer.rotZ);
	Serial.println(to_mcu1.to_mcu1_buffer.quatw);
	Serial.println(to_mcu1.to_mcu1_buffer.quatx);
	Serial.println(to_mcu1.to_mcu1_buffer.quaty);
	Serial.println(to_mcu1.to_mcu1_buffer.quatz);
	Serial.println(to_mcu1.to_mcu1_buffer.calib);
	Serial.println(to_mcu1.to_mcu1_buffer.tp);
	Serial.println(to_mcu1.to_mcu1_buffer.gpslat);
	Serial.println(to_mcu1.to_mcu1_buffer.gpslon);
	Serial.println(to_mcu1.to_mcu1_buffer.gpsspeed);
	Serial.println(to_mcu1.to_mcu1_buffer.gpshead);
	Serial.println(to_mcu1.to_mcu1_buffer.gpshk);
#endif

	return OK;
};
int flight_talk2_state(void) //Kommunikation mit MCU1 und f�llen eigener Werte in den Buffer
{
#ifdef DBG_SERIALOUTPUT 
	Serial.println(F("flightTalk2 State")); 
#endif // DBG_SERIALOUTPUT

	//SPI-Kommunikation
	//Eigene Daten in Buffer schreiben
	to_mcu1.to_mcu1_buffer.number = sysStatus.sendNum;
	to_mcu1.to_mcu1_buffer.number2 = sysStatus.sendNum;
	to_mcu1.to_mcu1_buffer.number3 = sysStatus.sendNum;

	to_mcu1.to_mcu1_buffer.time3 = (sysTime()/1000);

	//H�henmodus einspeichern. Folgendes Schema wird benutzt, um HIGH, MED, LOW auf ein Byte zu mappen:
	// 129 (10000001)=HIGH, 66(01000010)=MED, 36(00100100)=LOW
	switch (cur_alt)
	{
	case HIGHALT:
		to_mcu1.to_mcu1_buffer.state3 = 129;
		break;
	case MEDALT:
		to_mcu1.to_mcu1_buffer.state3 = 66;
		break;
	case LOWALT:
		to_mcu1.to_mcu1_buffer.state3 = 36;
		break;
	default:
		to_mcu1.to_mcu1_buffer.state3 = 129; //Default ist HIGH
		break;
	}

	to_mcu1.to_mcu1_buffer.resets3 = 0;
	//SPI-Kommunikation
	digitalWrite(SS_MCU1, LOW);
	delay(20);
	SPI_writeAnything(to_mcu1.to_mcu1_buffer);
	digitalWrite(SS_MCU1, HIGH);

	//Eine Nachricht wurde �bermittelt: �ndern der Nachrichtenzahl
	if (sysStatus.sendNum == 12)
	{
		sysStatus.sendNum = 1;
	}
	else
	{
		sysStatus.sendNum++;
	}

/*	//DEBUG: Ausgabe der an MCU1 geschickten Daten
#ifdef DBG_SERIALOUTPUT
	Serial.println(to_mcu1.to_mcu1_buffer.number);
	Serial.println(to_mcu1.to_mcu1_buffer.number2);
	Serial.println(to_mcu1.to_mcu1_buffer.number3);
	Serial.println(to_mcu1.to_mcu1_buffer.time3);
	Serial.println(to_mcu1.to_mcu1_buffer.time2);
	Serial.println(to_mcu1.to_mcu1_buffer.state2);
	Serial.println(to_mcu1.to_mcu1_buffer.state3);
	Serial.println(to_mcu1.to_mcu1_buffer.resets2);
	Serial.println(to_mcu1.to_mcu1_buffer.resets3);
	Serial.println(to_mcu1.to_mcu1_buffer.vbat);
	Serial.println(to_mcu1.to_mcu1_buffer.psensExt);
	Serial.println(to_mcu1.to_mcu1_buffer.psensSmd);
	Serial.println(to_mcu1.to_mcu1_buffer.psensAn);
	Serial.println(to_mcu1.to_mcu1_buffer.rotX);
	Serial.println(to_mcu1.to_mcu1_buffer.rotY);
	Serial.println(to_mcu1.to_mcu1_buffer.rotZ);
	Serial.println(to_mcu1.to_mcu1_buffer.quatw);
	Serial.println(to_mcu1.to_mcu1_buffer.quatx);
	Serial.println(to_mcu1.to_mcu1_buffer.quaty);
	Serial.println(to_mcu1.to_mcu1_buffer.quatz);
	Serial.println(to_mcu1.to_mcu1_buffer.calib);
	Serial.println(to_mcu1.to_mcu1_buffer.tcs[0]);
	Serial.println(to_mcu1.to_mcu1_buffer.tcs[1]);
	Serial.println(to_mcu1.to_mcu1_buffer.tcs[2]);
	Serial.println(to_mcu1.to_mcu1_buffer.tcs[3]);
	Serial.println(to_mcu1.to_mcu1_buffer.tcs[4]);
	Serial.println(to_mcu1.to_mcu1_buffer.tcs[5]);
	Serial.println(to_mcu1.to_mcu1_buffer.cjs[0]);
	Serial.println(to_mcu1.to_mcu1_buffer.cjs[1]);
	Serial.println(to_mcu1.to_mcu1_buffer.cjs[2]);
	Serial.println(to_mcu1.to_mcu1_buffer.cjs[3]);
	Serial.println(to_mcu1.to_mcu1_buffer.cjs[4]);
	Serial.println(to_mcu1.to_mcu1_buffer.cjs[5]);
	Serial.println(to_mcu1.to_mcu1_buffer.tp);
	Serial.println(to_mcu1.to_mcu1_buffer.gpslat);
	Serial.println(to_mcu1.to_mcu1_buffer.gpslon);
	Serial.println(to_mcu1.to_mcu1_buffer.gpsspeed);
	Serial.println(to_mcu1.to_mcu1_buffer.gpshead);
	Serial.println(to_mcu1.to_mcu1_buffer.gpshk);
#endif */
	return OK;
};
int flight_save_state(void)
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("flightSave State"));
#endif // DBG_SERIALOUTPUT

	if (sensStatus.sdWorking)
	{
		//Den aktuellen Sendebuffer (1 "Sekundenpaket") als Daten auf der SD-Karte speichern
		*lFile = SD.open("OBC.log", FILE_WRITE);
		if (lFile)
		{
			for (int i = 0; i < 75; i++)
			{
				lFile->write(to_mcu1.getbytes[i]);
			}
			lFile->println();

			lFile->close();
		}
	}
	return OK;
};
int flight_memo_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("flightMemo State"));
#endif // DBG_SERIALOUTPUT

	//Die aktuelle Systemzeit im EEPROM speichern
	tBuf.time = sysTime();

	for (int i = 0; i < 4; i++) //Systemzeitoffset in EEPROM speichern
	{
		EEPROM[i + 2].update(tBuf.tArray[i]);
	}
	//Falls nicht schon geschehen: Offsetmarker ACK im EEPROM setzen
	EEPROM[1].update(0x6);
	return OK;
};
int flight_pollgen_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("flightPoll State"));
#endif // DBG_SERIALOUTPUT

	//Sensoren abrufen und Daten bereit machen:

	//Thermocouples und CJs:
	//Abrufen
	float tcvals[12];
	poll_thermocouple(tcvals);
	//Mit 10 multiplizieren
	for (int i = 0; i < 12; i++)
	{
		tcvals[i] *= 10;
	}
	//Nach int16 casten und in ausgehenden Buffer verschieben
	for (int i = 0; i < 6; i++)
	{
		to_mcu1.to_mcu1_buffer.tcs[i] = (int)tcvals[i*2];
		to_mcu1.to_mcu1_buffer.cjs[i] = (int)tcvals[(i * 2) + 1];
	}

	//Drucksensoren(nur der Externe, der interne wird auf 0 gesetzt)
	float pval = digPs_ext.Readout();
	to_mcu1.to_mcu1_buffer.psensExt = (uint16_t)pval;
	to_mcu1.to_mcu1_buffer.psensSmd = 0; //Dummywert, Sensor nicht funktionsf�hig

	//Druckwert holen, durch 4 teilen und als Byte speichern
	uint16_t paval = analogRead(PSENS_ANALOG);
	paval = paval - (paval % 4);
	paval /= 4;
	to_mcu1.to_mcu1_buffer.psensAn = paval;


	return OK;
};

int flight_med_trans(void) //�bergang in den Medium altitude mode (Einschalten des GPS, ver�ndern der Pollreihenfolge+Sendepriorit�t)
{
#ifdef DBG_SERIALOUTPUT
	 Serial.println(F("Altitude change: High to Medium"));
#endif // DBG_SERIALOUTPUT

	cur_alt = MEDALT; //H�he �ndern
	return OK;
};
int flight_low_trans(void) //�bergang in den Low altiutude mode (Ver�ndern der Pollreihenfolge+Sendepriorit�t)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("Altitude change: Medium to low"));
#endif // DBG_SERIALOUTPUT

	cur_alt = LOWALT; //H�he �ndern
	return OK;
};

int test_listen_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testListen State"));
#endif // DBG_SERIALOUTPUT

	//EEPROM l�schen, um die Warteflag zu entfernen TODO bei fertigen Testing-Codes ersetzen
	for (int i = 0; i < 8; i++) {
		EEPROM[i].update(0);
	}
	while (1); //TODO Endlosschleife einbauen, simuliert das Warten auf eine Eingabe
	//Was hier hinsollte: Setzen einer Warteschleifenflag, in der Schleifge regelm��iges Abfragen des UART nach Kommandos. Au�erdem ablaufen eines Timers, um danach in den Reset-Modus zu schalten
	return TEST_RESET; //TODO eine der 4 m�glichen Returns, um Compilerwarnung zu vermeiden
};
int test_sens_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testSens State"));
#endif // DBG_SERIALOUTPUT
};
int test_sd_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testSD State"));
#endif // DBG_SERIALOUTPUT
};
int test_spicomm_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testSPI State"));
#endif // DBG_SERIALOUTPUT
};
int test_gps_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testGPS State"));
#endif // DBG_SERIALOUTPUT
};
int test_imu_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testIMU State"));
#endif // DBG_SERIALOUTPUT
};
int test_tp_state(void)
{
#ifdef DBG_SERIALOUTPUT
Serial.println(F("testTP State"));
#endif // DBG_SERIALOUTPUT
};
int test_irid_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testIridium State"));
#endif // DBG_SERIALOUTPUT
};
int test_delete_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("delete State"));
#endif // DBG_SERIALOUTPUT
};
int test_reset_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testReset State, waiting for PowerOff"));
#endif // DBG_SERIALOUTPUT
	//Hier sollte nichts weiter passieren, da hier das FSM-Ende erreicht ist und der mainloop geleert wird. Returnwerte dieses Zustands sollten nicht ausgewertet werden.
	//int-Typ ist f�r Homogenit�t und Pointerverwendung n�tig.
	//Daher:
	return ERROR; //In der Zustandstabelle ist kein R�cksprung aus RESET vorgesehen, daher wird jeglicher weiterer Auswertungsversuch sowieso zu SYSERROR f�hren.
};
int syserror_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("System Error State"));
#endif // DBG_SERIALOUTPUT
	//TODO evtl. alle Pins inert schalten: Evtl. muss Reihenfolge des FSM-Exitsprungs doch ver�ndert werden
	//Vorschlag: Inert schalten aller Pins, dann wechsel in den Reset-State
	return ERROR; //Auch hier eigentlich nicht n�tig, da Zustandstabelle keinen R�cksprung vorsieht
};



//Interrupt Service Routinen

//Interrupttimer-ISR
ISR(TIMER1_COMPA_vect){

	if (isrCounter > 10) //Ist (mehr als) eine Sekunde vergangen?
	{
		next = true; 
		isrCounter = 0;
	}

	isrCounter++; //Counter imkrementieren
}

void int_mcu1call()
{
	mcu1call = true;
}

void int_mcu2call()
{
	mcu2call = true;
}