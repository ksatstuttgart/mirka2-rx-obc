#pragma once
//Pins f�r MCU3 - Den SPI-Master

//Interrupts: Inputs
#define INT_INPUT_MCU2 2
#define INT_INPUT_MCU1 3
//Interrupts: Output an beide anderen MCUs
#define INT_OUTPUT A0

//Sensor- und sonstige Inputs
#define S_MISO 12
#define S_MOSI 11
#define S_SCK 13

#define PSENS_ANALOG A7

#define DEBUGPIN A6 //Debugjumper

//Slave Selects
#define SS_PSENS_SMD 4 //Onboard-Drucksensor
#define SS_PSENS_EXT 1 //Externer Drucksensor (Staupunktsensor)

#define SS_SD A3

#ifndef NEWSS //Falls Code auf dem Debug-Breadboard laufen soll, muss NEWSS definiert sein.
//MCU-Slave-Selects
#define SS_MCU1 A4
#define SS_MCU2 A5
#endif

#ifdef NEWSS //NEWSS vertauscht die Slave-Selects von den ersten zwei TCs mit denen der MCUs 1 und 2. Damit kann das Test-Breadboard besser verkabelt werden.
#warning "ACHTUNG! PINFLAG IST NOCH GESETZT! NICHT AUF OBC-BOARDS VERWENDEN!"
//MCU-Slave-Selects
#define SS_MCU1 5 //NEWSS ist gesetzt, Pintausch mit SS_TC1
#define SS_MCU2 6 //NEWSS ist gesetzt, Pintausch mit SS_TC2
#define SS_TC1 A4 //NEWSS ist gesetzt, Pintausch mit SS_MCU1
#define SS_TC2 A5 //NEWSS ist gesetzt, Pintausch mit SS_MCU2
#else
#define SS_TC1 5
#define SS_TC2 6
#endif

#define SS_TC3 7
#define SS_TC4 8
#define SS_TC5 9
#define SS_TC6 10

//Resetvote-Outputs
#define VOTE_MCU2 A1
#define VOTE_MCU1 A2