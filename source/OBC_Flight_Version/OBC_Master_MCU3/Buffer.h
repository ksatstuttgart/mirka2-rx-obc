#pragma once

struct to_mcu1_buffertype
{
	byte number;
	byte number2;
	byte number3;
	uint16_t time3;
	uint16_t time2;
	byte state2;
	byte state3;
	byte resets3;
	byte resets2;
	byte vbat;
	uint16_t psensExt;
	uint16_t psensSmd;
	byte psensAn;
	int rotX;
	int rotY;
	int rotZ;
	int quatw;
	int quatx;
	int quaty;
	int quatz;
	byte calib;
	int tcs[6];
	int cjs[6];
	int tp;
	float gpslat;
	float gpslon;
	float gpsspeed;
	float gpshead;
	byte gpshk;
};

struct from_mcu2_buffertype
{
	byte garbage;
	uint16_t time2;
	byte state2;
	byte resets2;
	byte vbat;
	int rotX;
	int rotY;
	int rotZ;
	int quatw;
	int quatx;
	int quaty;
	int quatz;
	byte calib;
	int tp;
	float gpslat;
	float gpslon;
	float gpsspeed;
	float gpshead;
	byte gpshk;
};

union from_mcu2_type
{
	from_mcu2_buffertype from_mcu2_buffer;
	byte getbyte[40];
};
union to_mcu1_type
{
	to_mcu1_buffertype to_mcu1_buffer;
	byte getbytes[75];
};
