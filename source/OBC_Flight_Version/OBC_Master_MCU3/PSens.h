#pragma once

#include <Arduino.h>
#include <SPI.h>
#define CMD_RESET 0x1E // ADC reset command
#define CMD_ADC_READ 0x00 // ADC read command
#define CMD_PROM_RD 0xA0 // Prom read command

class PSens
{
public:
	PSens(uint8_t CSel, uint16_t res);
	~PSens();
	float Readout();
	float getTemp();

private:
	uint16_t C[8]; // calibration coefficients
	uint8_t chipSel;        //Chip/Slave select pin
	uint8_t OCR;       //Code f�r die Aufloesung 0->256bits 8->4096bits
	uint8_t adctime;  //Wartezeit f�r eine ADC-Wandlung
	float T; // compensated temperature value

	//F�hrt einen Reset des Drucksensors aus
	void cmd_reset(void);
	//Liest einen ADC-Wert aus
	uint32_t cmd_adc(char cmd);
	//Liest die Kalibrierungswerte
	uint16_t cmd_prom(char coef_num);

	//Berechnet die CRC4 Checksumme der Kalibrierungswerte
	uint8_t crc4(uint16_t n_prom[]);
};

