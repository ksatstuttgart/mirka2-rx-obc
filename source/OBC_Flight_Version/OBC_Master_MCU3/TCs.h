#pragma once

#include "Pins.h"

uint32_t thermo_spiread32(uint8_t cs)
{
	int i;
	// easy conversion of four uint8_ts to uint32_t
	union bytes_to_uint32 {
		uint8_t bytes[4];
		uint32_t integer;
	} buffer;

	digitalWrite(cs, LOW);
	_delay_ms(1);

	for (i = 3; i >= 0; i--) {
		buffer.bytes[i] = SPI.transfer(0x00);
	}

	digitalWrite(cs, HIGH);
	return buffer.integer;
}

void poll_thermocouple(float werte[12])
{
	int i;
	for (i = 0; i < 11; i += 2)   //F�r alle sechs Thermocouple-ICs
	{
		//SPI-Message einlesen
		uint32_t raw = thermo_spiread32((i >> 1) + SS_TC1);

		//Termocouple Wert auslesen
		int16_t tc_val = raw >> 18;
		if (raw & 0x80000000)
		{
			// Negative Wert -> Auf 16 bit erweitern
			tc_val = 0xC000 | tc_val;
		}
		//Temperatur berechnen
		float tc = tc_val;
		tc *= 0.25;       // LSB = 0.25 �C

		//Interne Temperatur auslesen
		int16_t in_val = (raw >> 4) & 0x0FFF;
		if (in_val & 0x800)
		{
			//Negativer Wert -> auf 14 bit auff�llen
			in_val = 0xF800 | in_val;
		}
		//Interne Temperatur berechnen
		float internal = in_val;
		internal *= 0.0625; // LSB = 0.0625 �C

		//Auf Fehler �berpr�fen    
		if (raw & 0x00010007) //mindestens eines der Errorbits ist High -> Fehlerhafte Messung
		{
			tc = 0;           //Thermocouple-Wert ung�ltig
		}

		if (raw & 0x00020008) //Ein oder beide reserved-bits sind high -> fehlerhafte �bertragung
		{
			tc = 0;
			internal = 0;
		}
		werte[i] = tc;
		werte[i + 1] = internal;
	}
}