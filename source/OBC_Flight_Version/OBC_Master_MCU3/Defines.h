#pragma once

//Debugflags und -Konstanten

//#define MIRKADEBUG //Definiert Prototypmodus f�r die Firmware, muss vor der finalen Aufspielung deaktiviert werden
//#define NEWSS //Definiert neue Slave Select Pins f�r das Testbreadboard

#ifdef MIRKADEBUG
#warning "ACHTUNG! DEBUGFLAG IST NOCH GESETZT! VOR FLUG ENTFERNEN!"
#define DBG_SERIALOUTPUT //Aktiviert die serielle Ausgabe und deaktiviert gleichzeitig GPS und IRIDIUM sowie den externen Drucksensor
#define DBG_STATEWAIT 1000 //Zeit, die die MCU in jedem State verbringt
#define CIRCWAIT 60 //Einfache Wartezeit, eingesetzt um die Verz�gerung der Testboardschaltung auszugleichen
#define DBG_SERIALOUTPUT //Aktiviert die serielle Ausgabe und deaktiviert gleichzeitig GPS und IRIDIUM sowie den externen Drucksensor
#endif



//Stellgr��en
#define SYNCTIME 30 //Zeit, die der Master auf die beiden Slaves wartet
#define INTERRUPT_PEAKTIME 20 //Dauer, die der Interruptpin bet�tigt wird
#define START_WAITSHORT 15000 //So lange wartet die Kapsel nach Auswurf, bis sie die Arbeit aufnimmt (in ms)
#define START_WAITLONG 300000 //So lange wartet die Kapsel beim Einbau (in ms)
#define FLIGHT_WAITTOMED 140000//330000 //So lange wartet die Kapsel, bis GPS angeschaltet wird
#define FLIGHT_WAITTOLOW 300000//530000 //So lange wartet die Kapsel, bis besonders viele GPS-Daten �bertragen werden