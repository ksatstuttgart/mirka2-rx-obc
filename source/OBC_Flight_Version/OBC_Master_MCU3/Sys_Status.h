struct systemStatus
{
	bool isTimeOffset; //Anzeiger, dass ein Zeitoffset existiert.
	byte sendNum; //Nummer der letzten Teilnachricht, die an MCU1 ging 
};

union systemTime
{
	byte tArray[4]; //Zugriffsarray 
	unsigned long int timeOffset; //Offset f�r den Systemtimer, um die h�henspezifischen Zustands�berg�nge zu steuern
};

union timeBuf
{
	byte tArray[4]; //Zugriffsarray
	unsigned long int time; //Speicherort f�r die aktuelle Systemzeit
};

struct sensorStatus
{
	//Hier "funktioniert"-Flags f�r alle Sensoren und Peripherie einf�gen, bei Recall aus EEPROM auslesen
	bool sdWorking;
};