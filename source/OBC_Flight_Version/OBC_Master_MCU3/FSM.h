#pragma once
#include "Defines.h"
// Hier werden alle FSM-Modi und die �bergangsfunktionen sowie alle Events untergebracht:

//Haupt-FSM (Alle Zust�nde, die das System einnehmen kann)
//Deklaration der State-Funktionen
int init_waitl_state(void);
int init_waits_state(void);
int init_recall_state(void);
int init_debugcheck_state(void);
int init_start_state(void);

int flight_checkalt_state(void);
int flight_sync_state(void);
int flight_talk1_state(void);
int flight_talk2_state(void);
int flight_save_state(void);
int flight_memo_state(void);
int flight_pollgen_state(void);

int flight_med_trans(void); //�bergang in den Medium altitude mode (Einschalten des GPS, ver�ndern der Pollreihenfolge+Sendepriorit�t)
int flight_low_trans(void); //�bergang in den Low altiutude mode (Ver�ndern der Pollreihenfolge+Sendepriorit�t)

//Hier m�ssen auch alle Testzust�nde/Tests vorhanden sein, die die anderen beiden MCUs durchf�hren sollen 
int test_listen_state(void);
int test_sens_state(void); //MCU3-Sensoren
int test_sd_state(void); //MCU3-SD
int test_spicomm_state(void); //SPI-Kommunikation mit MCU3 als Master
int test_gps_state(void); //MCU2-GPS
int test_imu_state(void); //MCU2-IMU
int test_tp_state(void); //MCU2-Thermopile
int test_irid_state(void); //MCU1-Iridiumtransceiver
int test_delete_state(void); //L�schen aller Daten von SD, EEPROM, weiterleiten an MCU1 und MCU2 sowie Pr�fen der Durchf�hrung
int test_reset_state(void); //Warten auf Power-Off/Reset

int syserror_state(void); //Systemfehlerzustand

//Flugzust�nde, unabh�ngig vom H�henmodus. Der untere Array und die folgende enum m�ssen exakt gleich sein!
int(*state[])(void) = {
						init_waitl_state,
						init_waits_state,
						init_recall_state,
						init_debugcheck_state,
						init_start_state,

						flight_checkalt_state,
						flight_sync_state,
						flight_talk1_state,
						flight_talk2_state,
						flight_save_state,
						flight_memo_state,
						flight_pollgen_state,

						flight_med_trans,
						flight_low_trans,

						test_listen_state,
						test_sens_state,
						test_sd_state,
						test_spicomm_state,
						test_gps_state,
						test_imu_state,
						test_tp_state,
						test_irid_state,
						test_delete_state,
						test_reset_state,

						syserror_state

};

enum OBC_STATES //Zust�nde des Systems, Unterscheidung zwischen H�henmodi wird �ber die Lookuptabelle vorgenommen (ver�nderte Reihenfolge/Priorit�t der einzelnen States)
{
	INIT_WAITL,
	INIT_WAITS,
	INIT_RECALL,
	INIT_DEBUGCHECK,
	INIT_START,

	FLIGHT_CHECKALT,
	FLIGHT_SYNC,
	FLIGHT_TALK1,
	FLIGHT_TALK2,
	FLIGHT_SAVE,
	FLIGHT_MEMORIZE,
	FLIGHT_POLLGEN,

	FLIGHT_MED_TRANS,
	FLIGHT_LOW_TRANS,

	TESTING_LISTEN,
	TESTING_SENS_CHECK,
	TESTING_SD,
	TESTING_SPICOMM,
	TESTING_GPS,
	TESTING_IMU,
	TESTING_TP,
	TESTING_IRIDIUM,
	TESTING_DELETE,
	TESTING_RESET,

	SYS_ERROR,
};

enum RET_CODES //R�ckgabewerte eines Zustandes
{ 
	MIN_RET,
	DEF, //Default: Standardaktion ohne besondere Vorkommnisse
	FIRST, //Recallspezifisch: Erststart erkannt
	SEC, //Recallspezifisch: Zweitstart erkannt (Offsettimer noch 0)
	RES, //Recallspezifisch: Start nach erst- und zweitstart erkannt (reset im Betrieb)
	OK, //State erfolgreich durchlaufen
	FAIL, //State fehlgeschlagen/Fehlerereignis aufgetreten
	DEBUG, //Debugereignis (Debugpin registriert)
	ERROR, //Kein passender Returnwert zur�ckgegeben/Kritischer Systemfehler aufgetreten
	REPEAT, //State muss noch einmal durchlaufen werden

	ALT_HIGH, //Kapsel in gro�er H�he/erst kurze Flugzeit unterwegs
	ALT_MED, //Kapsel in GPS-geeigneter H�he/l�ngere Flugzeit unterwegs
	ALT_LOW, //Kapsel in niedriger H�he/ kurz vor Missionsende unterwegs

	TEST_SENS_CHECK, //Testet alle unmittelbar angeschlossenen Sensoren (TCs, PSens), gibt Statusbericht �ber Serial aus
	TEST_SD, //Testet Zugang zur SD-Karte
	TEST_SPICOMM, //Testet Kommunikation mit MCU1 und MCU2
	TEST_GPS, //Fragt einen GPS-Test von MCU2 an und gibt die erhaltenen Ergebnisse aus
	TEST_IMU, //Fragt einen IMU-Test von MCU2 an und gibt die erhaltenen Ergebnisse aus
	TEST_TP, //Fragt einen Thermopile-Test "
	TEST_IRIDIUM, //Fragt einen Iridiumtest von MCU1 an "
	TEST_DELETE, //L�scht EEPROM, SD
	TEST_RESET, //Schaltet alles aus, sendet Shutdownkommandos an MCU1 und 2 und macht sich bereit f�r Power-Off
	MAX_RET
};

enum ALTITUDE //H�henmodus des MCUs
{
	HIGHALT,
	MEDALT,
	LOWALT
};

struct transition {
	OBC_STATES source_state;
	RET_CODES   ret_code;
	OBC_STATES dest_state;
};

//Transition-Funktion:
OBC_STATES lookup_transitions(OBC_STATES state, ALTITUDE alt, RET_CODES ret);

transition state_transitions_default[] = 
{
	{ INIT_DEBUGCHECK, DEF, INIT_RECALL },
	{ INIT_DEBUGCHECK, DEBUG, TESTING_LISTEN }, //Achtung, im Moment k�nnte die Kapsel durch einen Debugpin-Fehler inert geschaltet werden!
	{ INIT_RECALL, FIRST, INIT_WAITL }, //Erststart erkannt, MCU wartet lange
	{ INIT_RECALL, SEC, INIT_WAITS }, //Zweitstart erkannt, MCU wartet kurz (Auswurf/Kurze Stabilisierung)
	{ INIT_RECALL, RES, INIT_START }, //Reset erkannt, kein Erststart und ein Offsettimer !=0 wurde ausgelesen
	{ INIT_WAITL, OK, TESTING_RESET }, //Nach der langen Wartezeit wird automatisch abgeschaltet
	{ INIT_WAITS, OK, INIT_START },
	{ INIT_START, OK, FLIGHT_CHECKALT},
	
	{ FLIGHT_SYNC, OK, FLIGHT_TALK1 }, //Beide MCUs haben sich gemeldet
	{ FLIGHT_SYNC, DEF, FLIGHT_TALK1 }, //TODO siehe INIT_SYNC, einer der MCUs hat sich nicht gemeldet
	{ FLIGHT_SYNC, FAIL, FLIGHT_TALK1}, //TODO �ndern auf RESET f�r fehlenden MCU (Systemstatus anpassen, passenden Resetpin schalten) //TODO �bergang muss wieder auf ERROR ge�ndert werden
	{ FLIGHT_SYNC, ERROR, SYS_ERROR },
	{ FLIGHT_SYNC, REPEAT, FLIGHT_SYNC},
	{ FLIGHT_TALK1, OK, FLIGHT_TALK2 },
	{ FLIGHT_TALK2, OK, FLIGHT_SAVE },
	{ FLIGHT_SAVE, OK, FLIGHT_MEMORIZE },
	{ FLIGHT_MEMORIZE, OK, FLIGHT_CHECKALT },
	{ FLIGHT_POLLGEN, OK, FLIGHT_SYNC },
	
	{ FLIGHT_CHECKALT, ALT_HIGH, FLIGHT_POLLGEN },
	{ FLIGHT_CHECKALT, ALT_MED, FLIGHT_MED_TRANS },
	{ FLIGHT_CHECKALT, ALT_LOW, FLIGHT_LOW_TRANS },

	{ TESTING_LISTEN, TEST_GPS, TESTING_GPS },
	{ TESTING_LISTEN, TEST_IRIDIUM, TESTING_IRIDIUM },
	{ TESTING_LISTEN, TEST_SENS_CHECK, TESTING_SENS_CHECK },
	{ TESTING_LISTEN, TEST_RESET, TESTING_RESET },
	{ TESTING_GPS, OK, TESTING_LISTEN },
	{ TESTING_IRIDIUM, OK, TESTING_LISTEN },
	{ TESTING_SENS_CHECK, OK, TESTING_LISTEN } 
};

transition state_transitions_medalt[] = //Nur FLIGHT-Zustands�nderungen, da INIT und TESTING bereits in default stehen
{
	{ FLIGHT_SYNC, OK, FLIGHT_TALK1 },
	{ FLIGHT_SYNC, DEF, FLIGHT_TALK1 }, //TODO siehe INIT_SYNC
	{ FLIGHT_SYNC, FAIL, FLIGHT_TALK1 }, //TODO �ndern auf RESET f�r fehlenden MCU (Systemstatus anpassen, passenden Resetpin schalten)
	{ FLIGHT_SYNC, ERROR, SYS_ERROR },
	{ FLIGHT_SYNC, REPEAT, FLIGHT_SYNC },
	{ FLIGHT_TALK1, OK, FLIGHT_TALK2 },
	{ FLIGHT_TALK2, OK, FLIGHT_SAVE },
	{ FLIGHT_SAVE, OK, FLIGHT_MEMORIZE },
	{ FLIGHT_MEMORIZE, OK, FLIGHT_CHECKALT },
	{ FLIGHT_POLLGEN, OK, FLIGHT_SYNC },

	{ FLIGHT_CHECKALT, ALT_MED, FLIGHT_POLLGEN },
	{ FLIGHT_CHECKALT, ALT_LOW, FLIGHT_LOW_TRANS },

	{ FLIGHT_MED_TRANS, OK, FLIGHT_POLLGEN }
};

transition state_transitions_lowalt[] =
{
	{ FLIGHT_SYNC, OK, FLIGHT_TALK1 },
	{ FLIGHT_SYNC, DEF, FLIGHT_TALK1 }, //TODO siehe INIT_SYNC
	{ FLIGHT_SYNC, FAIL, FLIGHT_TALK1 }, //TODO �ndern auf RESET f�r fehlenden MCU (Systemstatus anpassen, passenden Resetpin schalten)
	{ FLIGHT_SYNC, ERROR, SYS_ERROR },
	{ FLIGHT_SYNC, REPEAT, FLIGHT_SYNC },
	{ FLIGHT_TALK1, OK, FLIGHT_TALK2 },
	{ FLIGHT_TALK2, OK, FLIGHT_SAVE },
	{ FLIGHT_SAVE, OK, FLIGHT_MEMORIZE },
	{ FLIGHT_MEMORIZE, OK, FLIGHT_CHECKALT },
	{ FLIGHT_POLLGEN, OK, FLIGHT_SYNC },

	{ FLIGHT_CHECKALT, ALT_LOW, FLIGHT_POLLGEN },
	{ FLIGHT_LOW_TRANS, OK, FLIGHT_POLLGEN }
};

#define EXIT_STATE TESTING_RESET
#define ENTRY_STATE INIT_DEBUGCHECK
