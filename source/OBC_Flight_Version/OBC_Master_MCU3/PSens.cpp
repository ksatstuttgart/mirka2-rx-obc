#include "PSens.h"


PSens::PSens(uint8_t CSel, uint16_t res)
{
	chipSel = CSel;        //Chip/Slave select pin
	OCR = 0;       //Code f�r die Aufloesung 0->256bits 8->4096bits
	adctime = 10;  //Wartezeit f�r eine ADC-Wandlung
}

PSens::~PSens()
{
}

float PSens::Readout()
{
	// put your main code here, to run repeatedly:
	uint32_t D1 = cmd_adc(0x40 + OCR); // read uncompensated pressure
	uint32_t D2 = cmd_adc(0x50 + OCR); // read uncompensated temperature
	// calcualte 1st order pressure and temperature (MS5607 1st order algorithm)
	float dT = D2 - C[5] * pow(2, 8);
	float OFF = C[2] * pow(2, 17) + dT*C[4] / pow(2, 6);
	float SENS = C[1] * pow(2, 16) + dT*C[3] / pow(2, 7);
	T = (2000 + (dT*C[6]) / pow(2, 23)) / 100;
	float P = (((D1*SENS) / pow(2, 21) - OFF) / pow(2, 15)) / 100;
	return P;
}
float PSens::getTemp()
{
	return T;
}

//F�hrt einen Reset des Drucksensors aus
void PSens::cmd_reset(void)
{
	digitalWrite(chipSel, LOW); // pull CSB low to start the command
	SPI.transfer(CMD_RESET); // send reset sequence
	delay(3); // wait for the reset sequence timing
	digitalWrite(chipSel, HIGH); // pull CSB high to finish the command
}
//Liest einen ADC-Wert aus
uint32_t PSens::cmd_adc(char cmd)
{
	uint32_t temp = 0;
	digitalWrite(chipSel, LOW); // pull CSB low
	SPI.transfer(cmd); // send conversion command
	delay(adctime);
	digitalWrite(chipSel, HIGH); // pull CSB high to finish the conversion
	digitalWrite(chipSel, LOW); // pull CSB low to start new command
	SPI.transfer(CMD_ADC_READ); // send ADC read command  
	temp = 65536 * SPI.transfer(0x00); // send 0 to read 1st byte (MSB);
	temp = temp + 256 * SPI.transfer(0x00); // send 0 to read 2nd byte;
	temp = temp + SPI.transfer(0x00); // send 0 to read 3rd byte (LSB);
	digitalWrite(chipSel, HIGH); // pull CSB high to finish the read command
	return temp;
}
//Liest die Kalibrierungswerte
uint16_t PSens::cmd_prom(char coef_num)
{
	uint16_t rC = 0;
	digitalWrite(chipSel, LOW); // pull CSB low
	SPI.transfer(CMD_PROM_RD + coef_num * 2); // send PROM READ command  
	rC = 256 * SPI.transfer(0x00); // send 0 to read the MSB 
	rC = rC + SPI.transfer(0x00); // send 0 to read the LSB
	digitalWrite(chipSel, HIGH); // pull CSB high
	return rC;
}

//Berechnet die CRC4 Checksumme der Kalibrierungswerte
uint8_t PSens::crc4(uint16_t n_prom[])
{
	int16_t cnt; // simple counter
	uint16_t n_rem; // crc reminder
	uint16_t crc_read; // original value of the crc
	uint8_t n_bit;
	n_rem = 0x00;
	crc_read = n_prom[7]; //save read CRC
	n_prom[7] = (0xFF00 & (n_prom[7])); //CRC byte is replaced by 0
	for (cnt = 0; cnt < 16; cnt++) // operation is performed on bytes
	{ // choose LSB or MSB
		if (cnt % 2 == 1) n_rem ^= (unsigned short)((n_prom[cnt >> 1]) & 0x00FF);
		else n_rem ^= (unsigned short)(n_prom[cnt >> 1] >> 8);
		for (n_bit = 8; n_bit > 0; n_bit--)
		{
			if (n_rem & (0x8000))
			{
				n_rem = (n_rem << 1) ^ 0x3000;
			}
			else
			{
				n_rem = (n_rem << 1);
			}
		}
	}
	n_rem = (0x000F & (n_rem >> 12)); // // final 4-bit reminder is CRC code
	n_prom[7] = crc_read; // restore the crc_read to its original place
	return (n_rem ^ 0x00);
}