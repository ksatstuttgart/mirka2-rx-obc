#pragma once

template <typename T> unsigned int SPI_writeAnything(const T& value)
{
	const byte * p = (const byte*)&value;
	unsigned int i;
	for (i = 0; i < sizeof value; i++)
		SPI.transfer(*p++);
	return i;
}  // end of SPI_writeAnything

template <typename T> unsigned int SPI_readAnything(T& value)
{
	byte * p = (byte*)&value;
	unsigned int i;
	for (i = 0; i < sizeof value; i++)
		*p++ = SPI.transfer(0);
	return i;
}  // end of SPI_readAnything


template <typename T> unsigned int SPI_exchange_ISR(T& reader, T& writer)
{
	byte * r = (byte*)&reader;
	const byte * w = (const byte*)&writer;
	unsigned int i;
	*r++ = SPDR;  // get first byte
	SPDR = *w++; //Erstes Byte zur�ckgeben
	for (i = 1; i < sizeof reader; i++)
		*r++ = SPI.transfer(*w++);
	return i;
} 