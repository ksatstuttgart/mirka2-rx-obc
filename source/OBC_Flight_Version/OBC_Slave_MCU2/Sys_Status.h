struct systemStatus
{
	byte notFirstRun; //Anzeiger, dass das System nicht das erste mal gestartet wurde; InitWait wartet damit kurz oder gar nicht statt lang.
	byte isTimeOffset; //Anzeiger, dass ein Zeitoffset existiert.
};

union systemTime
{
	byte tArray[4]; //Zugriffsarray 
	unsigned long int timeOffset; //Offset f�r den Systemtimer, um die h�henspezifischen Zustands�berg�nge zu steuern
};

union timeBuf
{
	byte tArray[4]; //Zugriffsarray
	unsigned long int time; //Speicherort f�r die aktuelle Systemzeit
};

struct sensorStatus
{
	//Hier "funktioniert"-Flags f�r alle Sensoren und Peripherie einf�gen, bei Recall aus EEPROM auslesen
	bool imuWorking;
	bool gpsOn;
};

struct buzzerstruct
{
	bool beepfast; //Sorgt f�r ein schnelles Piepen
	bool beepvslow; //Sorgt f�r sehr langsames Piepen
	bool beepslow; //Sorgt f�r ein langsames Piepen
	bool beepcont; //Sorgt f�r einen kontienuierlichen Piepton
	volatile bool isOn; //Ist der Buzzer eingeschaltet?
	volatile int fastCounter; //Z�hler f�r die ISR
	volatile int slowCounter; //"
	volatile int vslowCounter; //"
};

enum BUZZERSTATE //Auf welche Arten kann der Buzzer piepen?
{
	VERYSLOW,
	SLOW,
	FAST,
	CONT,
	OFF
};