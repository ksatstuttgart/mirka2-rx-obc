struct to_mcu3_buffertype
{
	byte garbage;
	uint16_t time2;
	byte state2;
	byte resets2;
	byte vbat;
	int rotX;
	int rotY;
	int rotZ;
	int quatw;
	int quatx;
	int quaty;
	int quatz;
	byte calib;
	int tp;
	float gpslat;
	float gpslon;
	float gpsspeed;
	float gpshead;
	byte gpshk;
	byte garbage2;
};

union spi_buffertype
{
	to_mcu3_buffertype to_mcu3_buffer;
	byte getbyte[41];
};