#pragma once

//Pins f�r MCU2 - Die IMU-MCU

//Interrupts: Inputs
#define INT_INPUT_MCU1 2
#define INT_INPUT_MCU3 3

//GPS-Pins
#define GPS_ONOFF 4


//Interrupts: Output an beide anderen MCUs
#define INT_OUTPUT A0

//Resetvotes
#define VOTE_MCU1 A1
#define VOTE_MCU3 A2

//Beacon-Aktivierung (Optional)
#define BEACON_ENABLE A3

//I2C-Lines f�r die IMU und das Thermopile
#define IMU_SDA A4
#define IMU_SCL A5

//Buzzer-Aktivierung
#define BUZZER_ONOFF 9

//SPI-Pins
#define S_SS 10
#define S_MOSI 11
#define S_MISO 12
#define S_SCK 13

#define VBAT_AIN A7 //Analoger Input f�r den Spannungsteiler, an dem sich die Batteriespannung ablesen l�sst
#define DEBUGPIN A6 //Debugjumper


