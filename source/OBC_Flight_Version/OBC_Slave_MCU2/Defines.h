#pragma once

//Debugflags und -Konstanten

//#define MIRKADEBUG //Definiert Prototypmodus f�r die Firmware, muss vor der finalen Aufspielung deaktiviert werden

#ifdef MIRKADEBUG
#warning "ACHTUNG! DEBUGFLAG IST NOCH GESETZT! VOR FLUG ENTFERNEN!"
#define DBG_SERIALOUTPUT //Aktiviert die serielle Ausgabe und deaktiviert gleichzeitig GPS und IRIDIUM sowie den externen Drucksensor
#define DBG_SERIALOUTPUT //Aktiviert die serielle Ausgabe und deaktiviert gleichzeitig GPS und IRIDIUM sowie den externen Drucksensor
#define DBG_STATEWAIT 1000 //Zeit, die die MCU in jedem State verbringt
#endif



//Stellgr��en
#define SYNCTIME 50 //Zeit, die der Master auf die beiden Slaves wartet
#define INTERRUPT_PEAKTIME 20 //Dauer, die der Interruptpin bet�tigt wird
#define START_WAITSHORT 12000 //So lange wartet die Kapsel nach Auswurf, bis sie die Arbeit aufnimmt (in ms)
#define START_WAITLONG 300000 //So lange wartet die Kapsel nach Auswurf, bis sie die Arbeit aufnimmt (in ms)
#define FLIGHT_WAITTOMED 140000 //So lange wartet die Kapsel, bis GPS angeschaltet wird
#define FLIGHT_WAITTOLOW 300000 //So lange wartet die Kapsel, bis besonders viele GPS-Daten �bertragen werden

#define SPI_TALKTIME 300 //So lange wartet MCU2 auf die SPI-Kommunikation, bevor neue Werte ausgelesen werden

#define RESETRATE 100 //Alle 10 Sekunden wird der Restcounter zur�ckgesetzt, um einen �berlauf zu verhindern