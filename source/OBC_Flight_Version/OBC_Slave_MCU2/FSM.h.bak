#pragma once
#include "Defines.h"
// Hier werden alle FSM-Modi und die �bergangsfunktionen sowie alle Events untergebracht:

// Unter-FSMs f�r die einzelnen Hauptmodi (Low-Level States), gesammelt
//Haupt-FSM (High-Level States)

int init_wait_state(void);
int init_recall_state(void);
int init_debugcheck_state(void);
int init_sync_state(void);

int flight_checkalt_state(void);
int flight_sync_h_state(void);
int flight_talk1_h_state(void);
int flight_talk2_h_state(void);
int flight_save_h_state(void);
int flight_memo_h_state(void);
int flight_poll_h_state(void);

int flight_sync_m_state(void);
int flight_talk1_m_state(void);
int flight_talk2_m_state(void);
int flight_save_m_state(void);
int flight_memo_m_state(void);
int flight_poll_m_state(void);

int flight_sync_l_state(void);
int flight_talk1_l_state(void);
int flight_talk2_l_state(void);
int flight_save_l_state(void);
int flight_memo_l_state(void);
int flight_poll_l_state(void);

int test_listen_state(void);
int test_sens_state(void);
int test_gps_state(void);
int test_irid_state(void);
int test_reset_state(void);

int syserror_state(void);

//Der untere Array und die folgende enum m�ssen exakt gleich sein!
int(*state[])(void) = {
						init_wait_state,
						init_recall_state,
						init_debugcheck_state,
						init_sync_state,
						flight_checkalt_state,

						flight_sync_h_state,
						flight_talk1_h_state,
						flight_talk2_h_state,
						flight_save_h_state,
						flight_memo_h_state,
						flight_poll_h_state,
						
						flight_sync_m_state,
						flight_talk1_m_state,
						flight_talk2_m_state,
						flight_save_m_state,
						flight_memo_m_state,
						flight_poll_m_state,
						
						flight_sync_l_state,
						flight_talk1_l_state,
						flight_talk2_l_state,
						flight_save_l_state,
						flight_memo_l_state,
						flight_poll_l_state,

						test_listen_state,
						test_sens_state,
						test_gps_state,
						test_irid_state,
						test_reset_state,

						syserror_state,

};

enum OBC_STATES //Zust�nde, WIP: Hierarchie noch nicht implementiert, daher viele States
{
	INIT_WAIT,
	INIT_RECALL,
	INIT_DEBUGCHECK,
	INIT_SYNC,

	FLIGHT_CHECKALT,

	FLIGHT_SYNC_H,
	FLIGHT_TALK1_H,
	FLIGHT_TALK2_H,
	FLIGHT_SAVE_H,
	FLIGHT_MEMORIZE_H,
	FLIGHT_POLL_H,

	FLIGHT_SYNC_M,
	FLIGHT_TALK1_M,
	FLIGHT_TALK2_M,
	FLIGHT_SAVE_M,
	FLIGHT_MEMORIZE_M,
	FLIGHT_POLL_M,
	
	FLIGHT_SYNC_L,
	FLIGHT_TALK1_L,
	FLIGHT_TALK2_L,
	FLIGHT_SAVE_L,
	FLIGHT_MEMORIZE_L,
	FLIGHT_POLL_L,

	TESTING_LISTEN,
	TESTING_SENS_CHECK,
	TESTING_GPS,
	TESTING_IRIDIUM,
	TESTING_RESET,

	SYS_ERROR,
};

enum RET_CODES //R�ckgabewerte eines Zustandes
{ 
	MIN_RET,
	DEF, //Default: Standardaktion ohne besondere Vorkommnisse
	OK, //State erfolgreich durchlaufen
	FAIL, //State fehlgeschlagen/Fehlerereignis aufgetreten
	DEBUG, //Debugereignis (Debugpin registriert)
	REPEAT, //State muss noch einmal durchlaufen werden

	ALT_HIGH, //Kapsel in gro�er H�he/erst kurze Flugzeit unterwegs
	ALT_MED, //Kapsel in GPS-geeigneter H�he/l�ngere Flugzeit unterwegs
	ALT_LOW, //Kapsel in niedriger H�he/ kurz vor Missionsende unterwegs

	TEST_GPS, //Schaltet GPS ein, schaltet sonst alles aus
	TEST_IRIDIUM, //Schaltet Iridium ein, wartet auf gutes Signal, sendet 2 Testnachrichten, schaltet dann wieder ab
	TEST_SENS_CHECK, //Testet alle angeschlossenen Sensoren, gibt Statusbericht �ber Serial aus
	TEST_RESET, //Schaltet alles aus, l�scht den EEPROM und macht sich bereit f�rs Abschalten
	MAX_RET
};

struct transition {
	OBC_STATES source_state;
	RET_CODES   ret_code;
	OBC_STATES dest_state;
};

//Transition-Funktion:
OBC_STATES lookup_transitions(OBC_STATES state, RET_CODES ret);

//Tabelle der Zustands�berg�nge: WICHTIG, immer die Tabellengr��e aktuell halten! Ablesen direkt unter transition
#define TRANS_SIZE 35

transition state_transitions[] = 
{
	{ INIT_WAIT, DEF, INIT_DEBUGCHECK },
	{ INIT_DEBUGCHECK, DEBUG, TESTING_LISTEN }, //Achtung, im Moment k�nnte die Kapsel durch einen Debugpin-Fehler inert geschaltet werden!
	{ INIT_DEBUGCHECK, DEF, INIT_RECALL },
	{ INIT_RECALL, OK, FLIGHT_CHECKALT },
	{ INIT_RECALL, FAIL, SYS_ERROR },
	{ INIT_RECALL, DEF, INIT_SYNC },
	{ INIT_SYNC, DEF, FLIGHT_SYNC_H },
	
	{ FLIGHT_SYNC_H, OK, FLIGHT_TALK1_H },
	{ FLIGHT_TALK1_H, OK, FLIGHT_TALK2_H },
	{ FLIGHT_TALK2_H, OK, FLIGHT_SAVE_H },
	{ FLIGHT_SAVE_H, OK, FLIGHT_MEMORIZE_H },
	{ FLIGHT_MEMORIZE_H, OK, FLIGHT_POLL_H },
	{ FLIGHT_POLL_H, OK, FLIGHT_CHECKALT },

	{ FLIGHT_SYNC_M, OK, FLIGHT_TALK1_M },
	{ FLIGHT_TALK1_M, OK, FLIGHT_TALK2_M },
	{ FLIGHT_TALK2_M, OK, FLIGHT_SAVE_M },
	{ FLIGHT_SAVE_M, OK, FLIGHT_MEMORIZE_M },
	{ FLIGHT_MEMORIZE_M, OK, FLIGHT_POLL_M },
	{ FLIGHT_POLL_M, OK, FLIGHT_CHECKALT },

	{ FLIGHT_SYNC_L, OK, FLIGHT_TALK1_L },
	{ FLIGHT_TALK1_L, OK, FLIGHT_TALK2_L },
	{ FLIGHT_TALK2_L, OK, FLIGHT_SAVE_L },
	{ FLIGHT_SAVE_L, OK, FLIGHT_MEMORIZE_L },
	{ FLIGHT_MEMORIZE_L, OK, FLIGHT_POLL_L },
	{ FLIGHT_POLL_L, OK, FLIGHT_CHECKALT },
	
	{ FLIGHT_CHECKALT, ALT_HIGH, FLIGHT_SYNC_H },
	{ FLIGHT_CHECKALT, ALT_MED, FLIGHT_SYNC_M },
	{ FLIGHT_CHECKALT, ALT_LOW, FLIGHT_SYNC_L },

	{ TESTING_LISTEN, TEST_GPS, TESTING_GPS },
	{ TESTING_LISTEN, TEST_IRIDIUM, TESTING_IRIDIUM },
	{ TESTING_LISTEN, TEST_SENS_CHECK, TESTING_SENS_CHECK },
	{ TESTING_LISTEN, TEST_RESET, TESTING_RESET },
	{ TESTING_GPS, OK, TESTING_LISTEN },
	{ TESTING_IRIDIUM, OK, TESTING_LISTEN },
	{ TESTING_SENS_CHECK, OK, TESTING_LISTEN } 
};

#define EXIT_STATE TESTING_RESET
#define ENTRY_STATE INIT_WAIT


//M�gliche Events
enum EVENT_OBC
{
	NO_EVENT, //Benutzt um die Initialisierung der Warteschlange zu 0 auszunutzen

	TIMER_END_1, //Ablauf des Einmaltimers
	TIMER_END_2,
	TIMER_END_3,

	TIMER_OFLOW_1, //"Tick" des wiederkehrenden Timers
	TIMER_OFLOW_2,
	TIMER_OFLOW_3,

	INT_EXT_MCU1, //Externe Interrupts sind eingegangen
	INT_EXT_MCU2,

	EVENTS_COUNT //Benutzt um die maximale Anzahl an Events darzustellen
};

struct Event_Queue //Event-Warteschlange
{
	short counter;
	EVENT_OBC events[MAX_EVENTQUEUE];
	bool isEmpty;
	bool isFull;
};

EVENT_OBC event; //
Event_Queue event_queue; //TODO Initialisierung vollst�ndig machen