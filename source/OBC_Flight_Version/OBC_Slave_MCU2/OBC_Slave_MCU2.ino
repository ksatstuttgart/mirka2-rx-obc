#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <TinyGPS.h>
#include <EEPROM.h>
#include <Wire.h>
#include <SPI.h>
#include "FSM.h"
#include "avr\wdt.h"
#include "Pins.h"
#include "Defines.h"
#include "Sys_Status.h"
#include "utility\imumaths.h"
#include "Buffer.h"

OBC_STATES cur_state = ENTRY_STATE; //Anfangsstatus festlegen
ALTITUDE cur_alt = HIGHALT; //Anfangsh�he festlegen
RET_CODES retVal = ERROR; //Returncode-Variable erstellen, hier als Error falls die erste Pointerzuordnung gleich fehlschl�gt

systemStatus sysStatus = { 0, 0 }; //System-Statusstruktur erstellen, enth�lt Erststartindikator und Zeitoffset
sensorStatus sensStatus = { false, false};
systemTime sysT; //Speicher f�r die Systemzeit
timeBuf tBuf; //Buffer f�r den EEPROM-Speichervorgang

//SPI-Buffer erstellen 
spi_buffertype spi_buffer;
//SPI-Counter erstellen
byte c = 0;
uint16_t pos = 0;
//Sensoren erstellen
TinyGPS sens_gps;
Adafruit_BNO055 sens_imu = Adafruit_BNO055();

int(*state_fun)(void) = state[cur_state]; //Funktionspointer zum Ausf�hren der Zust�nde erstellen, mit Anfangszustand belegen
int retbuf = 0; //Buffer f�r R�ckgabewerte erstellen, Wert 0 ist nicht in der �bergangstabelle enthalten -> F�hrt zu �bergang in SYSERROR-Mode wenn kein neuer Wert vor der ersten Verwendung eingespielt wird

buzzerstruct buzzer = { false, false, false, false, false, 0, 0 }; //Buzzervariablen erzeugen
//Interrupttimer-Z�hler
volatile int isrCounter = 0;

//Generelle Flags
bool doNothing = false; //Benutzt, um die MCUs inert zu schalten, wenn sie nichts mehr zu tun haben
volatile bool mcu1call = false; //Flag: MCU1 hat den externen Interrupt ausgel�st
volatile bool mcu3call = false; //...
bool mcu1flag = false; //Flags, die die Werte von mcu1call/mcu2call bei Abruf "einfrieren", damit sie innnerhalb eines Funktionsdurchlaufes konstant bleiben
bool mcu3flag = false; //"

//TODO: Evtl. in struct verschieben
short spifail_mcu1 = 0; //Z�hlen die Misserfolge in Serie bei der Kommunikation mit den MCUs
short spifail_mcu3 = 0;
short syncfail_mcu1 = 0; //Z�hlen die Sync-Misserfolge
short syncfail_mcu3 = 0;

//TODO: "Vorstrafenregister" der anderen beiden MCUs, hier wird gesammelt. Anhand der gesammelten Informationen kann entschieden werden. Auch in struct
bool mcu1_int_na = false; //Interruptleitung von MCU1 zu 2 hat beim letzten Versuch nicht funktioniert
bool mcu3_int_na = false; //...3 zu 2

bool mcu1_mia = false; //MCU1 meldet sich nicht �ber Interrupt, Ergebnisse der SPI-Kommunikation werden abgewartet
bool mcu3_mia = false; //MCU3...

void setup()
{
	//Alle Pins inert schalten
	pinMode(INT_INPUT_MCU1, INPUT); //D2
	pinMode(INT_INPUT_MCU3, INPUT); //D3
	pinMode(GPS_ONOFF, INPUT); //D4
	pinMode(5, INPUT); //D5, NICHT BELEGT
	pinMode(6, INPUT); //D6, NICHT BELEGT
	pinMode(7, INPUT); //D7, NICHT BELEGT
	pinMode(8, INPUT); //D8, NICHT BELEGT
	pinMode(BUZZER_ONOFF, OUTPUT); //D9
	/*pinMode(S_SS, INPUT); //D10
	pinMode(S_MOSI, INPUT); //D11
	pinMode(S_MISO, INPUT); //D12
	pinMode(S_SCK, INPUT); //D13 */

	pinMode(INT_OUTPUT, INPUT); //A0
	pinMode(VOTE_MCU1, INPUT); //A1
	pinMode(VOTE_MCU3, INPUT); //A2
	pinMode(BEACON_ENABLE, INPUT); //A3, optional bzw. nicht mehr benutzt
	pinMode(IMU_SDA, INPUT); //A4
	pinMode(IMU_SCL, INPUT); //A5
	pinMode(DEBUGPIN, INPUT); //A6
	pinMode(VBAT_AIN, INPUT); //A7
	digitalWrite(INT_INPUT_MCU1, LOW); //D2
	digitalWrite(INT_INPUT_MCU3, LOW); //D3
	digitalWrite(GPS_ONOFF, LOW); //D4
	digitalWrite(5, LOW); //D5, NICHT BELEGT
	digitalWrite(6, LOW); //D6, NICHT BELEGT
	digitalWrite(7, LOW); //D7, NICHT BELEGT
	digitalWrite(8, LOW); //D8, NICHT BELEGT
	digitalWrite(BUZZER_ONOFF, LOW); //D9
	digitalWrite(S_SS, LOW); //D10
	digitalWrite(S_MOSI, LOW); //D11
	digitalWrite(S_MISO, LOW); //D12
	digitalWrite(S_SCK, LOW); //D13

	digitalWrite(INT_OUTPUT, LOW); //A0
	digitalWrite(VOTE_MCU1, LOW); //A1
	digitalWrite(VOTE_MCU3, LOW); //A2
	digitalWrite(BEACON_ENABLE, LOW); //A3
	digitalWrite(IMU_SDA, LOW); //A4
	digitalWrite(IMU_SCL, LOW); //A5
	digitalWrite(DEBUGPIN, LOW); //A6
	digitalWrite(VBAT_AIN, LOW); //A7

#ifdef DBG_SERIALOUTPUT
	//Serial.begin(38400);
	pinMode(0, INPUT);
	digitalWrite(0, LOW);
#endif // DBG_SERIALOUTPUT

	//Watchdogtimer mit einer Laufzeit von 8 Sekunden stellen (Beliebig gew�hlte Zeit, muss evtl. ver�ndert werden)
	//wdt_enable(WDTO_8S); //TODO
	//Externe Interrupts anschlie�en
	attachInterrupt(digitalPinToInterrupt(INT_INPUT_MCU1), int_mcu1call, RISING);
	attachInterrupt(digitalPinToInterrupt(INT_INPUT_MCU3), int_mcu3call, RISING);

	//Systemzeit auf 0 initialisieren
	sysT.timeOffset = 0;

	//Serielle Schnittstelle f�r GPS initialisieren
	Serial.begin(57600);

	//Timer-Interrupt f�r getimte Tasks setzen, Frequenz 10 Hz (Alle 100 Millisekunden) ausgel�st durch Timer1
	cli(); // F�r die Dauer der Interrupteinstellungen alle Interrupts ausschalten
	TCCR1A = 0;// Das TCCR1A-Register auf 0 setzen
	TCCR1B = 0;// s.o., f�r TCCR1B
	TCNT1 = 0;// Z�hler auf 0 setzen
	// Das compare-match-register f�r Inkrement alle 2 Sekunden setzen
	OCR1A = 6249;// = (16*10^6) / (10*256) - 1 (muss <65536 sein)
	// Clear Timer on Compare Match Modus setzen
	TCCR1B |= (1 << WGM12);
	//CS12-Bit setzen, um einen Prescaler von 256 zu erhalten (s.o.)
	TCCR1B |= (1 << CS12);
	// Den Interrupt (Timer Compare) einschalten
	TIMSK1 |= (1 << OCIE1A);
	sei(); //Nach Ende der Einstellungen Interrupts wieder einschalten
}

void loop()
{
	if (!doNothing)
	{
		retbuf = state_fun(); //F�hre aktuellen Zustand aus und erhalte R�ckgabewert
		if (retbuf > MIN_RET && retbuf < MAX_RET) //Pr�fe G�ltigkeit des Returnwerts
		{
			retVal = (RET_CODES)retbuf; //Konvertiere zu Returnwert-Typ 
		}
		cur_state = lookup_transitions(cur_state, cur_alt, retVal); //Zustand wechseln
		state_fun = state[cur_state]; //Zustands-Funktionspointer aktualisieren
		if (EXIT_STATE == cur_state) //Ist letzter Zustand der FSM erreicht, schalte FSM-Ausf�hrung VOR BEGINN DES ENDZUSTANDS aus
		{
			doNothing = true;
		}
	}
	//wdt_reset(); //Watchdog-Reset TODO Siehe andere wdt-Zeile
}

//�bergangs-Funktion:
OBC_STATES lookup_transitions(OBC_STATES state, ALTITUDE alt, RET_CODES ret)
{
	int i; //Z�hler
	int transCount = 0;

	switch (alt) //Analysiere die aktuelle H�he und wechsele die Lookuptabelle dementsprechend, 3x �hnlicher Code wg. Faulheit
	{
	case HIGHALT:
		transCount = (sizeof(state_transitions_default) / sizeof(*state_transitions_default)); //Die Anzahl der Eintr�ge im Array = Anzahl m�glicher �berg�nge
		for (i = 0; i < transCount; i++) //Gehe alle m�glichen �berg�nge durch, bis ein passender gefunden wurde
		{
			if (state == state_transitions_default[i].source_state) //Aktueller Zustand als Ursprung(Source) gefunden
			{
				if (ret == state_transitions_default[i].ret_code) //�bergebener Returnwert als valider Wert gefunden
				{
					return state_transitions_default[i].dest_state; //Gebe passenden neuen Zustand(Destination) aus
				}
			}
		}
		return SYS_ERROR; //Einer oder alle der eingegebenen Parameter waren nicht in der �bergangstabelle enthalten!
		//Keine break-Anweisung wegen dar�berliegender Returns
	case MEDALT:
		transCount = (sizeof(state_transitions_medalt) / sizeof(*state_transitions_medalt)); //Die Anzahl der Eintr�ge im Array = Anzahl m�glicher �berg�nge
		for (i = 0; i < transCount; i++) //Gehe alle m�glichen �berg�nge durch, bis ein passender gefunden wurde
		{
			if (state == state_transitions_medalt[i].source_state) //Aktueller Zustand als Ursprung(Source) gefunden
			{
				if (ret == state_transitions_medalt[i].ret_code) //�bergebener Returnwert als valider Wert gefunden
				{
					return state_transitions_medalt[i].dest_state; //Gebe passenden neuen Zustand(Destination) aus
				}
			}
		}
		return SYS_ERROR; //Einer oder alle der eingegebenen Parameter waren nicht in der �bergangstabelle enthalten!

	case LOWALT:
		transCount = (sizeof(state_transitions_lowalt) / sizeof(*state_transitions_lowalt)); //Die Anzahl der Eintr�ge im Array = Anzahl m�glicher �berg�nge
		for (i = 0; i < transCount; i++) //Gehe alle m�glichen �berg�nge durch, bis ein passender gefunden wurde
		{
			if (state == state_transitions_lowalt[i].source_state) //Aktueller Zustand als Ursprung(Source) gefunden
			{
				if (ret == state_transitions_lowalt[i].ret_code) //�bergebener Returnwert als valider Wert gefunden
				{
					return state_transitions_lowalt[i].dest_state; //Gebe passenden neuen Zustand(Destination) aus
				}
			}
		}
		return SYS_ERROR; //Einer oder alle der eingegebenen Parameter waren nicht in der �bergangstabelle enthalten!
	default:
		return SYS_ERROR; //Unpassender H�henparameter �bergeben!
	}
}

//Zeitfunktion: Maskiert millis() und die timeOffset-Variable, die das Offset bei Reset beh�lt
unsigned long sysTime(void)
{
	return (millis() + sysT.timeOffset);
}

int init_waitl_state(void)
{
	switchBuzzer(SLOW); //Buzzer einschalten(Langsam)
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("initWaitLong State"));
	delay(START_WAITSHORT); //Um nicht zu lange warten zu m�ssen, wird beim Debuggen WAITSHORT benutzt
#else
	delay(START_WAITLONG); //Warte lange (Einbau)
#endif // DBG_SERIALOUTPUT
	//Nach der Wartezeit: Stelle den Erststartmarker im EEPROM auf ACK, damit n�chstes Mal nur noch kurz/nicht gewartet wird
	EEPROM[0].update(0x6);
	return OK;
};

int init_waits_state(void)
{
	switchBuzzer(FAST);
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("initWaitShort State"));
#endif // DBG_SERIALOUTPUT
	delay(START_WAITSHORT); //Warte kurz (Auswurf)
	return OK;
};

int init_recall_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("Recall State"));
#endif // DBG_SERIALOUTPUT

//Auslesen von Statusvariablen aus dem EEPROM
if (EEPROM[0] == 0x6) //Ist das Erststartbit = ACK? Dann kein Erststart.
{
	if (EEPROM[1] == 0x6) //Ist das Offsetbit = ACK? Dann kein Zweitstart.
	{
		for (int i = 0; i < 4; i++) //Systemzeitoffset aus EEPROM lesen
		{
			sysT.tArray[i] = EEPROM[i + 2];
		}
		return RES; //Reset erkannt
	}
	else
	{
		return SEC; //Kein Erststart, aber auch kein Timeroffset. Daher: Zweitstart
	}
}
else
{
	return FIRST; //Erststart erkannt
}

};
int init_debugcheck_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("Debugcheck State"));
#endif // DBG_SERIALOUTPUT

	if (analogRead(DEBUGPIN) < 20) //Ist Debugpin gesetzt?
	{
		return DEBUG;
	}
	else
	{
		return DEF;
	}
};
int init_start_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("InitStart State"));
#endif // DBG_SERIALOUTPUT

	//SPI-Slaveinit, siehe Nick Gammon
	pinMode(S_MISO, OUTPUT);

	// SPI-Slavemode einschalten
	SPCR |= bit(SPE);

	// SPI-Interrupts einschalten
	SPI.attachInterrupt();

	//Buzzer dauerhaft anschalten
	switchBuzzer(CONT);

	//SPI-Buffer initialisieren
	for (int i = 0; i < 41; i++)
	{
		spi_buffer.getbyte[i] = 0;
	}

	//IMU initialisieren
	if (!sens_imu.begin())
	{
		sensStatus.imuWorking = false;
	}
	else
	{
		sens_imu.setExtCrystalUse(true);
	}

	//TODO: Hier sonstige Startvorg�nge einf�gen
	return OK;

};

int flight_checkalt_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("Checkalt State"));
#endif // DBG_SERIALOUTPUT
	if (sysTime() > FLIGHT_WAITTOMED)
	{
		if (sysTime() > FLIGHT_WAITTOLOW)
		{
			return ALT_LOW;
		}
		else
		{
			return ALT_MED;
		}
	}
	else
	{
		return ALT_HIGH;
	}
};
int flight_sync_state(void)
{	
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("flightSync State"));
	/*
	//DEBUG: Dummydaten in MCU2 einspeisen
	spi_buffer.to_mcu3_buffer.time2 = 1;
	spi_buffer.to_mcu3_buffer.state2 = 2;
	spi_buffer.to_mcu3_buffer.resets2 = 3;
	spi_buffer.to_mcu3_buffer.vbat = 4;
	spi_buffer.to_mcu3_buffer.rotX = 5;
	spi_buffer.to_mcu3_buffer.rotY = 6;
	spi_buffer.to_mcu3_buffer.rotZ = 7;
	spi_buffer.to_mcu3_buffer.quatw = 8;
	spi_buffer.to_mcu3_buffer.quatx = 9;
	spi_buffer.to_mcu3_buffer.quaty = 10;
	spi_buffer.to_mcu3_buffer.quatz = 11;
	spi_buffer.to_mcu3_buffer.calib = 12;
	spi_buffer.to_mcu3_buffer.tp = 13;
	spi_buffer.to_mcu3_buffer.gpslat = 14;
	spi_buffer.to_mcu3_buffer.gpslon = 15;
	spi_buffer.to_mcu3_buffer.gpsspeed =16;
	spi_buffer.to_mcu3_buffer.gpshead =17;
	spi_buffer.to_mcu3_buffer.gpshk =18;*/
#endif // DBG_SERIALOUTPUT
	if (mcu1call)
	{
#ifdef DBG_SERIALOUTPUT
		Serial.println(F("MCU1 Flag removed"));
#endif
		mcu1call = false;
	}
	if (mcu3call)
	{
#ifdef DBG_SERIALOUTPUT
		Serial.println(F("MCU3 Flag removed"));
#endif
		mcu3call = false;
	}

	while (!mcu1call && !mcu3call) //OR-Bedingung, wartet auf einen der beiden Interrupts und liest w�hrenddessen das GPS aus
	{
		//Ist das GPS an?
		if (sensStatus.gpsOn)
		{
			if (Serial.available())
			{
				int inp = Serial.read();
				sens_gps.encode(inp);
			}
		}
	}
	//Sobald Interrupt da ist, Signal zur�ckschicken
	digitalWrite(INT_OUTPUT, HIGH);
	pinMode(INT_OUTPUT, OUTPUT);
	delay(INTERRUPT_PEAKTIME);
	digitalWrite(INT_OUTPUT, LOW);
	pinMode(INT_OUTPUT, INPUT);
	//TODO �hnlichen Interrupt+Timer-Wartemechanismus wie in init-Sync einbauen
#ifdef DBG_SERIALOUTPUT
	if (mcu1call && mcu3call)
	{
		Serial.println(F("Both"));
	}
	else if (mcu3call)
	{
		Serial.println(F("MCU3"));
	}
	else
	{
		Serial.println(F("MCU1"));
	}
#endif

	return OK;
};
int flight_talk1_state(void)
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("flightTalk1 State"));
#endif // DBG_SERIALOUTPUT

	return OK;
};
int flight_talk2_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("flightTalk2 State"));
#endif // DBG_SERIALOUTPUT

	//Hier passiert nichts, die MCU wurde von MCU3 angepingt und wartet jetzt, w�hren SPI gelesen wird
	delay(SPI_TALKTIME);
	return OK;
};
int flight_save_state(void)
{
	//Nichts zu tun
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("flightSave State"));
#endif // DBG_SERIALOUTPUT

	return OK;
};
int flight_memo_state(void)
{
#ifdef DBG_SERIALOUTPUT
Serial.println(F("flightMemo State"));
#endif // DBG_SERIALOUTPUT

	//Die aktuelle Systemzeit im EEPROM speichern
	tBuf.time = sysTime();

	for (int i = 0; i < 4; i++) //Systemzeitoffset in EEPROM speichern
	{
		EEPROM[i + 2].update(tBuf.tArray[i]);
	}
	//Falls nicht schon geschehen: Offsetmarker ACK im EEPROM setzen
	EEPROM[1].update(0x6);
	return OK;
};
int flight_pollbat_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("flightPollBat State"));
#endif // DBG_SERIALOUTPUT

	//Batteriespannung auslesen
	int batVal = analogRead(VBAT_AIN);
	batVal = (batVal - (batVal % 4));
	batVal /= 4;
	spi_buffer.to_mcu3_buffer.vbat = batVal;

	//Systemzeit einspeichern
	spi_buffer.to_mcu3_buffer.time2 = (sysTime()/1000);
	//H�henmodus einspeichern. Folgendes Schema wird benutzt, um HIGH, MED, LOW auf ein Byte zu mappen:
	// 129 (10000001)=HIGH, 66(01000010)=MED, 36(00100100)=LOW
	switch (cur_alt)
	{
	case HIGHALT:
		spi_buffer.to_mcu3_buffer.state2 = 129;
		break;
	case MEDALT:
		spi_buffer.to_mcu3_buffer.state2 = 66;
		break;
	case LOWALT:
		spi_buffer.to_mcu3_buffer.state2 = 36;
		break;
	default:
		spi_buffer.to_mcu3_buffer.state2 = 129; //Default ist HIGH
		break;
	}

	//Anzahl der Resets einspeichern: vorerst 0 //TODO
	spi_buffer.to_mcu3_buffer.resets2 = 0;
	return OK;
};
int flight_pollgps_state(void)
{
	//Pr�fen, ob das GPS bereit ist
	float flat, flon; //Latitude, Longitude
	unsigned long fix_age; //Alter des Fixes
	sens_gps.f_get_position(&flat, &flon, &fix_age); //Positionsdaten gewinnen
	if (fix_age == TinyGPS::GPS_INVALID_AGE) //GPS-Daten sind nicht g�ltig
	{
		spi_buffer.to_mcu3_buffer.gpslat = 0;
		spi_buffer.to_mcu3_buffer.gpslon = 0;
		spi_buffer.to_mcu3_buffer.gpsspeed = 0;
		spi_buffer.to_mcu3_buffer.gpshead = 0;

		spi_buffer.to_mcu3_buffer.gpshk = 0;
	}
	else //Fix ist (noch) g�ltig
	{
		spi_buffer.to_mcu3_buffer.gpslat = flat;
		spi_buffer.to_mcu3_buffer.gpslon = flon;
		spi_buffer.to_mcu3_buffer.gpshead = sens_gps.f_course();
		spi_buffer.to_mcu3_buffer.gpsspeed = sens_gps.f_speed_mps();
		if (fix_age > 10000) //Fix ist �lter als 10s
		{
			//Gleiches Schema wie f�r die H�henmodi:
			// 129 (10000001)=Fix sehr jung, 66(01000010)=Fix �lter als 5s, 36(00100100)=Fix �lter als 10s
			spi_buffer.to_mcu3_buffer.gpshk = 36;
		}
		else if (fix_age > 5000) //Fix ist �lter als 5s
		{
			spi_buffer.to_mcu3_buffer.gpshk = 66;
		}
		else
		{
			spi_buffer.to_mcu3_buffer.gpshk = 129;
		}
	}
	

#ifdef DBG_SERIALOUTPUT
 Serial.println(F("flightPollGPS State"));
#endif // DBG_SERIALOUTPUT
	return OK;
};
int flight_pollimu_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("flightPollIMU State"));
#endif // DBG_SERIALOUTPUT

	//Quaternionen und Rollraten abrufen
	imu::Quaternion quat = sens_imu.getQuat();
	imu::Vector<3> gyro = sens_imu.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);
	uint8_t system, gyros, accel, mag = 0;
	sens_imu.getCalibration(&system, &gyros, &accel, &mag);

	//Kalibrierung auslesen
	byte calib = 0;
	//Bits setzen
	calib |= system << 6;
	calib |= gyros << 4;
	calib |= accel << 2;
	calib |= mag;

	spi_buffer.to_mcu3_buffer.quatw = (int)(quat.w()*10000);
	spi_buffer.to_mcu3_buffer.quatx = (int)(quat.x()*10000);
	spi_buffer.to_mcu3_buffer.quaty = (int)(quat.y()*10000);
	spi_buffer.to_mcu3_buffer.quatz = (int)(quat.z()*10000);

	spi_buffer.to_mcu3_buffer.rotX = (int)(gyro.x()*10);
	spi_buffer.to_mcu3_buffer.rotY = (int)(gyro.y()*10);
	spi_buffer.to_mcu3_buffer.rotZ = (int)(gyro.z()*10);

	spi_buffer.to_mcu3_buffer.calib = calib;

	return OK;
};
int flight_polltp_state(void)
{
	spi_buffer.to_mcu3_buffer.tp = 0;
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("flightPollTP State"));
#endif // DBG_SERIALOUTPUT

	return OK;
};
int flight_med_trans(void) //�bergang in den Medium altitude mode (Einschalten des GPS, ver�ndern der Pollreihenfolge+Sendepriorit�t)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("Altitude change: High to Medium"));
#endif // DBG_SERIALOUTPUT

	//GPS einschalten
	digitalWrite(GPS_ONOFF, LOW);
	pinMode(GPS_ONOFF, OUTPUT);
	sensStatus.gpsOn = true;

	cur_alt = MEDALT; //H�he �ndern
	return OK;
};
int flight_low_trans(void) //�bergang in den Low altitude mode (Ver�ndern der Pollreihenfolge+Sendepriorit�t)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("Altitude change: Medium to low"));
#endif // DBG_SERIALOUTPUT

 //GPS einschalten
 digitalWrite(GPS_ONOFF, LOW);
 pinMode(GPS_ONOFF, OUTPUT);
 sensStatus.gpsOn = true;

	cur_alt = LOWALT; //H�he �ndern
	return OK;
};


int test_listen_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testListen State"));
#endif // DBG_SERIALOUTPUT

	//Buzzer auf kurzen Piepton alle 4 Sekunden einstellen
	switchBuzzer(VERYSLOW);

	//EEPROM l�schen, um die Warteflag zu entfernen TODO bei fertigen Testing-Codes ersetzen
	for (int i = 0; i < 5; i++) {
		EEPROM[i].update(0);
	}
	
	//GPS einschalten
	digitalWrite(GPS_ONOFF, LOW);
	pinMode(GPS_ONOFF, OUTPUT);

	while (1); //TODO Endlosschleife einbauen, simuliert das Warten auf eine Eingabe
	//Was hier hinsollte: Setzen einer Warteschleifenflag, in der Schleifge regelm��iges Abfragen des UART nach Kommandos. Au�erdem ablaufen eines Timers, um danach in den Reset-Modus zu schalten
	return TEST_RESET; //TODO eine der 4 m�glichen Returns, um Compilerwarnung zu vermeiden
};
int test_tp_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testTP State"));
#endif // DBG_SERIALOUTPUT
};
int test_gps_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testGps State"));
#endif // DBG_SERIALOUTPUT
};
int test_imu_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testIMU State"));
#endif // DBG_SERIALOUTPUT
};
int test_spicomm_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testSPI State"));
#endif // DBG_SERIALOUTPUT
};
int test_reset_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("testReset State, waiting for PowerOff"));
#endif // DBG_SERIALOUTPUT
	//Hier sollte nichts weiter passieren, da hier das FSM-Ende erreicht ist und der mainloop geleert wird. Returnwerte dieses Zustands sollten nicht ausgewertet werden.
	//int-Typ ist f�r Homogenit�t und Pointerverwendung n�tig.
	//Daher:
	return ERROR; //In der Zustandstabelle ist kein R�cksprung aus RESET vorgesehen, daher wird jeglicher weiterer Auswertungsversuch sowieso zu SYSERROR f�hren.
};
int syserror_state(void)
{
#ifdef DBG_SERIALOUTPUT
 Serial.println(F("System Error State"));
#endif // DBG_SERIALOUTPUT

	//TODO: RETRY-Wert verwenden, um dauerhaft im Errorstate zu bleiben
	return ERROR;
};

void toggleBuzzer(void)
{
	if (buzzer.isOn)
	{
		digitalWrite(BUZZER_ONOFF, LOW);
		buzzer.isOn = false;
	}
	else
	{
		digitalWrite(BUZZER_ONOFF, HIGH);
		buzzer.isOn = true;
	}
}

void switchBuzzer(BUZZERSTATE buzState)
{
	switch (buzState)
	{

	case VERYSLOW:
		buzzer.beepcont = false;
		buzzer.beepfast = false;
		buzzer.beepslow = false;
		buzzer.beepvslow = true;
		break;
	case SLOW:
		buzzer.beepcont = false;
		buzzer.beepfast = false;
		buzzer.beepslow = true;
		buzzer.beepvslow = false;
		break;
	case FAST:
		buzzer.beepcont = false;
		buzzer.beepfast = true;
		buzzer.beepslow = false;
		buzzer.beepvslow = false;
		break;
	case CONT:
		buzzer.beepcont = true;
		buzzer.beepfast = false;
		buzzer.beepslow = false;
		buzzer.beepvslow = false;
		buzzer.isOn = true;
		digitalWrite(BUZZER_ONOFF, HIGH);
		break;
	case OFF:
		buzzer.beepcont = false;
		buzzer.beepfast = false;
		buzzer.beepslow = false;
		buzzer.beepvslow = false;
		buzzer.isOn = false;
		digitalWrite(BUZZER_ONOFF, LOW);
		break;
	default: //Falls falscher �bergabewert: Schalte ab
		buzzer.beepcont = false;
		buzzer.beepfast = false;
		buzzer.beepslow = false;
		buzzer.beepvslow = false;
		buzzer.isOn = false;
		digitalWrite(BUZZER_ONOFF, LOW);
		break;
	}
}


//Interrupt-Service-Routinen
void int_mcu1call()
{
	mcu1call = true;
}

void int_mcu3call()
{
	mcu3call = true;
}

//SPI-ISR
ISR(SPI_STC_vect)
{
	c = SPDR;
	SPDR = spi_buffer.getbyte[pos];
	pos++;
	if (pos > 39)
	{
		pos = 0;
	}
}

//Interrupttimer-ISR
ISR(TIMER1_COMPA_vect){
	//Timertrigger erstellen
	if (isrCounter % RESETRATE == 0)
	{
		isrCounter = 0;
	}
	if (buzzer.beepfast)
	{
		buzzer.fastCounter++;
	}
	else
	{
		buzzer.fastCounter = 0;
	}
	if (buzzer.beepslow)
	{
		buzzer.slowCounter++;
	}
	else
	{
		buzzer.slowCounter = 0;
	}
	if (buzzer.beepvslow)
	{
		buzzer.vslowCounter++;
	}
	else
	{
		buzzer.vslowCounter = 0;
	}

	if (buzzer.fastCounter == 5) //Alle 0,5s wird der Buzzer ein/ausgeschaltet
	{
		toggleBuzzer();
		buzzer.fastCounter = 0;
	}

	if (buzzer.slowCounter == 30) //Alle 3s wird der Buzzer ein/ausgeschaltet
	{
		toggleBuzzer();
		buzzer.slowCounter = 0;
	}

	if (buzzer.isOn && buzzer.vslowCounter == 4)
	{
		toggleBuzzer();
		buzzer.vslowCounter = 0;
	}
	if (!buzzer.isOn && buzzer.vslowCounter == 40)
	{
		toggleBuzzer();
		buzzer.vslowCounter = 0;
	}



	isrCounter++;
}