#pragma once
//Hier werden alle Iridium-Funktionen und -Strukturen au�er der FSM untergebracht

enum IRID_SIGNALS //Signalzust�nde des Transceivers: 0 bis 5 Balken
{
	S_0,
	S_1,
	S_2,
	S_3,
	S_4,
	S_5
};

struct icu_status //Beinhaltet alle Zustandsvariablen des Transceivers
{
	IRID_SIGNALS cur_sig; //Aktuelle Signalst�rke
	bool isRising; //Steigt die Signalst�rke gerade an?
	bool isOn; //Ist der Transceiver angeschaltet und antwortet auf das start-AT-Kommando?
	bool isReady; //Sind alle drei Init-Befehle geschrieben und verarbeitet worden?
	bool isBusy; //Steht noch eine Antwort des Transceivers auf einen Befehl aus?
	bool isOnline; //Ist das Iridium-Netzwerk verf�gbar?
	bool bufferFull; //Sind Daten im ausgehenden Buffer DES TRANSCEIVERS enthalten?
	bool pending; // Muss noch eine Nachricht gesendet werden?
	bool newMsg; //Ist eine neue Nachricht f�r den Transceiverbuffer AUF DER MCU vorhanden?
	bool secondLF; //Ist das eingelesene \n das erste oder zweite in der Reihe?
	bool readSig; //Wird beim CIEV-Auslesen Signalqualit�t oder Netzwerkstatus angegeben?
	byte failCount; //Z�hlt die Anzahl der Misserfolge beim Senden der aktuellen Nachricht
	unsigned long lastFail; //H�lt die Zeit des letzten Iridium-Sendemiserfolgs fest, damit bei schneller Folge(Wenig Spannung im Kondensator) etwas gewartet werden kann
};
