#include <EEPROM.h>
#include <SPI.h>
#include "SPI_Slave.h"
#include "FSM.h"
#include "avr\wdt.h"
#include "Pins.h"
#include "Defines.h"
#include "Iridium.h"
#include "Sys_Status.h"
#include "Buffer.h"

#ifdef DBG_SOFTSERIAL
#include <SoftwareSerial.h>
#endif

OBC_STATES cur_state = ENTRY_STATE; //Anfangsstatus festlegen
IRID_STATES cur_istate = IRID_ENTRY_STATE; //Anfangsstatus f�r den Iridiumtransceiver festlegen
IRID_META_STATES cur_imetastate = META_INIT_AT; //Anfangs-Metastatus des Iridiumtransceivers festlegen
ALTITUDE cur_alt = HIGHALT; //Anfangsh�he festlegen
RET_CODES retVal = ERROR; //Returncode-Variable erstellen, hier als Error falls die erste Pointerzuordnung gleich fehlschl�gt
IRID_RET_CODES iretVal = I_UNKNOWN; //Returncode-Variable f�r Iridium erstellen

iridium_buffertype iridium_buffer;
spi_buffertype spi_buffer;


icu_status iridium = { S_0, false, false, false, false, false, false, false, false, false, false, 0 , 0}; //Initialisiere die ISU-Statusvariable (offenbar hat bool keinen Standardwert bei Init, daher explizit)
systemStatus sysStatus = { 0, 0, 0, 0}; //System-Statusstruktur erstellen, enth�lt Memory-Timervariable, SPI-Nachrichtennummer und Iridium-Nachrichtennummer
systemTime sysT; //Systemzeit-Struktur erstellen
timeBuf tBuf; //Buffer f�r den EEPROM-Speichervorgang

int(*state_fun)(void) = state[cur_state]; //Funktionspointer zum Ausf�hren der Zust�nde erstellen, mit Anfangszustand belegen
int(*istate_fun)(void) = iridState[cur_istate]; //Gleiches f�r den Iridiumparser
int retbuf = 0; //Buffer f�r R�ckgabewerte erstellen, Wert 0 ist nicht in der �bergangstabelle enthalten -> F�hrt zu �bergang in SYSERROR-Mode wenn kein neuer Wert vor der ersten Verwendung eingespielt wird
int iretbuf = 0; //Buffer f�r Iridium-R�ckgabewerte

//Generelle Flags
bool doNothing = false; //Benutzt, um die MCUs inert zu schalten, wenn sie nichts mehr zu tun haben
volatile bool mcu2call = false; //Flag: MCU2 hat den externen Interrupt ausgel�st
volatile bool mcu3call = false; //...
bool mcu2flag = false; //Flags, die die Werte von mcu1call/mcu2call bei Abruf "einfrieren", damit sie innerhalb eines Funktionsdurchlaufes konstant bleiben
bool mcu3flag = false; //"

//TODO: "Vorstrafenregister" der anderen beiden MCUs, hier wird gesammelt. Anhand der gesammelten Informationen kann entschieden werden. Auch in struct
bool mcu2_int_na = false; //Interruptleitung von MCU2 zu 1 hat beim letzten Versuch nicht funktioniert
bool mcu3_int_na = false; //...3 zu 1

bool mcu2_mia = false; //MCU2 meldet sich nicht �ber Interrupt, Ergebnisse der SPI-Kommunikation werden abgewartet
bool mcu3_mia = false; //MCU3...

volatile bool newSpi = false; //Zeigt an, dass neue Nachrichten �ber SPI eingetroffen sind

//Softwareserial f�r das Debuggen der Iridiumkommunikation erstellen
#ifdef DBG_SOFTSERIAL
SoftwareSerial iridSerial(IRIDIUM_DSR, IRIDIUM_CTS);
#endif

void setup()
{
	//Alle Pins bis auf die SPI-Pins inert schalten
	pinMode(INT_INPUT_MCU3, INPUT); //D2
	pinMode(INT_INPUT_MCU2, INPUT); //D3
	pinMode(IRIDIUM_ONOFF, INPUT); //D4
	pinMode(IRIDIUM_DCD, INPUT); //D5
	pinMode(IRIDIUM_DSR, INPUT); //D6
	pinMode(IRIDIUM_CTS, INPUT); //D7
	pinMode(IRIDIUM_RI, INPUT); //D8
	pinMode(IRIDIUM_RTS, INPUT); //D9
	/*
	pinMode(S_SS, INPUT); //D10 Slave-Select-Pin: Hier nicht belegt
	pinMode(S_MOSI, INPUT); //D11
	pinMode(S_MISO, INPUT); //D12
	pinMode(S_SCK, INPUT); //D13 */

	pinMode(INT_OUTPUT, INPUT); //A0
	pinMode(VOTE_MCU3, INPUT); //A1
	pinMode(VOTE_MCU2, INPUT); //A2
	pinMode(BEACON_ENABLE, INPUT); //A3
	pinMode(IRIDIUM_DTR, INPUT); //A4
	pinMode(IRIDIUM_NETAV, INPUT); //A5
	pinMode(DEBUGPIN, INPUT); //A6
	pinMode(A7, INPUT); //NICHT BELEGT

	digitalWrite(INT_INPUT_MCU3, LOW); //D2
	digitalWrite(INT_INPUT_MCU2, LOW); //D3
	digitalWrite(IRIDIUM_ONOFF, LOW); //D4
	digitalWrite(IRIDIUM_DCD, LOW); //D5
	digitalWrite(IRIDIUM_DSR, LOW); //D6
	digitalWrite(IRIDIUM_CTS, LOW); //D7
	digitalWrite(IRIDIUM_RI, LOW); //D8
	digitalWrite(IRIDIUM_RTS, LOW); //D9
	/*digitalWrite(S_SS, LOW); //D10
	digitalWrite(S_MOSI, LOW); //D11
	digitalWrite(S_MISO, LOW); //D12
	digitalWrite(S_SCK, LOW); //D13*/

	digitalWrite(INT_OUTPUT, LOW); //A0
	digitalWrite(VOTE_MCU3, LOW); //A1
	digitalWrite(VOTE_MCU2, LOW); //A2
	digitalWrite(BEACON_ENABLE, LOW); //A3
	digitalWrite(IRIDIUM_DTR, LOW); //A4
	digitalWrite(IRIDIUM_NETAV, LOW); //A5
	digitalWrite(DEBUGPIN, LOW); //A6
	digitalWrite(A7, LOW); //NICHT BELEGT

#ifdef DBG_SERIALOUTPUT
	Serial.begin(38400);
	pinMode(0, INPUT);
	digitalWrite(0, LOW);
#else
	Serial.begin(19200); //Wenn Debugausgabe �ber Hardwareserial nicht an, schalte Kommunikation mit dem Iridiumtransceiver ein
#endif // DBG_SERIALOUTPUT

#ifdef DBG_SOFTSERIAL
	iridSerial.begin(38400);
#endif // DBG_SOFTSERIAL

	//Externe Interrupts anschlie�en
	attachInterrupt(digitalPinToInterrupt(INT_INPUT_MCU2), int_mcu2call, RISING);
	attachInterrupt(digitalPinToInterrupt(INT_INPUT_MCU3), int_mcu3call, RISING);

	//Systemzeit auf 0 initialisieren
	sysT.timeOffset = 0;
}

void loop()
{
	if (!doNothing)
	{
		retbuf = state_fun(); //F�hre aktuellen Zustand aus und erhalte R�ckgabewert
		if (retbuf > MIN_RET && retbuf < MAX_RET) //Pr�fe G�ltigkeit des Returnwerts
		{
			retVal = (RET_CODES)retbuf; //Konvertiere zu Returnwert-Typ 
		}
		cur_state = lookup_transitions(cur_state, retVal); //Zustand wechseln
		state_fun = state[cur_state]; //Zustands-Funktionspointer aktualisieren
		if (EXIT_STATE == cur_state) //Ist letzter Zustand der FSM erreicht, schalte FSM-Ausf�hrung VOR BEGINN DES ENDZUSTANDS aus
		{
			doNothing = true;
		}
	}
}

//Transition-Funktion:
OBC_STATES lookup_transitions(OBC_STATES state, RET_CODES ret)
{
	int i; //Z�hler
	int transCount = (sizeof(state_transitions) / sizeof(*state_transitions)); //Die Anzahl der Eintr�ge im Array = Anzahl m�glicher �berg�nge
	for (i = 0; i < transCount; i++)
	{
		if (state == state_transitions[i].source_state)
		{
			if (ret == state_transitions[i].ret_code)
			{
				return state_transitions[i].dest_state;
			}
		}
	}
	return SYS_ERROR; //Die eingegebenen Parameter waren nicht in der �bergangstabelle enthalten!
}
//Transition-Funktion f�r den Iridiumparser: Je nach Metazustand werden andere Zustand�berg�nge abgearbeitet
IRID_STATES lookup_irid(IRID_META_STATES meta ,IRID_STATES state, IRID_RET_CODES ret){
	int i; //Z�hler
	int transCount = 0; //Die Anzahl der Eintr�ge im Array = Anzahl m�glicher �berg�nge
	switch (meta)
	{
	case META_INIT_AT:
		transCount = (sizeof(initat_trans) / sizeof(*initat_trans));
		for (i = 0; i < transCount; i++)
		{
			if (state == initat_trans[i].source)
			{
				if (ret == initat_trans[i].ret)
				{
					return initat_trans[i].destin;
				}
			}
		}
		break;
	case META_INIT_ATE0:
		transCount = (sizeof(initate0_trans) / sizeof(*initate0_trans));
		for (i = 0; i < transCount; i++)
		{
			if (state == initate0_trans[i].source)
			{
				if (ret == initate0_trans[i].ret)
				{
					return initate0_trans[i].destin;
				}
			}
		}
		break;
	case META_INIT_ATD0:
		transCount = (sizeof(initatd0_trans) / sizeof(*initatd0_trans));
		for (i = 0; i < transCount; i++)
		{
			if (state == initatd0_trans[i].source)
			{
				if (ret == initatd0_trans[i].ret)
				{
					return initatd0_trans[i].destin;
				}
			}
		}
		break;
	case META_INIT_CIER:
		transCount = (sizeof(initcier_trans) / sizeof(*initcier_trans));
		for (i = 0; i < transCount; i++)
		{
			if (state == initcier_trans[i].source)
			{
				if (ret == initcier_trans[i].ret)
				{
					return initcier_trans[i].destin;
				}
			}
		}
		break;
	case META_IDLE:
		transCount = (sizeof(idle_trans) / sizeof(*idle_trans));
		for (i = 0; i < transCount; i++)
		{
			if (state == idle_trans[i].source)
			{
				if (ret == idle_trans[i].ret)
				{
					return idle_trans[i].destin;
				}
			}
		}
		break;
	case META_READ_CIEV:
		transCount = (sizeof(readciev_trans) / sizeof(*readciev_trans));
		for (i = 0; i < transCount; i++)
		{
			if (state == readciev_trans[i].source)
			{
				if (ret == readciev_trans[i].ret)
				{
					return readciev_trans[i].destin;
				}
			}
		}
		break;
	case META_READ_SBDI:
		transCount = (sizeof(readsbdi_trans) / sizeof(*readsbdi_trans));
		for (i = 0; i < transCount; i++)
		{
			if (state == readsbdi_trans[i].source)
			{
				if (ret == readsbdi_trans[i].ret)
				{
					return readsbdi_trans[i].destin;
				}
			}
		}
		break;
	case META_SEND_BWRITE:
		transCount = (sizeof(sendbwrite_trans) / sizeof(*sendbwrite_trans));
		for (i = 0; i < transCount; i++)
		{
			if (state == sendbwrite_trans[i].source)
			{
				if (ret == sendbwrite_trans[i].ret)
				{
					return sendbwrite_trans[i].destin;
				}
			}
		}
		break;
	case META_READ_BWRITE:
		transCount = (sizeof(readbwrite_trans) / sizeof(*readbwrite_trans));
		for (i = 0; i < transCount; i++)
		{
			if (state == readbwrite_trans[i].source)
			{
				if (ret == readbwrite_trans[i].ret)
				{
					return readbwrite_trans[i].destin;
				}
			}
		}
		break;
	case META_READ_OK:
		transCount = (sizeof(readok_trans) / sizeof(*readok_trans));
		for (i = 0; i < transCount; i++)
		{
			if (state == readok_trans[i].source)
			{
				if (ret == readok_trans[i].ret)
				{
					return readok_trans[i].destin;
				}
			}
		}
		break;
	default:
		//TODO hier sollte noch ein ERROR/Defaultstate hin
		break;

	}
	//TODO: Hier geh�rt noch ein Returnwert im Fehlerfall hin!
}

//Zeitfunktion: Maskiert millis() und die timeOffset-Variable, die das Offset bei Reset beh�lt
unsigned long sysTime(void)
{
	return (millis() + sysT.timeOffset);
}

//SPI-Timerfunktion
bool spi_event(void)
{
	if (newSpi) //Wurde ein SPI-Interrupt ausgel�st?
	{
		return true; //Ein SPI-Event ist eingetreten: Einer der Interrupts wurde abgefragt, oder der SPI-Timer ist angesprungen
	}
	else
	{
		return false; //Kein Event
	}
}

//Memorize-Timerfunktion
bool mem_event(void)
{
	if ((sysTime() - sysStatus.lastMem) > MEM_INTERVAL)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//Iridium-Hauptfunktion
void iridium_tick(void)
{
	iretbuf = istate_fun(); //F�hre aktuellen Zustand aus und erhalte R�ckgabewert
	if (iretbuf > I_MIN_RET && iretbuf < I_MAX_RET) //Pr�fe G�ltigkeit des Returnwerts
	{
		iretVal = (IRID_RET_CODES)iretbuf; //Konvertiere zu Returnwert-Typ 
	}
	cur_istate = lookup_irid(cur_imetastate ,cur_istate, iretVal); //Zustand wechseln
	istate_fun = iridState[cur_istate]; //Zustands-Funktionspointer aktualisieren
}

char iridium_read(void)
{
	if (Serial.available() > 0) //Ist etwas im seriellen Buffer gespeichert?
	{
		return Serial.read();
	}
	else //Nichts im Buffer
	{
		return 0x15; //Gib ASCII-NAK zur�ck
	}
}

int init_waitl_state(void)
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("initWaitLong State"));
	delay(START_WAITSHORT); //Um nicht zu lange warten zu m�ssen, wird beim Debuggen WAITSHORT benutzt
#else
	delay(START_WAITLONG); //Warte lange (Einbau)
#endif // DBG_SERIALOUTPUT
	//Nach der Wartezeit: Stelle den Erststartmarker im EEPROM auf ACK, damit n�chstes Mal nur noch kurz/nicht gewartet wird
	EEPROM[0].update(0x6);
	return OK;
};

int init_waits_state(void)
{
#ifdef DBG_SERIALOUTPUT
	Serial.println(F("initWaitShort State"));
#endif // DBG_SERIALOUTPUT
	delay(START_WAITSHORT); //Warte kurz (Auswurf)
	return OK;
};

int init_recall_state(void)
{
#ifdef DBG_SERIALOUTPUT
	delay(DBG_STATEWAIT);
	Serial.println(F("Recall State"));
#endif // DBG_SERIALOUTPUT

	//Auslesen von Statusvariablen aus dem EEPROM
	if (EEPROM[0] == 0x6) //Ist das Erststartbit = ACK? Dann kein Erststart.
	{
		if (EEPROM[1] == 0x6) //Ist das Offsetbit = ACK? Dann kein Zweitstart.
		{
			for (int i = 0; i < 4; i++) //Systemzeitoffset aus EEPROM lesen
			{
				sysT.tArray[i] = EEPROM[i + 2];
			}
			return RES; //Reset erkannt
		}
		else
		{
			return SEC; //Kein Erststart, aber auch kein Timeroffset. Daher: Zweitstart
		}
	}
	else
	{
		return FIRST; //Erststart erkannt
	}
};
int init_debugcheck_state(void)
{
#ifdef DBG_SERIALOUTPUT
	delay(DBG_STATEWAIT); Serial.println(F("Debugcheck State"));
#endif // DBG_SERIALOUTPUT

	if (analogRead(DEBUGPIN) < 20) //Ist Debugpin gesetzt?
	{
		return DEBUG;
	}
	else
	{
		return DEF;
	}
};

int init_start_state(void)
{
#ifdef DBG_SERIALOUTPUT
	delay(DBG_STATEWAIT); Serial.println(F("Start State"));
#endif // DBG_SERIALOUTPUT
	//Hier werden alle Peripherals eingeschaltet
	//Iridium:
	//N�tige Signalpins auf GND ziehen
	digitalWrite(IRIDIUM_RTS, LOW);
	digitalWrite(IRIDIUM_DTR, LOW);
	pinMode(IRIDIUM_RTS, OUTPUT);
	pinMode(IRIDIUM_DTR, OUTPUT);
	//Iridium einschalten
	digitalWrite(IRIDIUM_ONOFF, HIGH);
	pinMode(IRIDIUM_ONOFF, OUTPUT);

	//SPI-Slaveinit, siehe Nick Gammon
	pinMode(S_MISO, OUTPUT);

	// SPI-Slavemode einschalten
	SPCR |= _BV(SPE);

	// SPI-Interrupts einschalten
	SPI.attachInterrupt();

	//Iridium-Buffer mit 0 f�llen
	for (int i = 0; i < 340; i++)
	{
		iridium_buffer.getbyte[i] = 0;
	}

	//SPI-Buffer mit 0 f�llen
	for (int i = 0; i < 74; i++)
	{
		spi_buffer.getbyte[i] = 0;
	}

	//Kurz warten, damit Iridium sich initialisieren kann
	delay(START_WAITIRIDTIME);

	//Zeitvariable auf Systemzeit setzen, damit nicht gleich eine SPI-Sitzung gestartet wird (au�er �ber interrupts)
	//TODO muss mit den Timern der anderen MCUs abgestimmt werden?
	sysStatus.lastMem = sysTime();

	return OK;
}
int flight_main_state(void)
{
	if (spi_event()) //Ist ein Interrupt eingetroffen?
	{
		return SPIGO; //Schalte in SPI-Modus
	}
	else
	{
		iridium_tick(); //Rede mit Iridium
	}
	if (spi_event()) //Frage noch einmal SPI ab
	{
		return SPIGO;
	}
	else if (mem_event()) //Ist der Memorize-Timer angesprungen?
	{
		return MEMTIMER;
	}
	else
	{
		return REPEAT; //Nichts besonderes, Wiederhole den Zustand
	}
}

int flight_store_state(void)
{
#ifdef DBG_SERIALOUTPUT
	delay(DBG_STATEWAIT);
	Serial.println(F("flightStore State"));
#endif // DBG_SERIALOUTPUT
	//Nummer der Nachricht pr�fen
	byte num1 = spi_buffer.from_mcu3_buffer.number;
	byte num2 = spi_buffer.from_mcu3_buffer.number2;
	byte num3 = spi_buffer.from_mcu3_buffer.number3;

	byte num4 = 0;

	//Liegen g�ltige Nummern vor?
	if (num1 < 1 || num1 > 12) //num1 ist ung�ltig
	{
		num1 = 0; //num1 entwerten
	}

	if (num2 < 1 || num2 > 12) //num2 ist ung�ltig
	{
		num2 = 0; //num2 entwerten
	}

	if (num3 < 1 || num3 > 12) //num3 ist ung�ltig
	{
		num3 = 0; //num3 entwerten
	}

	if (num1 == num2 && num1 != 0) //Ergibt sich die Mehrheit aus num1 und num2?
	{
		num4 = num1;
	}
	else if (num2 == num3 && num2 != 0) //Falls nicht: Ergibt sich die Mehrheit aus num2 und num3?
	{
		num4 = num2;
	}
	else if (num1 == num3 && num1 != 0) //Falls nicht: Ergibt sich eine Mehrheit aus num1 und num3?
	{
		num4 = num1;
	}
	else
	{
		num4++; //Nummer wird pauschal um 1 inkrementiert, es wird davon ausgegangen dass wenigstens Teile der Nachricht durchgekommen sind
	}

	//Dummyabfrage: Ist die aktuelle Sendenummer gr��er als die letzte?
	if (num4 > sysStatus.sendNum)
	{
		sysStatus.sendNum = num4;
	}
	else
	{
		sysStatus.sendNum = sysStatus.sendNum + 1;
	}

	//DEBUG:
	//Ausgabe der Nachrichtennummer
#ifdef LMDEBUG
	Serial.println(sysStatus.sendNum);
#endif
	//Pr�fen, welcher H�henmodus vorliegt. Folgendes Schema wird benutzt, um HIGH, MED, LOW auf ein Byte zu mappen:
	// 129 (10000001)=HIGH, 66(01000010)=MED, 36(00100100)=LOW

	//Ist MCU3-State g�ltig?
	num1 = spi_buffer.from_mcu3_buffer.state3;
	num2 = spi_buffer.from_mcu3_buffer.state2;
	if (num1 == 129 || num1 == 66 || num1 == 36)
	{
		switch (num1)
		{
		case 129:
			cur_alt = HIGHALT;
#ifdef LMDEBUG
			Serial.println(129);
#endif
			break;
		case 66:
			cur_alt = MEDALT;
#ifdef LMDEBUG
			Serial.println(66);
#endif
			break;
		case 36:
			cur_alt = LOWALT;
#ifdef LMDEBUG
			Serial.println(36);
#endif
			break;
		}
	}
	else if (num2 == 129 || num2 == 66 || num2 == 36)//MCU3-State war nicht g�ltig, pr�fe MCU2-State
	{
		switch (num2)
		{
		case 129:
			cur_alt = HIGHALT;
#ifdef LMDEBUG
			Serial.println(229);
#endif
			break;
		case 66:
			cur_alt = MEDALT;
#ifdef LMDEBUG
			Serial.println(76);
#endif
			break;
		case 36:
#ifdef LMDEBUG
			Serial.println(46);
#endif
			cur_alt = LOWALT;
			break;
		}
	}
	//Falls beide States nicht lesbar, bleibe bei HIGHALT

	//Housekeeping:
	for (int i = 0; i < 4; i++)
	{
		iridium_buffer.buffer_high.hk[i] = 0;
	}
	//States
	switch (spi_buffer.from_mcu3_buffer.state2)
	{
	case 129:
		iridium_buffer.buffer_high.hk[0] |= 16;
		break;
	case 66:
		iridium_buffer.buffer_high.hk[0] |= 32;
		break;
	case 36:
		iridium_buffer.buffer_high.hk[0] |= 48;
		break;
	default:
		iridium_buffer.buffer_high.hk[0] |= 16;
		break;
	}

	switch (spi_buffer.from_mcu3_buffer.state3)
	{
	case 129:
		iridium_buffer.buffer_high.hk[0] |= 4;
		break;
	case 66:
		iridium_buffer.buffer_high.hk[0] |= 8;
		break;
	case 36:
		iridium_buffer.buffer_high.hk[0] |= 12;
		break;
	default:
		iridium_buffer.buffer_high.hk[0] |= 4;
		break;
	}

	switch (cur_alt)
	{
	case HIGHALT:
		iridium_buffer.buffer_high.hk[0] |= 64;
		break;
	case MEDALT:
		iridium_buffer.buffer_high.hk[0] |= 128;
		break;
	case LOWALT:
		iridium_buffer.buffer_high.hk[0] |= 192;
		break;
	default:
		iridium_buffer.buffer_high.hk[0] |= 64;
		break;
	}



	//Einordnen der Werte in den Iridium-Sendebuffer nach H�henmodus und Nachrichtennummer

	//Generelle Werte:
	//Nachrichtennummer
	iridium_buffer.buffer_high.msgnum = sysStatus.iridNum;
	//Resetcounter
	iridium_buffer.getbyte[6] = sysStatus.resetCount;
	iridium_buffer.getbyte[7] = spi_buffer.from_mcu3_buffer.resets2;
	iridium_buffer.getbyte[8] = spi_buffer.from_mcu3_buffer.resets3;
	//Batteriespannung:
	iridium_buffer.buffer_high.vbat = spi_buffer.from_mcu3_buffer.vbat;
	//Stateanzeige:

	//Nummernabh�ngige Einzeldaten, unabh�ngig von Flugh�he
	if (sysStatus.sendNum == 1)
	{
		//Systemzeiten zu Nachrichtenbeginn
		iridium_buffer.buffer_high.times[0] = (sysTime()/1000);
		iridium_buffer.buffer_high.times[1] = spi_buffer.from_mcu3_buffer.time2;
		iridium_buffer.buffer_high.times[2] = spi_buffer.from_mcu3_buffer.time3;
	}
	num1 = sysStatus.sendNum - 1;

	//Druckmesswerte+IMU-Rotation + LOW-Quaternionen + HIGH-TC/CJ:
	if (sysStatus.sendNum % 2 != 0)
	{
		iridium_buffer.buffer_high.psdig[num1] = spi_buffer.from_mcu3_buffer.psensExt;
		iridium_buffer.buffer_high.psdig[sysStatus.sendNum] = spi_buffer.from_mcu3_buffer.psensSmd;
		iridium_buffer.buffer_high.psan[num1 / 2] = spi_buffer.from_mcu3_buffer.psensAn;
		iridium_buffer.buffer_high.imurot[num1 * 3] = spi_buffer.from_mcu3_buffer.rotX;
		iridium_buffer.buffer_high.imurot[(num1 * 3) + 1] = spi_buffer.from_mcu3_buffer.rotY;
		iridium_buffer.buffer_high.imurot[(num1 * 3) + 2] = spi_buffer.from_mcu3_buffer.rotZ;

		if (cur_alt == LOWALT)
		{
			iridium_buffer.buffer_low.imuquat[num1 * 2] = spi_buffer.from_mcu3_buffer.quatw;
			iridium_buffer.buffer_low.imuquat[(num1 * 2)+1] = spi_buffer.from_mcu3_buffer.quatx;
			iridium_buffer.buffer_low.imuquat[(num1 * 2)+2] = spi_buffer.from_mcu3_buffer.quaty;
			iridium_buffer.buffer_low.imuquat[(num1 * 2)+3] = spi_buffer.from_mcu3_buffer.quatz;
			iridium_buffer.buffer_low.imucalib[num1] = spi_buffer.from_mcu3_buffer.calib;
		}
		if (cur_alt == HIGHALT)
		{
			num3 = 0;
			for (num2 = (num1*3); num2 < ((num1*3)+6); num2++)
			{
				iridium_buffer.buffer_high.tc[num2] = spi_buffer.from_mcu3_buffer.tcs[num3];
				iridium_buffer.buffer_high.tccj[num2] = spi_buffer.from_mcu3_buffer.cjs[num3];
				num3++;
			}
		}
		if (cur_alt == LOWALT)
		{
			iridium_buffer.buffer_low.gps[num1 * 2] = spi_buffer.from_mcu3_buffer.gpslat;
			iridium_buffer.buffer_low.gps[(num1 * 2) + 1] = spi_buffer.from_mcu3_buffer.gpslon;
			iridium_buffer.buffer_low.gps[(num1 * 2) + 2] = spi_buffer.from_mcu3_buffer.gpshead;
			iridium_buffer.buffer_low.gps[(num1 * 2) + 3] = spi_buffer.from_mcu3_buffer.gpsspeed;
		}

	}

	//IMU-Quaternionen
	if (cur_alt != LOWALT)
	{
		iridium_buffer.buffer_high.imuquat[num1 * 4] = spi_buffer.from_mcu3_buffer.quatw;
		iridium_buffer.buffer_high.imuquat[(num1 * 4) + 1] = spi_buffer.from_mcu3_buffer.quatx;
		iridium_buffer.buffer_high.imuquat[(num1 * 4) + 2] = spi_buffer.from_mcu3_buffer.quaty;
		iridium_buffer.buffer_high.imuquat[(num1 * 4) + 3] = spi_buffer.from_mcu3_buffer.quatz;
		iridium_buffer.buffer_high.imucalib[num1] = spi_buffer.from_mcu3_buffer.calib;
	}

	if ((sysStatus.sendNum - 1) % 4 == 0) //Pollrate von 0,25Hz
	{
		num2 = ((sysStatus.sendNum) - 1) / 4;
		if (cur_alt != HIGHALT)
		{
			for (num3 = 0; num3 < 6; num3++)
			{
				iridium_buffer.buffer_med.tc[(num2 * 6)+num3] = spi_buffer.from_mcu3_buffer.tcs[num3];
				iridium_buffer.buffer_med.tccj[(num2 * 6) + num3] = spi_buffer.from_mcu3_buffer.cjs[num3];
			}
		}
	}
	//1,4,7,10
	//0,4,8,12
	if ((sysStatus.sendNum - 1) % 3 == 0) //Pollrate von 0,33Hz
	{
		if (cur_alt == MEDALT)
		{
			num3 = ((sysStatus.sendNum-1)/3)*4;
			iridium_buffer.buffer_med.gps[num3] = spi_buffer.from_mcu3_buffer.gpslat;
			iridium_buffer.buffer_med.gps[num3+1] = spi_buffer.from_mcu3_buffer.gpslon;
			iridium_buffer.buffer_med.gps[num3 + 2] = spi_buffer.from_mcu3_buffer.gpshead;
			iridium_buffer.buffer_med.gps[num3 + 3] = spi_buffer.from_mcu3_buffer.gpsspeed;
		}
	}

	
	//Falls eine Nachricht komplett: Markiere Iridium als sendebereit
	if (sysStatus.sendNum == 12)
	{
		iridium.newMsg = true;
		sysStatus.sendNum = 0; //Wieder auf 0 setzen, falls im n�chsten Umlauf die Nachricht nicht erkannt wird und der Wert inkrementiert werden muss
	}
	
	//SPI-Flag zur�cksetzen
	newSpi = false;
	return OK;
};

int flight_memo_state(void)
{
#ifdef DBG_SERIALOUTPUT
	delay(DBG_STATEWAIT);
	
#endif // DBG_SERIALOUTPUT
	//Serial.println(freeRam());
	//Die aktuelle Systemzeit im EEPROM speichern
	tBuf.time = sysTime();

	for (int i = 0; i < 4; i++) //Systemzeitoffset in EEPROM speichern
	{
		EEPROM[i + 2].update(tBuf.tArray[i]);
	}
	//Falls nicht schon geschehen: Offsetmarker ACK im EEPROM setzen
	EEPROM[1].update(0x6);
	//Memorytimer updaten
	sysStatus.lastMem = tBuf.time;

	return OK;
};
int test_listen_state(void)
{
#ifdef DBG_SERIALOUTPUT
	delay(DBG_STATEWAIT);
	Serial.println(F("testListen State"));
#endif // DBG_SERIALOUTPUT

	//EEPROM l�schen, um die Warteflag zu entfernen TODO bei fertigen Testing-Codes ersetzen
	for (int i = 0; i < 5; i++) {
		EEPROM[i].update(0);
	}
	while (1);
};
int test_irid_state(void)
{
#ifdef DBG_SERIALOUTPUT
	delay(DBG_STATEWAIT);
	Serial.println(F("testIridium State"));
#endif // DBG_SERIALOUTPUT
};
int test_reset_state(void)
{
#ifdef DBG_SERIALOUTPUT
	delay(DBG_STATEWAIT);
	Serial.println(F("testReset State, waiting for PowerOff"));
#endif // DBG_SERIALOUTPUT
};
int syserror_state(void)
{
#ifdef DBG_SERIALOUTPUT
	delay(DBG_STATEWAIT);
	Serial.println(F("System Error State"));
#endif // DBG_SERIALOUTPUT
};

//Iridium-Parser-FSM

int sig_at_state(void){
#ifdef DBG_SOFTSERIAL
	iridSerial.println(F(">AT"));
#endif

	Serial.print(F("AT\r")); //AT-Kommando senden

	return I_SIG_OK;
}
int sig_e0_state(void){
#ifdef DBG_SOFTSERIAL
	iridSerial.println(F(">ATE0"));
#endif
	Serial.print(F("ATE0\r")); //Echo ausschalten
	cur_imetastate = META_INIT_ATE0; //Metazustand ver�ndern
	return I_SIG_OK;
}
int sig_d0_state(void){
#ifdef DBG_SOFTSERIAL
	iridSerial.println(F(">ATD0"));
#endif
	Serial.print(F("AT&D0\r")); //Flow Control ausschalten
	cur_imetastate = META_INIT_ATD0; //Metazustand ver�ndern
	return I_SIG_OK;
}
int sig_csq_state(void){
#ifdef DBG_SOFTSERIAL
	iridSerial.println(F(">AT+CSQ"));
#endif
	Serial.print(F("AT+CSQ\r")); //Signalqualit�t abfragen
	return I_SIG_OK;
}
int sig_f_state(void){
#ifdef DBG_SOFTSERIAL
	iridSerial.println(F(">AT*F"));
#endif
	return I_SIG_OK;
	Serial.print(F("AT*F\r")); //Bereitmachen zum Poweroff
}
int sig_cier_state(void){
#ifdef DBG_SOFTSERIAL
	iridSerial.println(F(">AT+CIER=1,1,1"));
#endif
	Serial.print(F("AT+CIER=1,1,1\r")); //Automatische Signalqualit�tsindikatoren einschalten
	cur_imetastate = META_INIT_CIER; //Metazustand ver�ndern
	return I_SIG_OK;
}
int sig_sbdi_state(void){
#ifdef DBG_SOFTSERIAL
	iridSerial.println(F(">AT+SBDI"));
#endif
	if (!iridium.isBusy){
		Serial.print(F("AT+SBDI\r")); //Sendeversuch
		iridium.isBusy = true; //Gerade wird gesendet; Kein weiterer Sendebefehl sollte geschickt werden
		return I_SIG_OK;
	}
	else
	{
		return I_NOVALUE;
	}
}
int sig_sbdd0_state(void){
#ifdef DBG_SOFTSERIAL
	iridSerial.println(F(">AT+SBDD0"));
#endif
	Serial.print(F("AT+SBDD0\r")); //Nachrichtenbuffer l�schen
	return I_SIG_OK;
}
int sig_sbdwt_state(void){
#ifdef DBG_SOFTSERIAL
	iridSerial.println(F(">AT+SBDWT=Test"));
#endif
	Serial.print(F("AT+SBDWT=Test\r")); //Testnachricht schreiben
	return I_SIG_OK;
}
int sig_sbdwb_state(void){
#ifdef DBG_SOFTSERIAL
	iridSerial.println(F(">AT+SBDWB"));
#endif
	//Check, ob seit dem letzten "OK"
	if (!iridium.isBusy){
		Serial.print(F("AT+SBDWB=340\r")); //Transceiver zum Empfangen einer 340B-Bin�rnachricht bereit machen
		iridium.isBusy = true; //Kein weiterer Befehl sollte gesendet werden, bevor der erste abgearbeitet ist
	}
	return I_SIG_OK;
}
int sig_repeat_state(void){
#ifdef DBG_SOFTSERIAL
	iridSerial.println(F(">A/"));
#endif
	Serial.print(F("A/")); //Letztes Kommando wiederholen
	return I_SIG_OK;
}

int read_a_state(void)
{
	switch (iridium_read())
	{
	case 'A':
#ifdef DBG_IRIDSTATES
		Serial.println("readA");
#endif
		return I_READ_OK;
	case 0x15:
		//if (){ //Hier timeout-Bedingung einf�gen
		//	return I_TIMEOUT;
		//}
		//else{
			return I_NOVALUE;
		//}
	default:
		return I_UNKNOWN;
	}
}
int read_b_state(void)
{
	switch (iridium_read())
	{
	case 'B':
#ifdef DBG_IRIDSTATES
		Serial.println("readB");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_c_state(void)
{
	switch (iridium_read())
	{
	case 'C':
#ifdef DBG_IRIDSTATES
		Serial.println("readC");
#endif
		return I_READ_OK;
	case 'S':
#ifdef DBG_IRIDSTATES
		Serial.println("readS");
#endif
		return I_S;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_d_state(void)
{
	switch (iridium_read())
	{
	case 'D':
#ifdef DBG_IRIDSTATES
		Serial.println("readD");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_e_state(void)
{
	switch (iridium_read())
	{
	case 'E':
#ifdef DBG_IRIDSTATES
		Serial.println("readE");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_i_state(void)
{
	switch (iridium_read())
	{
	case 'I':
#ifdef DBG_IRIDSTATES
		Serial.println("readI");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_k_state(void)
{
	switch (iridium_read())
	{
	case 'K':
#ifdef DBG_IRIDSTATES
		Serial.println("readK");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_o_state(void)
{
	switch (iridium_read())
	{
	case 'O':
#ifdef DBG_IRIDSTATES
		Serial.println("readO");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_q_state(void)
{
	switch (iridium_read())
	{
	case 'Q':
#ifdef DBG_IRIDSTATES
		Serial.println("readQ");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_r_state(void)
{
	switch (iridium_read())
	{
	case 'R':
#ifdef DBG_IRIDSTATES
		Serial.println("readR");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_s_state(void)
{
	switch (iridium_read())
	{
	case 'S':
#ifdef DBG_IRIDSTATES
		Serial.println("readS");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_t_state(void)
{
	switch (iridium_read())
	{
	case 'T':
#ifdef DBG_IRIDSTATES
		Serial.println("readT");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_v_state(void)
{
	switch (iridium_read())
	{
	case 'V':
#ifdef DBG_IRIDSTATES
		Serial.println("readV");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_y_state(void)
{
	switch (iridium_read())
	{
	case 'Y':
#ifdef DBG_IRIDSTATES
		Serial.println("readY");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_end_state(void)
{
	switch (iridium_read())
	{
	case '\r':
#ifdef DBG_IRIDSTATES2
		Serial.println("read_r");
#endif
		return I_CR;
	case '\n':
		if (iridium.secondLF){
			iridium.secondLF = false; //Wieder zur�cksetzen
#ifdef DBG_IRIDSTATES2
			Serial.println("read_2n");
#endif
			return I_SEC_LF;
		}
		else{
			iridium.secondLF = true;
#ifdef DBG_IRIDSTATES2
			Serial.println("read_1n");
#endif
			return I_FIRST_LF;
		}
	case 0x15:
		return I_NOVALUE;
	default:
#ifdef DBG_IRIDSTATES2
		Serial.println("read_unknown");
#endif
		return I_UNKNOWN;
	}
}
int read_plus_state(void)
{
	switch (iridium_read())
	{
	case '+':
#ifdef DBG_IRIDSTATES
		Serial.println("read_+");
#endif
		return I_READ_OK;
	case 'R':
#ifdef DBG_IRIDSTATES
		Serial.println("read_R");
#endif
		return I_R;
	case 'O':
#ifdef DBG_IRIDSTATES
		Serial.println("read_O");
#endif
		return I_O;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_colon_state(void)
{
	switch (iridium_read())
	{
	case ':':
#ifdef DBG_IRIDSTATES
		Serial.println("read_:");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_comma_state(void)
{
	switch (iridium_read())
	{
	case ',':
#ifdef DBG_IRIDSTATES
		Serial.println("read_comma");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_space_state(void)
{
	switch (iridium_read())
	{
	case 0x20: //Leerzeichen in ASCII
#ifdef DBG_IRIDSTATES
		Serial.println("read_space");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_snum_state(void)
{
	switch (iridium_read())
	{
	case '0':
#ifdef DBG_IRIDSTATES
		Serial.println("read_s0");
#endif
		return I_ZERO;
	case '1':
#ifdef DBG_IRIDSTATES
		Serial.println("read_s1");
#endif
		return I_ONE;
	case '2':
#ifdef DBG_IRIDSTATES
		Serial.println("read_s2");
#endif
		return I_TWO;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_num_state(void)
{
	switch (iridium_read())
	{
	case '0':
#ifdef DBG_IRIDSTATES
		Serial.println("read_0");
#endif
		return I_ZERO;
	case '1':
#ifdef DBG_IRIDSTATES
		Serial.println("read_1");
#endif
		return I_ONE;
	case '2':
#ifdef DBG_IRIDSTATES
		Serial.println("read_2");
#endif
		return I_TWO;
	case '3':
#ifdef DBG_IRIDSTATES
		Serial.println("read_3");
#endif
		return I_THREE;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_cnum1_state(void)
{
	iridium.readSig = false; //Flag vor erneutem CIEV-Auslesen zur�cksetzen
	switch (iridium_read())
	{
	case '0': //Signalqualit�t wird ausgegeben
		iridium.readSig = true;
#ifdef DBG_IRIDSTATES
		Serial.println("read_c0");
#endif
		return I_READ_OK;
	case '1': //Netzwerkstatus wird ausgegeben
#ifdef DBG_IRIDSTATES
		Serial.println("read_c1");
#endif
		return I_READ_OK;
	case 0x15:
		return I_NOVALUE;
	default:
		return I_UNKNOWN;
	}
}
int read_cnum2_state(void)
{
	if (iridium.readSig)
	{
		switch (iridium_read())
		{
		case '0':
			iridium.cur_sig = S_0;
			return I_READ_OK;
		case '1':
			iridium.cur_sig = S_1;
			return I_READ_OK;
		case '2':
			//Hier wird gecheckt, ob die Signalst�rke zu- oder abnimmt
			if (iridium.cur_sig < S_2)
			{
				iridium.isRising = true;
			}
			else
			{
				iridium.isRising = false;
			}
			iridium.cur_sig = S_2;
			return I_READ_OK;
		case '3':
			iridium.cur_sig = S_3;
			return I_READ_OK;
		case '4':
			iridium.cur_sig = S_4;
			return I_READ_OK;
		case '5':
			iridium.cur_sig = S_5;
			return I_READ_OK;
		case 0x15:
			return I_NOVALUE;
		default:
			return I_UNKNOWN;
		}
	}
	else
	{
		switch (iridium_read())
		{
		case '0':
			iridium.isOnline = false; //Kein Netz
			return I_READ_OK;
		case '1':
			iridium.isOnline = true; //Netz verf�gbar
			return I_READ_OK;
		case 0x15:
			return I_NOVALUE;
		default:
			return I_UNKNOWN;
		}
	}
}

int check_msg_state(void)
{
	if (!iridium.isBusy && iridium.newMsg)
	{
#ifdef DBG_IRIDSTATES2
		Serial.println("newmsg");
#endif
		return I_READ_OK; //Es ist eine neue Nachricht f�r den Transceiver im MCU-Speicher vorhanden
	}
	else
	{
		return I_NOVALUE; //Keine neue Nachricht
	}
}
int check_send_state(void)
{
	if (iridium.pending && ((sysTime()-iridium.lastFail)> IRID_MININT)) //Ist eine zu sendende Nachricht vorhanden und ist der letzte Sendefail lange genug her?
	{
#ifdef DBG_IRIDSTATES2
		Serial.println("pending");
#endif
		return I_READ_OK; //Es ist eine Nachricht zum Senden vorhanden
	}
	else
	{
		return I_NOVALUE; //Die letzte gepufferte Nachricht wurde erfolgreich gesendet, gerade muss nichts gesendet werden
	}
}
int check_sig_state(void)
{
	if (iridium.isOnline) //Ist Netzempfang gegeben?
	{
		switch (iridium.cur_sig)
		{
		case S_0:
			return I_NOVALUE;
		case S_1:
			return I_NOVALUE;
		case S_2:
			if (iridium.isRising) //2 ist eigentlich zu schwach zum Senden. Ist jedoch mit einem Anstieg auf 3 zu rechnen, kann gesendet werden
			{
				return I_READ_OK;
			}
			else
			{
				return I_NOVALUE;
			}
		case S_3:
			return I_READ_OK; //TODO hier k�nnte noch mehr Logik hin, um bei raschem Signalabfall das Senden bei 3 zu verhindern
		case S_4:
			return I_READ_OK; //Senden bei 4 ist kein Problem
		case S_5:
			return I_READ_OK; //Senden bei 5 ist kein Problem
		default:
			return I_NOVALUE; //Beim Auslesen ist ein Problem aufgetreten
		}
	}
	else
	{
		return I_NOVALUE; //Kein Signal
	}
}
//Bei allen send_-States gilt iridium.isbusy noch! Erst nach OK wird Eingabe wieder freigeschaltet
int send_success_state(void)
{
	//Nachricht wurde erfolgreich gesendet
	iridium.pending = false;
	//Failcounter wird zur�ckgesetzt
	iridium.failCount = 0;

	return I_READ_OK;
}
int send_fail_state(void)
{
	//Nachricht wurde nicht gesendet
	iridium.pending = true;
	//Failcounter wird inkrementiert
	iridium.failCount++;
	iridium.lastFail = sysTime(); //Zeitpunkt f�r "Zeitabstand" bei schneller Fehlerfolge speichern
	return I_READ_OK;
}
int send_nomsg_state(void)
{
	//Nachricht wurde nicht gesendet: Buffer ist leer
	iridium.pending = false;
	iridium.bufferFull = false;
	//Failcounter wird zur�ckgesetzt
	iridium.failCount = 0;

	return I_READ_OK;
}

int write_buf_state(void)
{
	//Schreibe den Iridiumbuffer mitsamt Pr�fsumme
	uint16_t checksum = 0;
	for (int i = 0; i < 340;i++)
	{
		Serial.write(iridium_buffer.getbyte[i]);
		checksum += (uint16_t)iridium_buffer.getbyte[i];
	}
	//Schreibe Pr�fsumme
	Serial.write(checksum >> 8);
	Serial.write(checksum & 0xFF);

	return I_READ_OK;
}
int write_success_state(void)
{
	iridium.newMsg = false; //Nachricht wurde in Buffer geschrieben
	iridium.bufferFull = true; // Buffer ist gef�llt
	iridium.pending = true; //Neue Nachricht muss verschickt werden
	//Failcounter zur�cksetzen, da neue Nachricht
	iridium.failCount = 0;
	//Iridium-Nachrichtennummer erh�hen
	sysStatus.iridNum++;

	//Buffer mit Nullen f�llen f�r den n�chsten Durchgang
	for (int i = 0; i < 340; i++)
	{
		iridium_buffer.getbyte[i] = 0;
	}
	return I_READ_OK;
}
int write_timeout_fail(void)
{
	//TODO evtl. noch andere Verarbeitungslogik
	iridium.bufferFull = false; // Buffer ist gef�llt mit veralteten Daten
	return I_READ_OK;
}
int write_chkfail_state(void)
{
	iridium.bufferFull = false; // Buffer ist gef�llt mit veralteten Daten
	return I_READ_OK;
}
int write_sizefail_state(void)
{
	iridium.bufferFull = false; // Buffer ist gef�llt mit veralteten Daten
	return I_READ_OK;
}

int switch_idle_state(void)
{
#ifdef DBG_IRIDSTATES
	Serial.println("switch:Idle");
#endif
	//WICHTIG: Bugfix: Iridium wird nur auf nicht mehr besch�ftigt gesetzt, wenn a) Der Buffer fertig geschrieben ist(Als Reaktion auf SBDWB) oder SBDI fertig gesendet hat(als Reaktion auf SBDI).
	//Als Reaktion auf einen +CIEV-Lesevorgang wird busy nicht resetted!
	if (cur_imetastate != META_READ_CIEV)
	{
		iridium.isBusy = false; //Iridium ist nicht mehr besch�ftigt
	}
	//Metastatus �ndern
	cur_imetastate = META_IDLE;
	//LF-Z�hler zur�cksetzen
	iridium.secondLF = false;
	return I_READ_OK;
}

int switch_rciev_state(void)
{
#ifdef DBG_IRIDSTATES
	Serial.println("switch:RCiev");
#endif
	//Metastatus �ndern
	cur_imetastate = META_READ_CIEV;
	return I_READ_OK;
}

int switch_bwrite_state(void)
{
#ifdef DBG_IRIDSTATES
	Serial.println("switch:BWrite");
#endif
	//lf-Counter resetten
	iridium.secondLF = false;
	//Metastatus �ndern
	cur_imetastate = META_SEND_BWRITE;
	return I_READ_OK;
}

int switch_rbwrite_state(void)
{
#ifdef DBG_IRIDSTATES
	Serial.println("switch:RBWrite");
#endif
	//lf-Counter resetten
	iridium.secondLF = false;
	//Metastatus �ndern
	cur_imetastate = META_READ_BWRITE;
	return I_READ_OK;
}

int switch_rsbdi_state(void)
{
#ifdef DBG_IRIDSTATES
	Serial.println("switch:RSBDI");
#endif
	//lf-Counter resetten
	iridium.secondLF = false;
	//Metastatus �ndern
	cur_imetastate = META_READ_SBDI;
	return I_READ_OK;
}

int switch_rok_state(void)
{
#ifdef DBG_IRIDSTATES
	Serial.println("switch:ROK");
#endif
	//lf-Counter resetten
	iridium.secondLF = false;
	//Metastatus �ndern
	cur_imetastate = META_READ_OK;
	return I_READ_OK;
}

//Interrupt Service Routinen
void int_mcu2call()
{
	mcu2call = true;
}

void int_mcu3call()
{
	mcu3call = true;
}

/*int freeRam()
{
	extern int __heap_start, *__brkval;
	int v;
	return (int)&v - (__brkval == 0 ? (int)&__heap_start : (int)__brkval);
}*/

ISR(SPI_STC_vect)
{
	SPI_readAnything_ISR(spi_buffer);
	newSpi = true;
}  // end of interrupt routine SPI_STC_vect