#pragma once
#include "Defines.h"
// Hier werden alle FSM-Modi und die �bergangsfunktionen sowie alle Events untergebracht:
//TODO diese FSM muss �berarbeitet werden, um nur mit Iridium umgehen zu k�nnen
// Unter-FSMs f�r die einzelnen Hauptmodi (Low-Level States), gesammelt
//Haupt-FSM (High-Level States)

int init_waitl_state(void);
int init_waits_state(void);
int init_recall_state(void);
int init_debugcheck_state(void);
int init_start_state(void);

int flight_main_state(void);
int flight_sync_state(void);
int flight_store_state(void);
int flight_memo_state(void);

int test_listen_state(void);
int test_irid_state(void);
int test_reset_state(void);

int syserror_state(void);

int sig_at_state(void);
int sig_e0_state(void);
int sig_d0_state(void);
int sig_csq_state(void);
int sig_f_state(void);
int sig_cier_state(void);
int sig_sbdi_state(void);
int sig_sbdd0_state(void);
int sig_sbdwt_state(void);
int sig_sbdwb_state(void);
int sig_repeat_state(void);

int read_a_state(void);
int read_b_state(void);
int read_c_state(void);
int read_d_state(void);
int read_e_state(void);
int read_i_state(void);
int read_k_state(void);
int read_o_state(void);
int read_q_state(void);
int read_r_state(void);
int read_s_state(void);
int read_t_state(void);
int read_v_state(void);
int read_y_state(void);
int read_end_state(void);
int read_plus_state(void);
int read_colon_state(void);
int read_comma_state(void);
int read_space_state(void);
int read_snum_state(void);
int read_num_state(void);
int read_cnum1_state(void);
int read_cnum2_state(void);

int check_msg_state(void);
int check_send_state(void);
int check_sig_state(void);
int send_success_state(void);
int send_fail_state(void);
int send_nomsg_state(void);

int write_buf_state(void);
int write_success_state(void);
int write_timeout_fail(void);
int write_chkfail_state(void);
int write_sizefail_state(void);

int switch_idle_state(void);
int switch_rciev_state(void);
int switch_bwrite_state(void);
int switch_rsbdi_state(void);
int switch_rbwrite_state(void);
int switch_rok_state(void);



//Der untere Array und die folgende enum m�ssen exakt gleich sein!
int(*state[])(void) = {
						init_waitl_state,
						init_waits_state,
						init_recall_state,
						init_debugcheck_state,
						init_start_state,

						flight_main_state,
						flight_store_state,
						flight_memo_state,

						test_listen_state,
						test_irid_state,
						test_reset_state,

						syserror_state,
};

enum OBC_STATES //Zust�nde
{
	INIT_WAITL,
	INIT_WAITS,
	INIT_RECALL,
	INIT_DEBUGCHECK,
	INIT_START,

	FLIGHT_MAIN,
	FLIGHT_STORE,
	FLIGHT_MEMORIZE,

	TESTING_LISTEN,
	TESTING_IRIDIUM,
	TESTING_RESET,

	SYS_ERROR,
};

//Der untere Array und die folgende Enum m�ssen exakt gleich sein!
int(*iridState[])(void) = { //Zust�nde des Iridiumparsers
	sig_at_state,
	sig_e0_state,
	sig_d0_state,
	sig_csq_state,
	sig_f_state,
	sig_cier_state,
	sig_sbdi_state,
	sig_sbdd0_state,
	sig_sbdwt_state,
	sig_sbdwb_state,
	sig_repeat_state,
	
	read_a_state,
	read_b_state,
	read_c_state,
	read_d_state,
	read_e_state,
	read_i_state,
	read_k_state,
	read_o_state,
	read_q_state,
	read_r_state,
	read_s_state,
	read_t_state,
	read_v_state,
	read_y_state,
	read_end_state,
	read_plus_state,
	read_colon_state,
	read_comma_state,
	read_space_state,
	read_snum_state,
	read_num_state,
	read_cnum1_state,
	read_cnum2_state,

	check_msg_state,
	check_send_state,
	check_sig_state,
	send_success_state,
	send_fail_state,
	send_nomsg_state,

	write_buf_state,
	write_success_state,
	write_timeout_fail,
	write_chkfail_state,
	write_sizefail_state,

	switch_idle_state,
	switch_rciev_state,
	switch_bwrite_state,
	switch_rsbdi_state,
	switch_rbwrite_state,
	switch_rok_state
};

enum IRID_STATES //Zust�nde des Iridiumparsers
{
	SIG_AT,
	SIG_E0,
	SIG_D0,
	SIG_CSQ,
	SIG_F,
	SIG_CIER,
	SIG_SBDI,
	SIG_SBDD0,
	SIG_SBDWT,
	SIG_SBDWB,
	SIG_REPEAT,

	READ_A,
	READ_B,
	READ_C,
	READ_D,
	READ_E,
	READ_I,
	READ_K,
	READ_O,
	READ_Q,
	READ_R,
	READ_S,
	READ_T,
	READ_V,
	READ_Y,
	READ_END,
	READ_PLUS,
	READ_COLON,
	READ_COMMA,
	READ_SPACE,
	READ_SNUM,
	READ_NUM,
	READ_CNUM1,
	READ_CNUM2,

	CHECK_MSG,
	CHECK_SEND,
	CHECK_SIG,

	SEND_SUCCESS,
	SEND_FAIL,
	SEND_NOMSG,

	WRITE_BUF,
	WRITE_SUCCESS,
	WRITE_TIMEOUT,
	WRITE_CHKFAIL,
	WRITE_SIZEFAIL,

	SWITCH_IDLE,
	SWITCH_RCIEV,
	SWITCH_BWRITE,
	SWITCH_RSBDI,
	SWITCH_RBWRITE,
	SWITCH_ROK
};

enum IRID_META_STATES //Metazust�nde des Iridiumparsers
{
	META_INIT_AT,
	META_INIT_ATE0,
	META_INIT_ATD0,
	META_INIT_CIER,
	META_IDLE,
	META_READ_CIEV,
	META_READ_SBDI,
	META_SEND_BWRITE,
	META_READ_BWRITE,
	META_READ_OK
};

enum RET_CODES //R�ckgabewerte eines Zustandes
{ 
	MIN_RET,
	DEF, //Default: Standardaktion ohne besondere Vorkommnisse
	FIRST, //Recallspezifisch: Erststart erkannt
	SEC, //Recallspezifisch: Zweitstart erkannt (Offsettimer noch 0)
	RES, //Recallspezifisch: Start nach erst- und zweitstart erkannt (reset im Betrieb)
	OK, //State erfolgreich durchlaufen
	FAIL, //State fehlgeschlagen/Fehlerereignis aufgetreten
	DEBUG, //Debugereignis (Debugpin registriert)
	REPEAT, //State muss noch einmal durchlaufen werden
	ERROR, //Kein passender Returnwert zur�ckgegeben/Kritischer Systemfehler aufgetreten
	SPIGO, //Hat einen SPI-Interrupt empfangen und geht in den SPI-Storemodus �ber
	MEMTIMER, //Der Timer f�r das Sichern des Zustandes hat angeschlagen

	TEST_IRIDIUM, //Schaltet Iridium ein, wartet auf gutes Signal, sendet 2 Testnachrichten, schaltet dann wieder ab
	TEST_RESET, //Schaltet alles aus, l�scht den EEPROM und macht sich bereit f�rs Abschalten
	MAX_RET
};

enum IRID_RET_CODES //R�ckgabewerte des Iridiumparsers
{
	I_MIN_RET,
	I_NOVALUE, //Nichts zu lesen vorhanden
	I_READ_OK, //Das gesuchte Zeichen ist erkannt worden; Wird benutzt, um die Anzahl der Returnwerte klein zu halten
	I_SIG_OK, //Senden eines Kommandos abgeschlossen
	I_CR, // /r-Zeichen (Carriage Return)
	I_FIRST_LF, // /n-Zeichen (Line Feed), das erste in einer Reihe
	I_SEC_LF,  //", das zweite in einer Reihe
	I_O,
	I_R,
	I_S,
	I_ZERO,
	I_ONE,
	I_TWO,
	I_THREE,
	I_UNKNOWN,
	I_TIMEOUT, //Die Wartezeit auf eine spezielle Antwort ist abgelaufen
	I_CHECK, //Die ISU ist gerade nicht bei der Verarbeitung eines Befehls, es sollte nach vorhandenen Nachrichten geschaut werden
	I_REPEAT, //Lesen noch nicht abgeschlossen/Leercharaktere werden �bersprungen
	I_MAX_RET
};

enum ALTITUDE //H�henmodus des MCUs
{
	HIGHALT,
	MEDALT,
	LOWALT
};

struct transition {
	OBC_STATES source_state;
	RET_CODES   ret_code;
	OBC_STATES dest_state;
};

struct irid_transition {
	IRID_STATES source;
	IRID_RET_CODES ret;
	IRID_STATES destin;
};
//Transition-Funktion:
OBC_STATES lookup_transitions(OBC_STATES state, RET_CODES ret);
//Transition-Funktion f�r Iridium:
IRID_STATES lookup_irid(IRID_META_STATES meta, IRID_STATES state, IRID_RET_CODES ret);

transition state_transitions[] = 
{
	{ INIT_DEBUGCHECK, DEF, INIT_RECALL },
	{ INIT_DEBUGCHECK, DEBUG, TESTING_LISTEN }, //Achtung, im Moment k�nnte die Kapsel durch einen Debugpin-Fehler inert geschaltet werden!
	{ INIT_RECALL, FIRST, INIT_WAITL }, //Erststart erkannt, MCU wartet lange
	{ INIT_RECALL, SEC, INIT_WAITS }, //Zweitstart erkannt, MCU wartet kurz (Auswurf/Kurze Stabilisierung)
	{ INIT_RECALL, RES, INIT_START }, //Reset erkannt, kein Erststart und ein Offsettimer !=0 wurde ausgelesen
	{ INIT_RECALL, FAIL, SYS_ERROR },
	{ INIT_WAITL, OK, TESTING_RESET }, //Nach der langen Wartezeit wird automatisch abgeschaltet, Iridium wurde nicht eingeschaltet
	{ INIT_WAITS, OK, INIT_START }, //Wartezeit h�ngt vom "Erststart-Flag" ab: Minuten oder wenige Sekunden, siehe Defines.h
	{ INIT_START, OK, FLIGHT_MAIN },
	
	{ FLIGHT_MAIN, REPEAT, FLIGHT_MAIN },
	{ FLIGHT_MAIN, SPIGO, FLIGHT_STORE },
	{ FLIGHT_MAIN, MEMTIMER, FLIGHT_MEMORIZE },
	{ FLIGHT_STORE, OK, FLIGHT_MAIN },
	{ FLIGHT_MEMORIZE, OK, FLIGHT_MAIN },
	
	{ TESTING_LISTEN, TEST_IRIDIUM, TESTING_IRIDIUM },
	{ TESTING_LISTEN, TEST_RESET, TESTING_RESET },
	{ TESTING_IRIDIUM, OK, TESTING_LISTEN }
};

irid_transition initat_trans[] = //Initialisierung: Senden des ersten AT-Kommandos
{
	{ SIG_AT, I_SIG_OK, READ_A },

	{ READ_A, I_NOVALUE, READ_A },
	{ READ_A, I_TIMEOUT, SIG_AT }, //Anfangswert hat einen Timeout, um das Kommando noch einmal zu senden
	{ READ_A, I_READ_OK, READ_T },
	{ READ_A, I_UNKNOWN, READ_A }, //Falls unbekannte Werte gesendet werden (Fehler): Es wird weiter versucht, A zu lesen

	{ READ_T, I_NOVALUE, READ_T },
	{ READ_T, I_READ_OK, READ_END },

	{ READ_END, I_NOVALUE, READ_END },
	{ READ_END, I_CR, READ_END },
	{ READ_END, I_FIRST_LF, READ_O },
	{ READ_END, I_SEC_LF, SIG_E0 }, //Hier wird der Metazustand ver�ndert

	{ READ_O, I_NOVALUE, READ_O },
	{ READ_O, I_READ_OK, READ_K },

	{ READ_K, I_NOVALUE, READ_K },
	{ READ_K, I_READ_OK, READ_END }
};

irid_transition initate0_trans[] =
{
	{ SIG_E0, I_SIG_OK, READ_A },

	{ READ_A, I_NOVALUE, READ_A },
	{ READ_A, I_READ_OK, READ_T },

	{ READ_T, I_NOVALUE, READ_T },
	{ READ_T, I_READ_OK, READ_E },

	{ READ_E, I_NOVALUE, READ_E },
	{ READ_E, I_READ_OK, READ_NUM },

	{ READ_NUM, I_NOVALUE, READ_NUM },
	{ READ_NUM, I_ZERO, READ_END },

	{ READ_END, I_NOVALUE, READ_END },
	{ READ_END, I_CR, READ_END },
	{ READ_END, I_FIRST_LF, READ_O },
	{ READ_END, I_SEC_LF, SIG_D0 }, //Hier muss der Metazustand ver�ndert werden //Hier muss die LF-Flag wieder zur�ckgesetzt werden

	{ READ_O, I_NOVALUE, READ_O },
	{ READ_O, I_READ_OK, READ_K },

	{ READ_K, I_NOVALUE, READ_K },
	{ READ_K, I_READ_OK, READ_END }
};

irid_transition initatd0_trans[] = //Ab hier werden die Auswertetabellen kleiner, weil die Echofunktion ausgeschaltet wurde
{
	{ SIG_D0, I_SIG_OK, READ_END },

	{ READ_END, I_NOVALUE, READ_END },
	{ READ_END, I_CR, READ_END },
	{ READ_END, I_FIRST_LF, READ_O },
	{ READ_END, I_SEC_LF, SIG_CIER }, //Hier muss der Metazustand ver�ndert werden //Hier muss die LF-Flag wieder zur�ckgesetzt werden

	{ READ_O, I_NOVALUE, READ_O },
	{ READ_O, I_READ_OK, READ_K },

	{ READ_K, I_NOVALUE, READ_K },
	{ READ_K, I_READ_OK, READ_END }
};

irid_transition initcier_trans[] = //Eigentlich �berfl�ssig, da mit d0 identisch. So aber �bersichtlicher
{
	{ SIG_CIER, I_SIG_OK, READ_END },

	{ READ_END, I_NOVALUE, READ_END },
	{ READ_END, I_CR, READ_END },
	{ READ_END, I_FIRST_LF, READ_O },
	{ READ_END, I_SEC_LF, SWITCH_IDLE }, //Hier muss der Metazustand ver�ndert werden //Hier muss die LF-Flag wieder zur�ckgesetzt werden

	{ READ_O, I_NOVALUE, READ_O },
	{ READ_O, I_READ_OK, READ_K },

	{ READ_K, I_NOVALUE, READ_K },
	{ READ_K, I_READ_OK, READ_END }
};
//TODO, wichtig: Bei Aufwachen aus Reset muss Iridium anders angesprochen werden (Pr�fung auf Echo, falls vorhanden normales Init, falls nicht, direkt zu idle)
irid_transition idle_trans[] = //In diesem Zustand verbringt die Iridium-FSM die meiste Zeit; Es wird auf das Vorhandensein einer zu sendenden Nachricht gewartet und auf CIEV-Antworten gehorcht
{
	{ SWITCH_IDLE, I_READ_OK, READ_END},
	{ READ_END, I_UNKNOWN, READ_END}, //Falls unbekannte Zeichen auftauchen: Ignorieren
	{ READ_END, I_NOVALUE, CHECK_MSG }, // /r hat Vorrang vor einer neuen Nachricht
	{ READ_END, I_CR, SWITCH_RCIEV }, //Hier Metazustand ver�ndern -> READ_CIEV
	{ CHECK_MSG, I_NOVALUE, CHECK_SEND }, //Neue Nachrichten in Buffer schreiben hat Vorrang vor Senden einer Nachricht
	{ CHECK_SEND, I_NOVALUE, READ_END }, //Pr�fen, ob ungesendete Nachrichten vorliegen (IM Transceiverbuffer)
	{ CHECK_SEND, I_READ_OK, CHECK_SIG }, //Falls ungesendete Nachrichten vorliegen, pr�fe Signalst�rke
	{ CHECK_SIG, I_NOVALUE, READ_END }, //Falls kein Signal/zu schwaches Signal, warte auf Verbesserung
	{ CHECK_SIG, I_READ_OK, SIG_SBDI }, //Falls Signal stark genug, starte Sendeversuch
	{ CHECK_MSG, I_READ_OK, SIG_SBDWB }, //Falls neue Nachricht auf MCU vollst�ndig, f�lle Transceiverbuffer TODO: Wie kann verhindert werden, dass immer nur gef�llt statt gesendet wird?
	{ SIG_SBDWB, I_SIG_OK, READ_END }, //Sobald Bufferschreibekommando gesendet wurde, gehe auf Idle und warte auf Antwort
	//{ SIG_SBDI, I_SIG_OK, READ_END } // Sobald Sendekommando geschickt, warte auf Antwort: Wechsel zu READSBDI
	{ SIG_SBDI, I_SIG_OK, SWITCH_RSBDI },
	{ SIG_SBDI, I_NOVALUE, READ_END }
};

irid_transition readciev_trans[] = 
{
	{ SWITCH_RCIEV, I_READ_OK, READ_END },
	{ READ_END, I_NOVALUE, READ_END },
	{ READ_END, I_CR, READ_END },
	{ READ_END, I_FIRST_LF, READ_PLUS },
	{ READ_END, I_SEC_LF, SWITCH_IDLE }, //Hier muss die LF-Flag wieder zur�ckgesetzt werden und der Metazustand wird angepasst

	{ READ_PLUS, I_NOVALUE, READ_PLUS },
	{ READ_PLUS, I_READ_OK, READ_C },
	{ READ_PLUS, I_R, SWITCH_BWRITE }, //Metazustand ver�ndern -> SEND_BWRITE
	{ READ_PLUS, I_O, READ_K},

	{ READ_K, I_READ_OK, READ_END},

	{ READ_C, I_NOVALUE, READ_C },
	{ READ_C, I_READ_OK, READ_I },
	//{ READ_C, I_S, SWITCH_RSBDI}, //Metazustand ver�ndern -> READ_SBDI, LF-Flag zur�cksetzen

	{ READ_I, I_NOVALUE, READ_I },
	{ READ_I, I_READ_OK, READ_E },

	{ READ_E, I_NOVALUE, READ_E },
	{ READ_E, I_READ_OK, READ_V },

	{ READ_V, I_NOVALUE, READ_V },
	{ READ_V, I_READ_OK, READ_COLON },

	{ READ_COLON, I_NOVALUE, READ_COLON },
	{ READ_COLON, I_READ_OK, READ_CNUM1 },

	{ READ_CNUM1, I_NOVALUE, READ_CNUM1 },
	{ READ_CNUM1, I_READ_OK, READ_COMMA }, //Entweder 0 oder 1 wurde eingelesen

	{ READ_COMMA, I_NOVALUE, READ_COMMA },
	{ READ_COMMA, I_READ_OK, READ_CNUM2 },

	{ READ_CNUM2, I_NOVALUE, READ_CNUM2 },
	{ READ_CNUM2, I_READ_OK, READ_END }
};

irid_transition readsbdi_trans[] =
{
	//{ SWITCH_RSBDI, I_READ_OK, READ_B },
	{ SWITCH_RSBDI, I_READ_OK, READ_END },

	{ READ_END, I_NOVALUE, READ_END },
	{ READ_END, I_UNKNOWN, READ_END},
	{ READ_END, I_CR, READ_END },
	//{ READ_END, I_FIRST_LF, SWITCH_IDLE },
	{ READ_END, I_FIRST_LF, READ_PLUS },
	//{ READ_END, I_SEC_LF, SWITCH_IDLE },
	{ READ_END, I_SEC_LF, SWITCH_ROK },

	{ READ_PLUS, I_NOVALUE, READ_PLUS },
	{ READ_PLUS, I_READ_OK, READ_S },

	{ READ_S, I_NOVALUE, READ_S },
	{ READ_S, I_READ_OK, READ_B },

	{ READ_B, I_NOVALUE, READ_B },
	{ READ_B, I_READ_OK, READ_D },

	{ READ_D, I_NOVALUE, READ_D },
	{ READ_D, I_READ_OK, READ_I },

	{ READ_I, I_NOVALUE, READ_I },
	{ READ_I, I_READ_OK, READ_COLON },

	{ READ_COLON, I_NOVALUE, READ_COLON },
	{ READ_COLON, I_READ_OK, READ_SPACE },

	{ READ_SPACE, I_NOVALUE, READ_SPACE },
	{ READ_SPACE, I_READ_OK, READ_SNUM },

	{ READ_SNUM, I_NOVALUE, READ_SNUM },
	{ READ_SNUM, I_ZERO, SEND_NOMSG}, //Speicher ist nicht gef�llt, Misserfolg //F�r alle drei �nderung des Metazustands zur�ck zu idle
	{ READ_SNUM, I_ONE, SEND_SUCCESS}, //Nachricht erfolgreich gesendet
	{ READ_SNUM, I_TWO, SEND_FAIL}, //Nachricht nicht gesendet: Misserfolg

	{ SEND_SUCCESS, I_READ_OK, READ_END },
	{ SEND_FAIL, I_READ_OK, READ_END },
	{ SEND_NOMSG, I_READ_OK, READ_END },
};

irid_transition readbwrite_trans[] =
{
	{SWITCH_RBWRITE, I_READ_OK, READ_END},

	{ READ_END, I_NOVALUE, READ_END },
	{ READ_END, I_CR, READ_END },
	{ READ_END, I_FIRST_LF, READ_NUM },
	{ READ_END, I_SEC_LF, SWITCH_ROK },

	{ READ_NUM, I_NOVALUE, READ_NUM },
	{ READ_NUM, I_ZERO, WRITE_SUCCESS },
	{ READ_NUM, I_ONE, WRITE_TIMEOUT },
	{ READ_NUM, I_TWO, WRITE_CHKFAIL },
	{ READ_NUM, I_THREE, WRITE_SIZEFAIL },

	{ WRITE_SUCCESS, I_READ_OK, READ_END},
//	{ WRITE_TIMEOUT, I_READ_OK, READ_END }, //TODO vorl�ufig auskommentiert, um die bessere Verarbeitung der anderen beiden Fehler zu erm�glichen
	{ WRITE_CHKFAIL, I_READ_OK, READ_END },
	{ WRITE_SIZEFAIL, I_READ_OK, READ_END }
};

irid_transition readok_trans[] = 
{
	{SWITCH_ROK, I_READ_OK, READ_END},
	{ READ_END, I_NOVALUE, READ_END },
	{ READ_END, I_CR, READ_END },
	{ READ_END, I_FIRST_LF, READ_O },
	{ READ_END, I_SEC_LF, SWITCH_IDLE },

	{ READ_O, I_NOVALUE, READ_O },
	{ READ_O, I_READ_OK, READ_K },

	{ READ_K, I_NOVALUE, READ_K },
	{ READ_K, I_READ_OK, READ_END }
};

irid_transition sendbwrite_trans[] =
{
	{SWITCH_BWRITE, I_READ_OK, READ_E},
	{ READ_E, I_NOVALUE, READ_E },
	{ READ_E, I_READ_OK, READ_A },

	{ READ_A, I_NOVALUE, READ_A },
	{ READ_A, I_READ_OK, READ_D },

	{ READ_D, I_NOVALUE, READ_D },
	{ READ_D, I_READ_OK, READ_Y },

	{ READ_Y, I_NOVALUE, READ_Y },
	{ READ_Y, I_READ_OK, READ_END },

	{ READ_END, I_NOVALUE, READ_END },
	{ READ_END, I_CR, READ_END },
	{ READ_END, I_FIRST_LF, WRITE_BUF },

	{ WRITE_BUF, I_READ_OK, SWITCH_RBWRITE },
};


#define IRID_ENTRY_STATE SIG_AT

#define EXIT_STATE TESTING_RESET
#define ENTRY_STATE INIT_DEBUGCHECK
