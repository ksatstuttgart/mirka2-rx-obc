struct systemStatus
{
	unsigned long int lastMem; //Zeit, zu der zuletzt eine Sicherung der Systemzeit im EEPROM durchgef�hrt wurde
	byte sendNum; //Nummer der letzten Teilnachricht, die an MCU1 ging 
	byte iridNum; //Nummer der aktuell gepufferten Iridiumnachricht
	byte resetCount; //Anzahl der bisherigen Resets
};

union systemTime
{
	byte tArray[4]; //Zugriffsarray 
	unsigned long int timeOffset; //Offset f�r den Systemtimer, um die h�henspezifischen Zustands�berg�nge zu steuern
};

union timeBuf
{
	byte tArray[4]; //Zugriffsarray
	unsigned long int time; //Speicherort f�r die aktuelle Systemzeit
};

struct sensorStatus
{
	//Hier "funktioniert"-Flags f�r alle Sensoren und Peripherie einf�gen, bei Recall aus EEPROM auslesen
};