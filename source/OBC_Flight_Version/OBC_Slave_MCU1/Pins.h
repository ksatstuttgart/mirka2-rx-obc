#pragma once
//Pins f�r MCU1 - die Iridium-MCU

//Interrupts: Inputs
#define INT_INPUT_MCU3 2
#define INT_INPUT_MCU2 3
//Interrupts: Output an beide anderen MCUs
#define INT_OUTPUT A0

//SPI-Pins
#define S_SS 10
#define S_MOSI 11
#define S_MISO 12
#define S_SCK 13

#define DEBUGPIN A6 //Debugjumper

//Iridiumpins
#define IRIDIUM_RTS 9 //MUSS F�R FUNKTIONIERENDE KOMMUNIKATION LOW GEZOGEN WERDEN
#define IRIDIUM_RI 8
#define IRIDIUM_CTS 7
#define IRIDIUM_DSR 6
#define IRIDIUM_DCD 5
#define IRIDIUM_ONOFF 4
#define IRIDIUM_NETAV A5
#define IRIDIUM_DTR A4 //MUSS F�R FUNKTIONIERENDE KOMMUNIKATION LOW GEZOGEN WERDEN

//Beacon-Pins
#define BEACON_ENABLE A3

//Resetvote-Outputs
#define VOTE_MCU3 A1
#define VOTE_MCU2 A2

