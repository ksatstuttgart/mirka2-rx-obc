#pragma once

//Debugflags und -Konstanten

//#define MIRKADEBUG

#ifdef MIRKADEBUG
#define DBG_SERIALOUTPUT //Aktiviert die serielle Ausgabe und deaktiviert gleichzeitig GPS und IRIDIUM sowie den externen Drucksensor
#define DBG_STATEWAIT 1000 //Zeit, die die MCU in jedem State verbringt
#endif

//#define LMDEBUG

//#define DBG_SOFTSERIAL //Softwareserialausgabe f�r die Iridium-Kommunikation (Wichtig: Vorher Hardwareserial ausschalten!)
//#define DBG_IRIDSTATES2


//Stellgr��en
#define MEM_INTERVAL 2000 //Alle 2 Sekunden wird eine EEPROM-Speicherung durchgef�hrt DEBUG
#define INTERRUPT_PEAKTIME 20 //Dauer, die der Interruptpin bet�tigt wird
#define START_WAITSHORT 12000 //So lange wartet die Kapsel nach Auswurf, bis sie die Arbeit aufnimmt (in ms)
#define START_WAITLONG 300000 //So lange wartet die Kapsel nach Auswurf, bis sie die Arbeit aufnimmt (in ms)
#define START_WAITIRIDTIME 1000 //So lange wartet MCU1, damit Iridium sich initialisieren kann
#define FLIGHT_WAITTOMED 140000 //So lange wartet die Kapsel, bis GPS angeschaltet wird
#define FLIGHT_WAITTOLOW 300000 //So lange wartet die Kapsel, bis besonders viele GPS-Daten �bertragen werden
#define IRID_MININT 1500// So lange wartet die Kapsel, falls bei Wiederholtem Sendefail der Kondensator wieder laden muss