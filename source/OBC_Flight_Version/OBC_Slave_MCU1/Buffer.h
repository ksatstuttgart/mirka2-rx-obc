struct buffer_hightype
{
	byte msgnum; //Nummer der Nachricht
	unsigned int hk[4]; //Housekeeping-Daten
	unsigned int times[3];//Zeitstempel(intern), die die einzelnen Sensordaten von ihrer jeweiligen MCU erhalten haben
	byte vbat; //Batteriespannung
	unsigned int psdig[12];//SPI-PSx2
	byte psan[6];//Analog-PS
	int imurot[18]; //IMU-Rotationsgeschwindigkeit um 3 Achsen, oversample aus 5 Messungen
	int imuquat[48]; //Quaternionen der Orientierung, 4 2-Byte-Werte laut Datenblatt, daher int.
	byte imucalib[12];//Kalibrierungsstatus der Sensoren und damit Qualit�t der Daten
	int tc[36]; //TCs
	int tccj[36]; //CJs
	int tpile[3]; //Thermopile-Messung
};

struct buffer_medtype
{
	byte msgnum; //Nummer der Nachricht
	unsigned int hk[4]; //Housekeeping-Daten
	unsigned int times[3];//Zeitstempel(intern), die die einzelnen Sensordaten von ihrer jeweiligen MCU erhalten haben
	byte vbat; //Batteriespannung
	unsigned int psdig[12];//SPI-PSx2
	byte psan[6];//Analog-PS
	int imurot[18]; //IMU-Rotationsgeschwindigkeit um 3 Achsen, oversample aus 5 Messungen
	int imuquat[48]; //Quaternionen der Orientierung, 4 2-Byte-Werte laut Datenblatt, daher int.
	byte imucalib[12];//Kalibrierungsstatus der Sensoren und damit Qualit�t der Daten
	int tc[18]; //TCs
	int tccj[18]; //CJs
	int tpile[3]; //Thermopile-Messung
	float gps[16]; //GPS-Daten
	byte gpshk[8]; //GPS-Housekeepingdaten
};

struct buffer_lowtype
{
	byte msgnum; //Nummer der Nachricht
	unsigned int hk[4]; //Housekeeping-Daten
	unsigned int times[3];//Zeitstempel(intern), die die einzelnen Sensordaten von ihrer jeweiligen MCU erhalten haben
	byte vbat; //Batteriespannung
	unsigned int psdig[12];//SPI-PSx2
	byte psan[6];//Analog-PS
	int imurot[18]; //IMU-Rotationsgeschwindigkeit um 3 Achsen, oversample aus 5 Messungen
	int imuquat[24]; //Quaternionen der Orientierung, 4 2-Byte-Werte laut Datenblatt, daher int.
	byte imucalib[6];//Kalibrierungsstatus der Sensoren und damit Qualit�t der Daten
	int tc[18]; //TCs
	int tccj[18]; //CJs
	float gps[24]; //GPS-Daten
	float gpsh[6]; //GPS-H�he
	byte gpshk[12]; //GPS-Housekeepingdaten
};

struct from_mcu3_buffertype
{
	byte number;
	byte number2;
	byte number3;
	uint16_t time3;
	uint16_t time2;
	byte state2;
	byte state3;
	byte resets3;
	byte resets2;
	byte vbat;
	uint16_t psensExt;
	uint16_t psensSmd;
	byte psensAn;
	int rotX;
	int rotY;
	int rotZ;
	int quatw;
	int quatx;
	int quaty;
	int quatz;
	byte calib;
	int tcs[6];
	int cjs[6];
	int tp;
	float gpslat;
	float gpslon;
	float gpsspeed;
	float gpshead;
	byte gpshk;
};

union spi_buffertype
{
	volatile from_mcu3_buffertype from_mcu3_buffer;
	byte getbyte[75];
};

union iridium_buffertype
{
	buffer_hightype buffer_high;
	buffer_medtype buffer_med;
	buffer_lowtype buffer_low;
	byte getbyte[340]; //Zugriffsarray
};