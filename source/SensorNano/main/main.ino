
void setup() {
  
  //put setupcode to run once
  //e.g init sensors 

}

void loop() {
  
  //this one runs in loops after the arduino is started until it is shut down
  //or it resets
  
}

bool initSensors(){
  
  //init sensors here
  //return if successfull
  
}

bool initComm(){
  
  //init communication
  
}

char[] readSensors(){
  
  //read sensors here
  
}
